<?php
	exec('gpio mode 3 OUT');
	exec('gpio write 3 1');
	
	
	$seconds_to_reset = 10;
	$seconds_time_delay_after_restart = 20;
	
	function ResetArduino(){
		exec('gpio write 3 0');
		sleep(1);
		exec('gpio write 3 1');
	}
	function GetLastReceive(){
		$path_time_last_recieve = "/home/pi/ramdisk/last_recieve";
		if(file_exists($path_time_last_recieve)) 
		{
			$str = file_get_contents($path_time_last_recieve);
			$res = explode(";", $str);
			$time_last = $res[0];
		}
		else
		{
			$time_last = 0;
		}
		return $time_last;
	}
	
	echo date("Y-m-d H:i:s")."  Start check ( after delay 20 sec ) "."\r\n";
	sleep(20);
	$last_restart = 0;
	while(true)
	{
		$time_last = GetLastReceive();
		if(abs($time_last - time()) > $seconds_to_reset)
		{
			sleep(3);
			$time_last = GetLastReceive();
			if(abs($time_last - time()) > $seconds_to_reset)
			{
				if(abs($last_restart - time()) > $seconds_time_delay_after_restart)
				{
					ResetArduino();
					$last_restart = time();
					echo date("Y-m-d H:i:s")."  Arduino restarted. TimeLast = ".date("Y-m-d H:i:s", $time_last)." "."\r\n";
				}
			}
		}
		else
			$last_restart = 0;
		sleep(2);
	}
	
