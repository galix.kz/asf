import webhelper_utils
import sys
import os
import ConfigParser
from threading import Timer

_VERSION_URL = 'https://gitlab.com/OverGrower/Raspberry/raw/master/VERSION'
_FIRMWARE_URL = 'https://gitlab.com/OverGrower/Raspberry/raw/master.tar.gz'
_CONFIG_SECTION = 'update'
_CONFIG_OPTION_TIMEOUT = 'timeout'

class Updater():
    _timer = None
    _config = ConfigParser.ConfigParser()

    def __init__( self, conf ):
        self._config.readfp( open( conf, 'w+' ) )


    def fetchCurrentVersion( self ):
        return webhelper_utils.safe_exec( [ 'git', 'describe' ] ).split( '-' )[ 0 ].rstrip( '\n' )

    def fetchLatestVersion( self ):
        sys.stdout.write( webhelper_utils.safe_exec( [ 'git', 'fetch' ] ) )
        return webhelper_utils.safe_exec( [ 'git', 'tag', '--sort', 'version:refname' ] ).split()[ -1 ].rstrip( '\n' )

    @property
    def autoUpgradeTimeout( self ):
        if self._config.has_option( _CONFIG_SECTION, _CONFIG_OPTION_TIMEOUT ):
            return self._config.get( _CONFIG_SECTION, _CONFIG_OPTION_TIMEOUT )
        return 0

    @autoUpgradeTimeout.setter
    def autoUpgradeTimeout( self, timeout ):
        if self._timer:
            self._timer.stop()
        if timeout > 0:
            self._timer = Timer( timeout, lambda : self.performUpgrade() )
            self._timer.start()
        else:
            timeout = 0
            self._timer = None
        self._config.set( _CONFIG_SECTION, _CONFIG_OPTION_TIMEOUT, timeout )

    def performUpgrade( self ):
        current = self.fetchCurrentVersion()
        latest = self.fetchLatestVersion()
        sys.stdout.write( webhelper_utils.safe_exec( [ 'git', 'pull' ] ) )
        sys.stdout.write( '\n' )
        webhelper_utils.safe_exec( [ 'chown', '-R', 'pi:www-data', '.' ] )
        sys.stdout.write( webhelper_utils.safe_exec( [ 'php', 'post-upgrade.php', current, latest ] ) )
        sys.stdout.write( '\n' )
