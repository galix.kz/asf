import types

def enumerate_functions( module ):
    result = dict()
    for a in dir( module ):
        func = module.__dict__.get( a )
        if isinstance( func, types.FunctionType ):
            result[ "" + func ] = func
    return result
