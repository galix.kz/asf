import sys
import webhelper_utils


class NtpConfig( object ):
    _SERVERS = 'Servers='
    _CONF = '/etc/systemd/timesyncd.conf'
    _NTPLIST = '0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org'

    @property
    def enabled( self ):
        with open( self._CONF, 'r' ) as conf:
            for line in conf:
                if line.find( self._SERVERS ) >= 0:
                    return len( line ) > len( self._SERVERS ) + 1
        return False

    @enabled.setter
    def enabled( self, value ):
        content = ''

        with open( self._CONF, 'r' ) as conf:
            for line in conf:
                if line.find( self._SERVERS ) < 0:
                    content += line
                elif value:
                    content += self._SERVERS + self._NTPLIST + "\n"
                else:
                    content += self._SERVERS + "\n"

        if not content:
            content = self._SERVERS + self._NTPLIST

        with open( self._CONF, 'w' ) as conf:
            conf.write( content )

        if value:
            webhelper_utils.safe_exec( [ 'timedatectl', 'set-ntp', 'true' ] )
            webhelper_utils.safe_exec( [ 'systemctl', 'restart', 'systemd-timesyncd' ] )
        else:
            webhelper_utils.safe_exec( [ 'timedatectl', 'set-ntp', 'false' ] )
            webhelper_utils.safe_exec( [ 'systemctl', 'stop', 'systemd-timesyncd' ] )
