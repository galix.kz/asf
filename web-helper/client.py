import sys
from jsonrpc_client import Client

if __name__ == '__main__':
    if len( sys.argv ) < 2:
        sys.exit( 'usage: client method [args]' )
    client = Client( { 'strJSONRPCRouterURL': 'http://127.0.0.1:8080' } )
    params = []
    if len( sys.argv ) > 2:
        params = sys.argv[ 2 : ] 
    sys.stdout.write( client.rpc( sys.argv[ 1 ], params ) )
