import sys
import json
import os

rpi_sign_path="/sys/firmware/devicetree/base/model"

if os.path.exists( rpi_sign_path ) \
    and open( rpi_sign_path, "r" ).read().startswith( "Raspberry Pi" ):

    ethernet_interface = "lan0"
    wireless_interface = "wlan0"
else:
    ethernet_interface = "eno1"
    wireless_interface = "wlp3s0"

test_list = [
    { "name": "ethernet_link", "data": ( "systemrpc.get_ethernet_link", [ ethernet_interface ], True ) },
    { "name": "wireless_link", "data": ( "systemrpc.get_wireless_link", [ wireless_interface ], False ) },
    { "name": "wireless_scan", "data": ( "systemrpc.get_wireless_list", [ wireless_interface ], [] ) }
]

if __name__ == "__main__":
    try:
        import netifaces
        from jsonrpc2 import JsonRpcApplication
        from webtest import TestApp
    except:
        import pip
        pip.main( [ "install", "jsonrpc2" , "webtest", "netifaces" ] )
        from jsonrpc2 import JsonRpcApplication
        from webtest import TestApp

    import systemrpc

    app = JsonRpcApplication()
    app.rpc.add_module( sys.modules[ systemrpc.__name__ ] )
    testapp = TestApp( app )

    for test in test_list:
        print test
        method, params, lhs = test[ "data" ]
        request = {
            "jsonrpc":  "2.0",
            "method":   method,
            "params":   params,
            "id":       method
        }

        rhs = json.loads( testapp.post( "/",
                                        params=json.dumps( request ),
                                        content_type="application/json" ).body )
        print test[ "name" ], method, params, lhs, rhs

        assert( rhs[ "jsonrpc" ] == "2.0" )
        assert( rhs[ "id" ] == method )
        assert( rhs[ "result" ] == lhs )
