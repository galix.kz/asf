import sys
import subprocess
import webhelper_utils
# _mode_wap = "mode=2"
# _mode_wlan = "mode=0"

def _open_layout( mode ):
    try:
        return (
            open( "/etc/network/interfaces.d/20_wireless", mode ),
            open( "/etc/wpa_supplicant.conf", mode )
        )
    except:
        return ()

def _close_layout( layout ):
    for  stream in layout:
        if stream.mode != 'r':
            stream.truncate()
        stream.close()

def _read_config_params( setup ):
    result = {}
    for line in setup.split( '\n' ):
        pair = line.split( '=' )
        if len( pair ) == 2:
            result[ pair[ 0 ].strip() ] = pair[ 1 ].strip()
    return result

def _configure( setup, mode ):
    ssid = setup.get( 'ssid', '' )
    passphrase = "network={\n\t"

    if ssid:
        key = "ssid="
        passphrase += key
        passphrase += "\"" + ssid + "\""
    else:
        key = "bssid="
        passphrase += key
        passphrase += setup[ "bssid" ]

    if "pass" not in setup or len( setup[ "pass" ] ) < 8:
        if "encryption" in setup:
            del setup[ "encryption" ]
        passphrase += "\n}"
    else:
        passphrase = subprocess.check_output( [
            "/usr/bin/wpa_passphrase",
            ssid,
            setup[ "pass" ]
        ] ).decode()

    tail = "\n\tmode=%d\n\tid_str=\"over_grover\"\n\t" + key
    hidden = "scan_ssid=%d\n\t"
    encryption = setup.get( "encryption", "NONE" )


    if encryption == "NONE":
        tail = "\n\tkey_mgmt=" + encryption + tail
    else:
        tail = "\n\tkey_mgmt=WPA-PSK" + tail
        if encryption == "WPA-PSK":
            tail = "pairwise=TKIP\n\tproto=WPA\n\t" + tail
        else:
            tail = "pairwise=CCMP\n\tproto=WPA2\n\t" + tail

    if "hidden" in setup and setup[ "hidden" ]:
        hidden = hidden % 1
    else:
        hidden = hidden % 0

    return passphrase.replace( key, hidden + tail % mode )

class WpaConfig( object ):
    ctrl_interface_path = "/var/run/wpa_supplicant"

    def __init__( self ):
        self.current = None

    def load( self, iface ):
        assert( iface )

        result = {}
        layout = _open_layout( "r" )

        if not layout:
            result[ "mode" ] = ""
        else:
            network, wpa = layout
            prefix = iface + " inet "
            network_content = network.read()
            wpa_content = wpa.read()

            if prefix + "static" in network_content:
                result[ "mode" ] = "wap"
            elif prefix + "dhcp" in network_content:
                result[ "mode" ] = "wlan"
            else:
                result[ "mode" ] = ""

            if result[ "mode" ]:
                params = _read_config_params( wpa_content )

                if "proto" in params and params[ "proto" ] == "WPA2":
                    result[ "encryption" ] = "WPA2-PSK"
                elif "proto" in params and params[ "proto" ] == "WPA":
                    result[ "encryption" ] = params[ "WPA-PSK" ]
                else:
                    result[ "encryption" ] = "NONE"

                if params[ "scan_ssid" ] == "1":
                    result[ "hidden" ] = True

                result[ "ssid" ] = params[ "ssid" ].strip( "\"" )

                if "#psk" in params:
                    result[ "pass" ] = params[ "#psk" ]

        if self.current == result:
            sys.stdout.write( "config not changed\n" )

        self.current = result
        _close_layout( layout )

        return result

    def save( self, iface, setup ):
        assert( iface )

        if self.current == setup:
            sys.stdout.write( "same config detected\n" )
        else:
            self.current = setup

        network, wpa = _open_layout( "w" )

        wpa.write( "update_config=1\nctrl_interface=%s\nctrl_interface_group=netdev\n" % WpaConfig.ctrl_interface_path )
        network.write( "auto %s\n" % iface )

        if not setup or "mode" not in setup or not setup[ "mode" ]:
            network.write( "iface %s inet manual\n" % iface )
        elif setup[ "mode" ] == "wlan":
            wpa.write( _configure( setup, 0 ) )
            network.write( "iface %s inet dhcp\n" % iface )
        elif setup[ "mode" ] == "wap":
            wpa.write( _configure( setup, 2 ) )
            network.write( "iface %s inet static\n" % iface )
            network.write( "    address %s\n" % setup.get( "ip", "192.168.0.1" ) )
            network.write( "    netmask 255.255.255.0\n" )
        else:
            assert( not setup[ "mode" ] )

        network.write( "        wpa-conf " + wpa.name + "\n" )

        _close_layout( ( network, wpa ) )
