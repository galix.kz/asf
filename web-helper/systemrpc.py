import sys
import traceback
import subprocess
import time
import os.path
import webhelper_utils
from ntp_config import NtpConfig
from wpa_config import WpaConfig
from updater import Updater
from jsonrpc2 import JsonRpcException
from jsonrpc2 import INTERNAL_ERROR

_ntpconfig = NtpConfig()
_wpaconfig = WpaConfig()
_updater = Updater( '/etc/web-helper.conf' )

def _rethrow( exc_info ):
    exc_type, exc_value, exc_tb   = exc_info
    sys.stderr.write( "An exception of type {} occurred.".format( exc_type.__name__ ) )
    sys.stderr.write( "Arguments: {}\n".format( traceback.extract_tb( exc_tb ) ) )
    data = traceback.format_exception( exc_type, exc_value, exc_tb )
    raise JsonRpcException( 2, INTERNAL_ERROR, data )

def _safe_exec( command ):
    return sys.stderr.write( webhelper_utils.safe_exec( command ) )

def _wpa_cli( iface, command ):
    return webhelper_utils.safe_exec( [ "wpa_cli", "-i" + iface, command ] )

def _wireless_interface( iface ):
    if not iface:
        iface = "wlan0"
    return iface

def _wireless_up( iface ):
    _safe_exec( [ "ifup", iface ] )
    time.sleep( 2 )

def _wireless_down( iface ):
    _safe_exec( [ "ifdown", iface ] )
    for cmd in ( [ "wpa_cli", "-i", iface, "terminate" ], [ "systemctl", "stop", "wpa_supplicant" ], [ "pkill", "wpa_supplicant" ] ):
        if os.path.exists( os.path.join( WpaConfig.ctrl_interface_path, iface ) ):
            _safe_exec( cmd )
            time.sleep( 2 )
    assert( not os.path.exists( os.path.join( WpaConfig.ctrl_interface_path, iface ) ) )

def getEthernetLink( iface ):
    try:
        result = webhelper_utils.get_interface_link( iface )
    except:
        _rethrow( sys.exc_info() )
    return result

def getEthernetConfig( iface ):
    try:
        iface = _wireless_interface( iface )
        result = {
            "ip" : webhelper_utils.get_interface_addr( iface, False ),
            "mac": webhelper_utils.get_interface_addr( iface, True )
        }
    except:
        _rethrow( sys.exc_info() )
    return result

def getWirelessLink( iface ):
    try:
        iface = _wireless_interface( iface )
        result = webhelper_utils.get_interface_link( iface )
    except:
        _rethrow( sys.exc_info() )
    return result

def getWirelessConfig( iface ):
    try:
        iface = _wireless_interface( iface )
        result = _wpaconfig.load( iface )
        result[ "ip" ] = webhelper_utils.get_interface_addr( iface, False )
        result[ "mac" ] = webhelper_utils.get_interface_addr( iface, True )
    except:
        _rethrow( sys.exc_info() )
    return result

def setWirelessConfig( iface, config = None ):
    try:
        iface = _wireless_interface( iface )
        mode = getWirelessConfig( iface )[ "mode" ]
        mode_set = type( config ) == dict and "mode" in config
        if not mode_set or mode != config[ "mode" ]:
            if mode == "wap":
                #assert( getWirelessLink( iface ) )
                _safe_exec( [ "systemctl", "stop", "isc-dhcp-server" ] )
                time.sleep( 2 )

        _wireless_down( iface )
        try:
            _wpaconfig.save( iface, config )
        except UnicodeError:
            _wireless_up( iface )
            raise ValueError("invalid encoding detected")

        _wireless_up( iface )
        result = getWirelessLink( iface )

        if mode_set and config[ "mode" ] == "wap":
            _safe_exec( [ "systemctl", "start", "isc-dhcp-server" ] )
        elif not mode_set or config[ "mode" ] != "wlan":
            result = not result
    except:
        _rethrow( sys.exc_info() )
    return result

def getWirelessList( iface ):
    try:
        iface = _wireless_interface( iface )

        if "PONG" not in _wpa_cli( iface, "ping" ):
            setWirelessConfig( iface )

        assert( "OK" in _wpa_cli( iface, "scan" ) )

        attempts = 5
        scan_results = []
        while len( scan_results ) < 2 and attempts > 0:
            time.sleep( 2 )
            scan_results = _wpa_cli( iface, "scan_results" ).split( "\n" )
            attempts -= 1
        if attempts <= 0:
            sys.stderr.write( "scan_results:[{}]".format( scan_results ) )

        headers = scan_results[ 0 ].split( "/" )
        result = []

        for scan_result in scan_results[ 1 : ]:
            if not scan_result:
                continue

            fields = scan_result.split( "\t" )
            assert( len( fields ) == len( headers ) )
            wifi_entry = {}
            for idx, field in enumerate( fields ):
                wifi_entry[ headers[ idx ].strip() ] = field.strip()

            if "WPA2-PSK" in wifi_entry[ "flags" ]:
                encryption = "WPA2-PSK"
            elif "WPA-PSK" in wifi_entry[ "flags" ]:
                encryption = "WPA-PSK"
            else:
                encryption = "NONE"

            level = max( int( wifi_entry[ "signal level" ] ), -100 )
            assert( level < 0 )

            result.append( {
                "ssid": wifi_entry[ "ssid" ],
                "bssid": wifi_entry[ "bssid" ],
                "encryption": encryption,
                "strength": level + 100
            } )
    except:
        _rethrow( sys.exc_info() )
    return result


def getTimezoneList():
    return webhelper_utils.safe_exec( [ 'timedatectl', 'list-timezones' ] )

def setTimezone( tz ):
    webhelper_utils.safe_exec( [ 'timedatectl', 'set-timezone', tz ] )
    webhelper_utils.safe_exec( [ 'systemctl', 'reload', 'apache2' ] )
    webhelper_utils.safe_exec( [ 'systemctl', 'restart', 'comPortArduino' ] )
    webhelper_utils.safe_exec( [ 'systemctl', 'restart', 'syncArduino' ] )

def getCurrentTimestamp():
    return int( time.time() )

def setCurrentTimestamp( timestamp ):
    if isNtpdEnabled():
	 raise RuntimeError( 'ntp must be disabled' )
    return webhelper_utils.linux_set_time( timestamp )

def enableNtpd():
    _ntpconfig.enabled = True

def disableNtpd():
    _ntpconfig.enabled = False

def isNtpdEnabled():
    return _ntpconfig.enabled

def getCurrentVersion():
    return _updater.fetchCurrentVersion()

def getLatestVersion():
    return _updater.fetchLatestVersion()

def getAutoUpgradeTimeout():
    return _updater.autoUpgradeTimeout

def setAutoUpgradeTimeout( timeout ):
    _updater.autoUpgradeTimeout = timeout

def applyUpdate():
    _updater.performUpgrade()

if __name__ == "__main__":
    from jsonrpc2 import JsonRpcApplication
    from wsgiref.simple_server import make_server

    app = JsonRpcApplication()
    app.rpc.add_module( sys.modules[ __name__ ] )

    httpd = make_server( "127.0.0.1", 8080, app )
    httpd.serve_forever()
