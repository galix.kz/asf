#!/bin/bash

function exec_rpc() {
    if [ $# -eq 0 ]
    then
        echo "provide the method name"
        exit 1
    fi

    local length=$(($#-1))
    local args=${@:2:$length}
    local params=""

    while [[ $length -ne 0 ]]
    do
        length=$(($length - 1))
        if [ -z $params ]
        then
            params="${args[$length]}"
        else
            params="$params,${args[$length]}"
        fi
    done

    local json="{\"jsonrpc\":\"2.0\",\"id\":\"$1\",\"method\":\"systemrpc.$1\",\"params\":[$params]}"
    curl -q --data-binary $json -H 'content-type:application/json;' http://127.0.0.1:8080/
}
function getCurrentTimestamp() {
    printf $(exec_rpc getCurrentTimestamp | cut -d ' ' -f 6 | cut -d '}' -f 1)
}

left=$(getCurrentTimestamp)
right=$(( $left - 1000 ))
result=$(exec_rpc setCurrentTimestamp $right)
sample=$(getCurrentTimestamp)
echo "!!$left $right $result $sample!!"

test "$sample" -ne "$right" && exit 30

echo "done"
