import sys
import types
import netifaces
import subprocess
import traceback
import ctypes
import ctypes.util
import time
import datetime

def enumerate_functions( module ):
    result = dict()
    for a in dir( module ):
        func = module.__dict__.get( a )
        if isinstance( func, types.FunctionType ):
            result[ "" + func ] = func
    return result

def get_interface_link( iface ):
    return netifaces.AF_INET in netifaces.ifaddresses( iface )

def get_interface_addr( iface, is_mac ):
    result = ""
    addrs = netifaces.ifaddresses( iface )

    if is_mac:
        family = netifaces.AF_LINK
    else:
        family = netifaces.AF_INET

    if family in addrs:
        result = addrs[ family ][ 0 ][ "addr" ]

    return result


def safe_exec( args ):
    try:
        result = subprocess.check_output( args )
    except:
        result = "error: {} - {}".format( sys.exc_type, sys.exc_value )
    return result

def linux_set_time( timestamp ):
    dt = time.strftime( '%c', time.localtime( timestamp ) )
    return safe_exec( [ 'hwclock', '--set', '--date', dt  ] ) + \
           safe_exec( [ 'hwclock', '-s' ] ) + \
           safe_exec( [ 'hwclock', '--adjust' ] )
