jQuery(document).ready(function(){

	jQuery(document).on('click', function(e){
		if (jQuery(e.target).closest('#menu-top').length || jQuery(e.target).closest('.button').length)
		{
			return;
		}

		jQuery('#menu-top').hide();
		e.stopPropagation();
	});

	jQuery('#header .button').on('click', function(){
		if (jQuery('#menu-top').is(':visible'))
		{
			jQuery('#menu-top').slideUp(200);
		}
		else
		{
			jQuery('#menu-top').slideDown(200);
		}
	});

});

function changeSelectedView(select) {
	document.location=document.location.toString().replace(/view=[^=]+/, 'view=' + select.options[select.selectedIndex].value)
}

