jQuery(document).ready(function() {
	$('.styler').styler();
	
	$('.steps-line .slider').slick({
		infinite: false,
		draggable : false,
		slidesToShow: 4,
//		fade : true,
		slidesToScroll: 4
	});
	$('.slider_main_step_cycle').slick({
		infinite: false,
		arrows: false,
		slidesToShow: 1,
		mmm : 2,
		draggable : false,
		slidesToScroll: 1
		
		
	});
	
	$('.select-values').slick({
		infinite: false,
		slidesToShow: 3,
		mmm : 22,
		vertical: true,
		verticalSwiping: true
	});
	
	$('.shedule .itm .values').click(function(){
			$($(this).attr('data-pop-id')).toggleClass('active');
//		$(this).parents('.itm').find('.botpopup').toggleClass('active');
	});
	$('.calibr-step .itm .showpop').click(function(){
		if($(".botpopup").hasClass("active"))
			return false;
		$(this).parents('.itm').find('.botpopup').toggleClass('active');
	});
	$('.controls .itm .btn').click(function(){
		$(this).parents('.itm').find('.botpopup').toggleClass('active');
	});

	//	$('.botpopup .btn').click(function(){
//		$(this).parents('.botpopup').toggleClass('active');
//	});
	$('.steps-line .slider span').click(function(){
		$('.steps-line .slider span').removeClass('active');
		$(this).addClass('active');
		var step = $(this).text() - 1;
		$('.slider_main_step_cycle').slick('slickGoTo', step);
	})
	$('#slider_main_step_cycle').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		if(slick.options.mmm == 2)
		{
		$('.steps-line .slider span').removeClass('active');
		$('[data-span-val = '+ nextSlide+ ']').addClass('active');
//	  console.log('Собираемся осуществить переход к '+nextSlide+' слайду');
//	  console.log(slick);
		}
	});	
	$('.select-values .inputbox').change(function(){
		var cur_val = $(this).val();
		$(this).parent().parent().find('input[type=hidden]').val(cur_val);
	})
	$('.select-values').on('afterChange', function(event, slide, index){
		if(slide.options.mmm == 22)
		{
			
			var sl = $(slide.$slides.get(index + 1));
			var cur_val;
			if(sl.attr('empty') == 1)
				cur_val = "";
			else if(sl.attr('data-val'))
			{
				cur_val = sl.attr('data-val');
			}
			else
				cur_val = sl.find('input').val();
			var input = $(this).find('input[type=hidden]');
			input.val(cur_val);
			if(sl.attr('data-text'))
				input.attr('data-text', sl.attr('data-text'));
/*			if(slide.options.id_to)
			{
				$(slide.options.id_to).html(cur_val);
			}
*/			
			
		}
	});	
	
	$('#href_rus').click(function(){
		$('.botpopup .lang a').removeClass('active');
		$(this).addClass('active');
		$('.botpopup .keys').hide();
		$('#word_rus').show();
		return false;
	});
	$('#href_eng').click(function(){
		$('.botpopup .lang a').removeClass('active');
		$(this).addClass('active');
		$('.botpopup .keys').hide();
		$('#word_eng').show();
		return false;
	});
	$('#href_num').click(function(){
		$('.botpopup .lang a').removeClass('active');
		$(this).addClass('active');
		$('.botpopup .keys').hide();
		$('#word_num').show();
		return false;
	});
	$('#button_steret').click(function(){
		$('#to_paste').val(
			function(index, value){
				return value.substr(0, value.length - 1);
		})
		return false;
	});
	$('#button_vstavit').click(function(){
		var cur_href = $('.botpopup .lang .active').attr('id');
		var symvol = '';
		if(cur_href == "href_rus")
		{
			var cur = $('#word_rus').slick('slickCurrentSlide') + 1;
			var div = "div[data-slick-index=" + cur + "]";
			symvol = $('#word_rus').find(div).text();
		}
		if(cur_href == "href_eng")
		{
			var cur = $('#word_eng').slick('slickCurrentSlide') + 1;
			var div = "div[data-slick-index=" + cur + "]";
			symvol = $('#word_eng').find(div).text();
		}
		if(cur_href == "href_num")
		{
			var cur = $('#word_num').slick('slickCurrentSlide') + 1;
			var div = "div[data-slick-index=" + cur + "]";
			symvol = $('#word_num').find(div).text();
		}
		$( "#to_paste" ).val($( "#to_paste" ).val() + symvol);
		return false;
	});
	
	
$( "#to_paste" ).focus(function() {	

	$('.botpopup').addClass('active');
});
setTimeout(
  function() 
  {
	$('.botpopup .keys').hide();
	$('#word_eng').show();
  }, 100);
  $('.co2_gotovo').click(function(){
	  var time = '';
	  $(this).parent().parent().find('input[type=hidden]').each( function(){
		  if(time == '')
				time += $(this).val();
			else
				time += (" - " + $(this).val());
			if($(this).val() == '')
			{
				time = '';
				return false;
			}
/*		  var sl = $(this).parents('.select-values');
			console.log(sl);
		  var cur = sl.slick('slickCurrentSlide') + 1;
			var div = "div[data-slick-index=" + cur + "]";
			console.log(div);
			var symvol = sl.find(div).find('.inputbox').val();
			console.log(symvol);
*/		
		  

	  });
	  var pp = $($(this).attr('data-co2'));
	  if(time == '')
	  {
		  pp.addClass('empty');
		  time = "____ - ____";
	  }
	  else
		  pp.removeClass('empty');
	  pp.find('.time').html(time);
		$(this).parents(".botpopup").removeClass('active');
	  return false;
  })
  $(".co2_delete").click(function(){
		$(this).parents(".botpopup").find(".select-values").each(function(){
			$(this).slick('slickGoTo', 0);
		})
		return false;
  });
  $('.gotovo_standart').click(function(){
		var val = $(this).parents('.botpopup').find('input[type=hidden]').val();
		$($(this).attr('data-id')).html(val);
		 $(this).parents('.botpopup').removeClass('active');
	 return false; 
  });
  $('.otmena_standart').click(function(){
		 $(this).parents('.botpopup').removeClass('active');
	 return false; 
  });
});


