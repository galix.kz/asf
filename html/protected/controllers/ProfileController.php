<?php

class ProfileController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
			array ('allow', // allow all users to perform these actions
					'users' => array('@'),
				),
			array ('allow', // allow all users to perform these actions
					'ips' => array('127.0.0.1', "::1"),
				),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	 
	 public function actionExcel($id){
		 
		$history = History::model()->findByPk($id);
		$dateFrom = $history->start_time;
		$dateTo = $history->stop_time;
			
		$model = Data::model()->findAll("date > $dateFrom AND date < $dateTo");
		$arr = array();
		$arr[] = array('Дата', 'pH', 'TDS', 'wlevel', 'WaterT', 'CO2', 'Humidity', 'Light', 'Air t');
		foreach($model as $one)
		{
			$arr[] = array(date("Y-m-d H:i:s",$one->date), $one->ph, $one->tds, $one->wlevel, $one->wtemp, $one->co2, $one->hum, $one->light, $one->temp);
		}
		
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', false, 'points');
		$xls->addArray($arr);
		$xls->generateXML('Points');
		die();
	 }
	 public function actionExcelNow(){
		 
		 $settings = Settings::model()->findByPk(1);
		$dateFrom = $settings->active_start_time;
			
		$model = Data::model()->findAll("date > $dateFrom");
		$arr = array();
		$arr[] = array('Дата', 'pH', 'TDS', 'wlevel', 'WaterT', 'CO2', 'Humidity', 'Light', 'Air t');
		foreach($model as $one)
		{
			$arr[] = array(date("Y-m-d H:i:s",$one->date), $one->ph, $one->tds, $one->wlevel, $one->wtemp, $one->co2, $one->hum, $one->light, $one->temp);
		}
		
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', false, 'points');
		$xls->addArray($arr);
		$xls->generateXML('Points');
		die();
	 }
	 public function actionHistory()
	 {
		 $history_list = History::model()->findAll();
		 $this->render('history', array('history_list' => $history_list));
	 }
	 public function actionDelHistory($id)
	 {
		 $model = History::model()->findByPk($id);
		 if($model)
			 $model->delete();
		  $this->redirect(array('profile/history'));
	 }
	 public function actionViewHistory($id)
	 {
		 $history = History::model()->findByPk($id);
		 
		 $this->redirect(array('site/viewgraph', 'dateFrom' => date("Y-m-d H:i", $history->start_time), 'dateTo' => date("Y-m-d H:i", $history->stop_time), 'cnPoint' => 60));
	 }
	 public function actionCurrent()
	 {
		 $this->title = 'Управлением выращиванием';
		 
		 $model = new Profile;
		 $settings = Settings::model()->findByPk(1);
		 if($settings->active_profile_id && $settings->active_profile_id > 0)
		 {
			$model = $this->loadModel($settings->active_profile_id);
			$cycle = $model->getActiveCycle($settings->active_start_day);
		 }
		 else 
			 $cycle = NULL;
		 
		 $list = Profile::model()->findAll();
		 
		 if(isset($_POST['profile_id']))
		 {
			 $settings->active_profile_id = $_POST['profile_id'];
			 $settings->active_start_day = strtotime('today') - $_POST['day_count_from'] * 24 * 60 * 60;
			 $settings->active_start_time = time();
			 
			 
			 $settings->save(false);
			 
			 
			 $this->redirect(array('profile/current'));
		 }
		 if(isset($_POST['stop']))
		 {
			 $hh = new History;
			 $hh->profile_id = $settings->active_profile_id;
			 $hh->name = $model->name;
			 $hh->start_time = $settings->active_start_time;
			 $hh->stop_time = time();
			 $hh->save(false);
			 $settings->active_profile_id = 0;
			 $settings->active_start_day = $settings->active_start_time = 0;
			 $settings->save(false);
//			 list($controller) = Yii::app()->createController('Command');
//   			 $controller->execCommandByCron("set", 'reg_delay', 0);
			 $this->redirect(array('profile/current'));
		 }
		 $this->render('current', array('model' => $model, 'settings' => $settings, 'profile_list' => $list, 'cycle' => $cycle));
	 }
	public function actionAddCycle($profile_id, $cycle_id = NULL)
	{
		if($profile_id == 1)
		{
			$cycleOne = Profile::model()->findByPk(1)->circles[0]->id;
			if($cycle_id != $cycleOne)
				$this->redirect(array('profile/view', 'id' => $profile_id));
		}
		$model = new Cycle;
		if($cycle_id)
			$model = Cycle::model()->findByPk($cycle_id);
		else 
		{
			$prof = $this->loadModel($profile_id);
			$m1 = NULL;
			foreach($prof->circles as $one)
				$m1 = $one;
			if($m1 != NULL)
			$model->attributes = $m1->attributes;
		}
		if(Yii::app()->request->isAjaxRequest)
		{
		
		
			if(isset($_POST['Cycle']))
			{
				$model->attributes=$_POST['Cycle'];
				$model->parent_id = $profile_id;
				if($model->co2_time1_from == '')
				{
					if($model->co2_time2_from)
					{
						$model->co2_time1_from = $model->co2_time2_from;
						$model->co2_time1_to = $model->co2_time2_to;
						$model->co2_time2_from = '';
						$model->co2_time2_to = '';
					}
					else if($model->co2_time3_from)
					{
						$model->co2_time1_from = $model->co2_time3_from;
						$model->co2_time1_to = $model->co2_time3_to;
						$model->co2_time3_from = '';
						$model->co2_time3_to = '';
					}
				}
				if($model->co2_time2_from == '')
				{
					if($model->co2_time3_from)
					{
						$model->co2_time2_from = $model->co2_time3_from;
						$model->co2_time2_to = $model->co2_time3_to;
						$model->co2_time3_from = '';
						$model->co2_time3_to = '';
					}
				}
				
				if($model->save())
				{
					echo $model->id;
					Yii::app()->end();
				}
			}
			$this->renderPartial('_formCycle',array(
				'model'=>$model,
				'profile_id' => $profile_id,
			));
			Yii::app()->end();
		}
		
		$this->render('_fCycle',array(
			'model'=>$model,
			'profile_id' => $profile_id,
		));
	}
	public function actionView($id)
	{
		$this->title = 'Редактирование профиля';
		if($id == 1) 
		{
			$model = $this->loadModel($id);
			$this->redirect(array('profile/viewCycle', 'id' => $model->circles[0]->id));
		}
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionCurrentCycle(){
		
		 $settings = Settings::model()->findByPk(1);
		 if($settings->active_profile_id && $settings->active_profile_id > 0)
		 {
			$profile = $this->loadModel($settings->active_profile_id);
			$cycle = $profile->getActiveCycleP($settings->active_start_day, true);
		 }
		 else 
		 {
			 $cycle = NULL;
			 $profile = NULL;
		 }
//		print_r($cycle);
		$this->render('currentCycle',array(
			'profile' => $profile,
			'model' => $cycle,
			
		));
	}
	public function actionViewCycle($id)
	{
		$this->title = 'Редактирование профиля';
		$model = Cycle::model()->findByPk($id);
		$profile = Profile::model()->findByPk($model->parent_id);

		$this->render('viewCycle',array(
			'model'=>$model,
			'profile' => $profile,
			
		));
	}
	public function actionDelCycle($id){
		$model = Cycle::model()->findByPk($id);
		$profile_id = $model->parent_id;
		$model->delete();
		if(!isset($_GET['ajax']))
			$this->redirect(array('view', 'id' => $profile_id));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Profile;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('_form',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Profile']))
		{
			$model->attributes=$_POST['Profile'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('_form',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete1($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = Profile::model()->findAll();
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Profile('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Profile']))
			$model->attributes=$_GET['Profile'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Profile the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Profile::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Profile $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
