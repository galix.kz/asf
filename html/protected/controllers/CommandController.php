<?php

/*
 * 
 * Команды:
  set,<команда>,<значение>
  ph1 - ph2 - tds - hum - reg_delay
  ну ты понял
  5 значений
  передавать на порт
  выдача вроде как вот так выглядит:
  RegulatedPH; ph1=5; ph2=6;
  RegulatedTds; tds=450;
  Humidity=30;
  reg_delay=1;
 * 
 */

class CommandController extends Controller {
    public $SLEEPTIME = 1;//sec time after execute every command by cron
    public $PORT = '/dev/ttyS0';
	public $debug = true;
    public function accessRules() {
        return array(
			array ('allow', // allow all users to perform these actions
					'users' => array('@'),
				),
			array ('allow', // allow all users to perform these actions
					'ips' => array('127.0.0.1', "::1"),
				),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	public function SendStart() // send all 
	{
		return;
		$settings = Settings::model()->findByPk(1);
		$profile_id = $settings->active_profile_id;
		if($profile_id > 0)
		{
			$profile = Profile::model()->findByPk($profile_id);
			if(!$profile)
				return;
			$cycle = $profile->getActiveCycle($settings->active_start_day);
			if(!$cycle)
				return;

			$this->execCommandByCron("set", 'reg_delay', 10);
			sleep($this->SLEEPTIME);
			
			$this->execCommandByCron("set", 'ph1', $cycle->ph_min);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'ph2', $cycle->ph_max);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'ph_delay', $cycle->ph_ml);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'tds', $cycle->tds_min);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'tdsA', $cycle->soil_a);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'tdsB', $cycle->soil_b);
			sleep($this->SLEEPTIME);
			$this->execCommandByCron("set", 'tdsC', $cycle->soil_c);
			sleep($this->SLEEPTIME);
			
		}
	}
    public function actionLoadCircleByCron() {
        
//        return;
        //echo "LoadCircleByCron";
        $debug = true;
        $ramdpath = Yii::app()->params['defaults']['ramdpath'];// "/home/pi/ramdisk/";
// -------------- start time check ---------------------------		
		$fileTime = $ramdpath."curtime";
		if(!file_exists($fileTime)) return;  	// Нет времени - ничего не делаем
		
		$arduino_time = rtrim(@file_get_contents($fileTime));
		if(!$arduino_time)	return;
		$arduino_time = strtotime($arduino_time);
		if(abs($arduino_time - time()) > 60)
		{
			$this->execCommandByCron("date", date('U'), date('O'));
			sleep($this->SLEEPTIME);
		}
// --------------- stop time check ---------------------------		
        $files = array(
             "ph1" => "ph_min", "ph2" => "ph_max", "ph_delay" => "ph_ml", "tds_min" => "tds_min", "tdsa" => "soil_a" ,"tdsb" => "soil_b", "tdsc" => "soil_c", "reg_delay" => "reg_delay", "lightlvl" => "lightlvl",
			 "day" => "light_from", "night" => "light_to", "co2min" => "co2_min", "co2max" => "co2_max", 'airtempday' => 'microclimate_t_day', 'airtempnight' => 'microclimate_t_night',
        );
		$correctCommad = array('tdsa' => 'tdsA', 'tdsb' => 'tdsB', 'tdsc' => 'tdsC', 'tds_min' => 'tds', 'co2min' => 'CO2Min', 'co2max' => 'CO2Max', 'airtempday' => 'AirD', 'airtempnight' => 'AirN');
		$settings = Settings::model()->findByPk(1);
		$profile_id = Settings::model()->findByPk(1)->active_profile_id;
		if( $profile_id > 0 )
		{
			$profile = Profile::model()->findByPk($profile_id);
			if(!$profile)
				return;
			$index = 0;
			$cycle = $profile->getActiveCycle($settings->active_start_day, $index);
			$cycle = $profile->circles[$index];
			$cycle_next = NULL;
			if(isset($profile->circles[$index + 1]))
				$cycle_next = $profile->circles[$index + 1];
			if(!$cycle)
				return;
			
			foreach( $files as $k => $v )
			{
				$param_val = rtrim(@file_get_contents($ramdpath.$k));
				switch($k){
					case "airtempday" :
					case "airtempnight" : 
						$param_val = (int)$param_val; 
						break;
				}
//				$val = (($v == "reg_delay") ? $settings->reg_delay : $cycle->{$v});
				switch($v){
					case "reg_delay" : $val = $settings->reg_delay; break;
					case "lightlvl" :  
						if($settings->lightSensorType == 1)
							$val = 0; 
						else  if($settings->lightSensorType == 2)
							$val = $settings->lightDosvetka; 
						else 
							$val = 0; // На всякий случай
						break;
					default : $val = $cycle->{$v};
					
				}
				if(in_array($k, array('tdsa', 'tdsb', 'tdsc')))
				{
					$val = ($settings->volume_cap / 100.0) * $val;
					$val = ((int)($val * 100 + 0.5)) / 100.0;
				}
				if(in_array($k, array('day', 'night')))
				{
					$val = str_replace(":", "", $val);
				}
				if($v == 'tds_min')
				{
					if($cycle->tds_gradual && $cycle_next)
					{
/*						$cnDay = (int)((time() - $settings->active_start_day) / (60 * 60 * 24));
						$cnDayAll = $cycle->day_count;
						$val = $val + ($cnDay / $cnDayAll) * ($cycle_next->{$v} - $cycle->{$v});
*/						
					}
				}
				$k = (isset($correctCommad[$k])) ? $correctCommad[$k] : $k;
				if ( $param_val != "" && $param_val != $val){
					if($debug)  echo date('Y-m-d H:i')."Start $k -> $v<br>";
					$this->execCommandByCron("set", $k, $val);
					if($debug)  echo "<br>".date('Y-m-d H:i')."Stop $k -> $v<br>";
					sleep($this->SLEEPTIME);
				}
				else if ($param_val == "")
				{
					if($debug)  echo date('Y-m-d H:i')."Start(empty) $k -> $v<br>";
					$this->execCommandByCron("set", $k, $val);
					if($debug)  echo "<br>".date('Y-m-d H:i')."Stop(empty) $k -> $v<br>";
					sleep($this->SLEEPTIME);
				}
			}
			
//	----------		check CO2  ------------------
//			Конвертируем из ардуино в массив
			$val = rtrim(@file_get_contents($ramdpath."co2arr"));
			$valArrayArduino = array(); $i = 0;
			if($val)
			{
				$arrayC1 = explode(',', $val);
				foreach($arrayC1 as $v1)
				{
					$valArrayArduino[$i++] = $v1;
				}
			}
			
//			Конвертируем из малины в массив
			$co2Arr = array('0' => 'co2_time1_from', '1' => 'co2_time2_from', '2' => 'co2_time3_from');
			$valArrayMalina = array(); $i = 0;
			foreach ($co2Arr as $k => $v)
			{
				$vvMin = $cycle->{$v} ? str_replace(":", "", $cycle->{$v}) : -1; $vvMin = (int)$vvMin;
				$vMax = str_replace("from", "to", $v);
				$vvMax = $cycle->{$vMax} ? str_replace(":", "", $cycle->{$vMax}) : -1; $vvMax = (int)$vvMax;
				$strVal = $vvMin.':'.$vvMax;
				$valArrayMalina[$i++] = $strVal;
			}
//  	сравниваем массив ардуины с массивом с малины
			$currentBool = array(0 => false, 1 => false, 2 => false);
			foreach($valArrayMalina as $k1 => $v1)
			{
				foreach($valArrayArduino as $k2 => $v2)
				{
					if(!$currentBool[$k2] && ($v2 == $v1))
					{
						$currentBool[$k2] = true; break;
//						die('123');
					}
				}
			}
//			print_r($currentBool);die();
			$isEq = true;
			foreach($currentBool as $v)
				if(!$v)
					$isEq = false;
			
			if(!$isEq)
			{
				$this->execCommandByCron("set", "CO2Arr", implode(',', $valArrayMalina));
				sleep($this->SLEEPTIME);
			}
//	----------	end	check CO2  ------------------
		}
		else
		{
			if($debug)  echo "<br>".date('Y-m-d H:i')."getRegD";
			$param_val = @file_get_contents($ramdpath."reg_delay");
			$param_val = rtrim($param_val);
			if( $param_val == "" || $param_val > 0)
			{
				if($debug)  echo "<br>".date('Y-m-d H:i')."set reg_delay 0<br>";
				$this->execCommandByCron("set", "reg_delay", 0);
				sleep($this->SLEEPTIME);
			}
		}
        Yii::app()->end();
    }    
    
    public function actionLoadcircle() {
        $circle = Circle::model()->find(array('condition'=>'id='.$_POST['circle_id']));
        echo CJSON::encode($circle);
        //echo "Ok".$_POST['profile_id'];
        Yii::app()->end();
    }

    public function actionSetcircle() {
        //echo "Ok".$_POST['profile_id'];
        $conffile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configfile'];

        $config = new CConfiguration($conffile);
        $config['profile_id'] = $_POST['profile_id'];
        $config['circle_id'] = $_POST['circle_id'];
        $val = "<?php return ".$config->saveAsString()."; ?>";
        file_put_contents($conffile, $val);
        echo $val;
        Yii::app()->end();
    }
    
    public function actionLoadcircles() {
        
        
        $circle = Circle::model()->findAll(array('order' => 'name','condition'=>'profile_id='.$_POST['profile_id']));
 
        $data = CHtml::listData($circle, 'id', 'name');

        echo "<option value=''>Select circle</option>";
        foreach($data as $value=>$name)
            echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
        
        Yii::app()->end();
    }
    
    public function actionCommand($command, $arg = NULL) {
        
        $this->execCommand('set', $command, $arg);
    }
    public function actionAjaxSaveParamVal($nameVal, $val)
	{
		Params::SaveParam($nameVal, $val);
		echo $nameVal;
		die();
	}
	
    public function actionSaveCalTds($nbPoint, $valReal) {
		$cal = Calibration::model()->findByPk(1);
		if($nbPoint == 1)
		{
			$cal->caltds1 = $valReal;
			$cal->ct1raw = Data::getCurrentValue('tds_raw');
//			Params::SaveParam("cal_tds_1", $valReal);
		}
		if($nbPoint == 2)
		{
			$cal->caltds2 = $valReal;
			$cal->ct2raw = Data::getCurrentValue('tds_raw');
//			Params::SaveParam("cal_tds_2", $valReal);
		}
		$cal->save(false);
    }
    public function actionSaveCalPh($nbPoint, $valReal) {
		$cal = Calibration::model()->findByPk(1);
		if($nbPoint == 1)
		{
			$cal->calph1 = $valReal;
			$cal->cp1raw = Data::getCurrentValue('ph_raw');
//			Params::SaveParam("cal_ph_1", $valReal);
			
			
		}
		if($nbPoint == 2)
		{
			$cal->calph2 = $valReal;
			$cal->cp2raw = Data::getCurrentValue('ph_raw');
//			Params::SaveParam("cal_ph_2", $valReal);
		}
		$cal->save(false);
    }

    public function actionCommandget($command, $arg = NULL) {
        
        $this->execCommand('get', $command, $arg);
    }
    
    
    /**
     * 
     * @param type $prefix set or get 
     * @param type $command ph1 ph2 and etc.
     * @param type $arg value of ph1,ph2 and etc.
     * 
     */
	 
	 public function doCommand($prefix, $command, $arg){
		$precom = $prefix . ',' . $command . ',' . $arg;
        $com = 'echo "' . $precom . '"';
        $com = $com . ' > '.$this->PORT;
//        echo $com;
        
        $out = "";
        $p = proc_open($com, array(1 => array('pipe', 'w'),
            2 => array('pipe', 'w')), $io);

        while (!feof($io[1])) {
            $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
        }
        while (!feof($io[2])) {
            $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
        }

        fclose($io[1]);
        fclose($io[2]);
        proc_close($p);
	 }
    public function execCommandByCron($prefix, $command, $arg){
        //$precom = 'set' . ',' . $command . ',' . $arg;
        $precom = $prefix . ',' . $command . ',' . $arg;
        $com = 'echo "' . $precom . '"';
        $com = $com . ' > '.$this->PORT;
        echo $com;
//        die();
        $out = "";
        $p = proc_open($com, array(1 => array('pipe', 'w'),
            2 => array('pipe', 'w')), $io);

        while (!feof($io[1])) {
            $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
        }
        while (!feof($io[2])) {
            $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
        }

        fclose($io[1]);
        fclose($io[2]);
        proc_close($p);

        echo $out;
		if($this->debug)
		{
			$model = new CommandLogs;
			$model->command = $com;
			$model->tag = 'execCommandByCron';
			$model->save();
		}
/*        
        //echo Yii::getPathOfAlias('application.config');
        $conffile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configfile'];
        
        // load
        $config = new CConfiguration($conffile);
        // access and setup
        $config[$command] = $arg;
        //echo $config['mySecondOption'];
		*/
        // save
/*        file_put_contents($conffile, "<?php return ".$config->saveAsString()."; ?>"); */


		 
//        Yii::app()->end();  -- Нельзя раскомментить! Не будет работать стоп
    }
    
    /**
     * 
     * @param type $prefix set or get 
     * @param type $command ph1 ph2 and etc.
     * @param type $arg value of ph1,ph2 and etc.
     * 
     */
    public function execCommand($prefix, $command, $arg){
        //$precom = 'set' . ',' . $command . ',' . $arg;
        $precom = $prefix . ',' . $command . ',' . $arg;
        $com = 'echo "' . $precom . '"';
        $com = $com . ' > '.$this->PORT;///dev/ttyUSB0';
        echo $com;
        
        $out = "";
        $p = proc_open($com, array(1 => array('pipe', 'w'),
            2 => array('pipe', 'w')), $io);

        while (!feof($io[1])) {
            $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
        }
        while (!feof($io[2])) {
            $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
        }

        fclose($io[1]);
        fclose($io[2]);
        proc_close($p);

        echo $out;
		if($this->debug)
		{
			$model = new CommandLogs;
			$model->command = $com;
			$model->tag = 'execCommand';
			$model->save();
		}
        
        //echo Yii::getPathOfAlias('application.config');
/*        $conffile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configfile'];
        
        // load
        $config = new CConfiguration($conffile);
        // access and setup
        $config[$command] = $arg;
        //echo $config['mySecondOption'];
        // save
        file_put_contents($conffile, "<?php return ".$config->saveAsString()."; ?>");
*/		
        Yii::app()->end();

    }
    /*
      public function actionCommand($command, $ph1 = NULL) {
      echo $command . $ph1;
      Yii::app()->end();
      } */
	public function actionShowDefaultCal(){
		$this->title = "Сохранение калибровки";
		$cal1 = Calibration::model()->findByPk(1);
		$cal2 = Calibration::model()->findByPk(2);
		$this->render('showDefaultCal', array('def' => $cal1->IsEq($cal2)));
	}
	public function actionSaveDefaultCal(){
		$cal1 = Calibration::model()->findByPk(1);
		$cal2 = Calibration::model()->findByPk(2);
		$cal2->attributes = $cal1->attributes;
		$cal2->save();
		$this->redirect(array('command/showDefaultCal'));
	}
	public function actionLoadDefaultCal(){
		$cal1 = Calibration::model()->findByPk(1);
		$cal2 = Calibration::model()->findByPk(2);
		$cal1->attributes = $cal2->attributes;
		$cal1->save();
		$this->redirect(array('command/indexNew'));
	}
    public function actionIndex() {
        $this->render('index');
    }
	
    public function actionIndexNew() {
		$this->title = "Калибровка";
		$cal1 = Calibration::model()->findByPk(1);
		$cal2 = Calibration::model()->findByPk(2);
        $this->render('indexNew', array('def' => $cal1->IsEq($cal2)));
    }
	
    public function actionCalPh() {
		$this->title = "Калибровка pH";
//		$cal = Calibration::model()->findByPk(1);
		$param1 = Params::GetParam("cal_ph_1", 4);
		$param2 = Params::GetParam("cal_ph_2", 7);
		
        $this->render('calPh', array('param1' => $param1, 'param2' => $param2));
    }
    public function actionCalTds() {
		$this->title = "Калибровка pH";
		$param1 = Params::GetParam("cal_tds_1", 100);
		$param2 = Params::GetParam("cal_tds_2", 1100);
        $this->render('calTds', array('param1' => $param1, 'param2' => $param2));
    }
	public function actionTurnPump($nb){
		$settings = Settings::model()->findByPk(1);
		$timeMls = $settings->pump_work_second * 1000;
		
		$this->doCommand("pump", $nb, $timeMls);
	}
	public function actionPumps(){
		$this->title = "Общая калибровка";
		$settings = Settings::model()->findByPk(1);

		if($settings->IsGrowing())
			$this->render('isgrowing_error', array('link_back' => array('command/indexNew')));
		else 
			$this->render('pumps');
	}
	public function actionSetTimePumps($time){
		$settings = Settings::model()->findByPk(1);
		$settings->pump_work_second = $time;
		$settings->save();
//		$this->doCommand("cal", "pump_delay", $time);
		
        $this->redirect(array('command/pumps'));
	}
	public function actionCorrectPumps($command){
		$this->title = "Насосы";
		$settings = Settings::model()->findByPk(1);
		if($settings->IsGrowing())
			$this->redirect(array('command/pumps'));
		
		
		if(isset($_GET['ml']))
		{
			$val = $_GET['ml'];
//			print_r($_GET); die();
			$model = Calibration::model()->findByPk(1);
			$settings = Settings::model()->findByPk(1);
			$k = $settings->pump_work_second * 1000 / $val;
			$k = ((int)($k * 1000)) / 1000.0;
			switch($command){
				case 'phUp' : $model->k_pump_ph_up = $k; break;
				case 'phDown' : $model->k_pump_ph_down = $k; break;
				case 'tdsA' : $model->k_pump_tds_a = $k; break;
				case 'tdsB' : $model->k_pump_tds_b = $k; break;
				case 'tdsC' : $model->k_pump_tds_c = $k; break;
				default :  break;
			}
			$model->save();
			$this->redirect(array('command/pumps', 'fromC' => $command));
		}

		$timeMls = $settings->pump_work_second * 1000;
		switch($command)
		{
			case 'phUp' : $title = '1. Работа насоса pH UP'; $this->doCommand("pump", 0, $timeMls); break;
			case 'phDown' : $title = '2. Работа насоса pH Down'; $this->doCommand("pump", 1, $timeMls); break;
			case 'tdsA' : $title = '3. Работа насоса pH tdsA'; $this->doCommand("pump", 2, $timeMls); break;
			case 'tdsB' : $title = '4. Работа насоса pH tdsB'; $this->doCommand("pump", 3, $timeMls); break;
			case 'tdsC' : $title = '5. Работа насоса pH tdsC'; $this->doCommand("pump", 4, $timeMls); break;
			default : $title = '...';
		}


		
		$this->render('correctPumps', array('command' => $command, 'title' => $title));
	}
     public function actionCal() {
        $this->render('cal');
    }
    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


 public function actionCommandonof($command, $val = NULL, $arg = NULL) {
      
            if ($command == "onof") {
                if ($val == 1) {
                    $precom = 'sudo service alfacheck.sh start';
                } else {
                    $precom = 'sudo service alfacheck.sh stop';
                }
                $com = $precom;
            } 

            echo $com;
            echo "\n";
            //echo shell_exec ( $com );   
            //echo shell_exec("whoami");
            //$stdout = shell_exec ("echo \"cool1,1,1600\" > /dev/ttyUSB1");   
            //$out = htmlspecialchars($stdout,ENT_COMPAT, 'UTF-8');

            $out = "";
            $p = proc_open($com, array(1 => array('pipe', 'w'),
                2 => array('pipe', 'w')), $io);

            while (!feof($io[1])) {
                $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
            }
            while (!feof($io[2])) {
                $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
            }

            fclose($io[1]);
            fclose($io[2]);
            proc_close($p);

            echo $out;
            $session = new CHttpSession;
            $session->open();
            $session['com'] = $precom;
            Yii::app()->end();
        
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
