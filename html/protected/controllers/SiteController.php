<?php

class SiteController extends Controller {
	public $iface = "wlan0";
	
	public function filters()
    {
        return array( 'accessControl' ); // perform access control for CRUD operations
    }
    public function accessRules() {
        return array(
			
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('login', 'logout', 'ajaxGetCurrentTime', 'test'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('serviceData', 'specialSettings'),
                'users' => array('admin'),
            ),
            array('deny', // allow all users to perform 'index' and 'view' actions
                'actions' => array('serviceData', 'specialSettings'),
                'users' => array('*'),
            ),
			array ('allow', // allow all users to perform these actions
					'users' => array('@'),
				),
			array ('allow', // allow all users to perform these actions
					'ips' => array('127.0.0.1', "::1"),
				),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	
	
	
	
	public function actionAjaxGetCurrentVal($param){
		echo Data::getCurrentValue($param);
		Yii::app()->end();
	}
	public function actionChangeWifiPoint(){
		
		$settings = Settings::model()->findByPk(1);
		if(isset($_POST['name']))
		{
			$settings->default_wifi_name_point = $_POST['name'];
			$settings->save();
			$this->redirect(array('site/internet', 'lay' => 1));
		}
		$this->render('_formName', array('title' => 'Введите название точки доступа', 'link_back' => array('site/internet', 'lay' => 1), 'hideRus' => 1, 'val' => $settings->default_wifi_name_point));
	}
	public function actionChangeWifiPointPassword(){
		
		$settings = Settings::model()->findByPk(1);
		if(isset($_POST['name']))
		{
			$settings->default_wifi_name_password_point = $_POST['name'];
			$settings->save();
			$this->redirect(array('site/internet', 'lay' => 1));
		}
		$this->render('_formName', array('title' => 'Введите пароль точки доступа', 'link_back' => array('site/internet', 'lay' => 1), 'hideRus' => 1, 'val' => $settings->default_wifi_name_password_point));
	}
	public function actionChangePassword(){
		
		if(isset($_POST['name']) && $_POST['name'] != "")
		{
			$settings = Settings::model()->findByPk(1);
			$settings->password = md5($_POST['name']);
			$settings->save();
			$this->redirect(array('site/deviceSettings'));
		}
		$this->render('_formName', array('title' => 'Введите новый пароль', 'link_back' => array('site/deviceSettings')));
	}
	public function actionAjaxUpdatePribor(){
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	
		$rpc->systemrpc_applyUpdate();
//		sleep(5);
		echo "1";
		Yii::app()->end();
	}
	public function actionAjaxAutoUpdate(){
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	
		
		$cur_autoUpdate = $rpc->systemrpc_getAutoUpgradeTimeout();
//		echo $cur_autoUpdate;die();
		$cur_ver = $rpc->systemrpc_getCurrentVersion();
		$lat_ver = $rpc->systemrpc_getLatestVersion();
		$this->renderPartial('_autoUpdate', array('cur_autoUpdate' => $cur_autoUpdate, 'lat_ver' => $lat_ver, 'cur_ver' => $cur_ver));
	}
	public function actionAutoUpdate(){
		$time_auto = 60;
		
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		if(isset($_POST['save']) && $_POST['save'] == 1)
		{
			$rpc = new Rpc();	
			$autoUpdate = (isset($_POST['autoUpdate']) && $_POST['autoUpdate'] == 1) ? $time_auto : 0; 
//			echo($autoUpdate); die();
			$rpc->systemrpc_setAutoUpgradeTimeout($autoUpdate);
			$this->redirect(array('site/autoUpdate'));
		}
		$this->render('autoUpdate');
	}
	public function actionTest(){
		$this->layout = '//layouts/columnMobile';
		$this->render('index');
		
		die();
		$mailer = new FreakMailer();
//		print_r($mailer); die();
		// Устанавливаем тему письма
		$mailer->Subject = 'Это тест';

		// Задаем тело письма
		$mailer->Body = 'Это тест моей почтовой системы!';

		// Добавляем адрес в список получателей
		$mailer->AddAddress('klinok-s@mail', 'Rusklinok');

		if(!$mailer->Send())
		{
		  echo 'Не могу отослать письмо!';
		}
		else
		{
		  echo 'Письмо отослано!';
		}
		$mailer->ClearAddresses();
		$mailer->ClearAttachments();
		
		die();
		$result = mail("klinok-s@mail.ru", "Тестовое письмо", "Текст письма \n 1-ая строчка \n 2-ая строчка \n 3-ая строчка");
		echo $result."<br>";
		die('end');
//		print_r(Yii::app()->user); die();
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	

//		$res = $rpc->systemrpc_getWirelessConfig($this->iface);
//		$res= $rpc->systemrpc_getEthernetConfig("eth0");

//		print_r($config);die();
		$res = $rpc->systemrpc_getLatestVersion();
		$getCurrentVersion = $rpc->systemrpc_getCurrentVersion();
		echo "getLatestVersion - ".$res."<br/>"; 
		echo "getCurrentVersion - ".$getCurrentVersion."<br/>"; 
//		die();
//		$res = $rpc->systemrpc_setWirelessConfig($this->iface);
//		$res = $rpc->systemrpc_getWirelessConfig($this->iface);
//		$strTime = "17:00 9.11.2017";
//		echo strtotime($strTime); 
//		strt
//		$res = $rpc->systemrpc_setCurrentTimestamp(strtotime($strTime));
//		echo date("Y-m-d H:i");die();
		
//		echo $res;
//		echo $res;
		die();
	}
	public function actionAjaxGetCurrentTime(){
		echo date("H:i");
		Yii::app()->end();
	}
	public function actionTimeSettings(){
	
		$settings = Settings::model()->findByPk(1);
		if($settings->IsGrowing())
		{
			$this->render('isgrowing_error', array('link_back' => array('site/deviceSettings')));
		}
		else
		{
			require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
			$rpc = new Rpc();	
			$cur_auto = false;
			$cur_auto = $rpc->systemrpc_isNtpdEnabled();
			if(isset($_POST['save']))
			{
				$str = trim($_POST['timezone']);
				if(isset($_POST['autoUpdate']) && $_POST['autoUpdate'] == 1)
				{
					$rpc->systemrpc_setTimezone($str);
					$rpc->systemrpc_enableNtpd();
				}
				else
				{
					$rpc->systemrpc_disableNtpd();
					$rpc->systemrpc_setCurrentTimestamp(strtotime($_POST['currentDate']));
				}
				$this->redirect(array('site/deviceSettings'));
			}
			$res = $rpc->systemrpc_getTimezoneList();
			$tzList = explode("\n", $res);
			$this->render('timeSettings', array('cur_auto' => $cur_auto, 'tzList' => $tzList, 'cur_zone' => date_default_timezone_get()));
		}
	}
	public function actionTest11(){
		echo ini_get('date.timezone'); die("0");
		echo phpversion(); die();
		$date = new DateTime('now');
		echo date_timezone_get($date)->getName(); die('11');
		$cur_time_zone = date_default_timezone_get(); 
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	 
		
		$res = $rpc->systemrpc_getTimezoneList();
		$arr = explode("\n", $res);
		$cn = 0;
		foreach($arr as $v){
			if($cur_time_zone  == $v)
				echo $v."<br/>";
//			$cn++;
		}
		die('000'.$cn);
		
	}
	public function actionDeleteLAN(){
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	
		$res = $rpc->systemrpc_setWirelessConfig("wlan0");
		print_r($res);
		die('0');
	}
	public function actionCreatePoint(){
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();
		$config = array();
		$settings = Settings::model()->findByPk(1);
		
		$config['mode'] = "wap";
		$config['ssid'] = $settings->default_wifi_name_point;
//			$config['pass'] = $_POST['pass'];
		$config['pass'] = $settings->default_wifi_name_password_point;;
		$config['encryption'] = 'WPA2-PSK';
		$res = $rpc->systemrpc_setWirelessConfig("wlan0", $config);
		print_r($res);
		die('done');
	}
	public function actionAjaxGetWifiList(){
		
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	
		
		$res = $rpc->systemrpc_getWirelessConfig('wlan0');
		if(is_object($res) && $res->mode == "wlan" && $res->ip)
		{
			$this->renderPartial('_currentWiFi', array('res' => $res));
		}
		else
		{
			$res = $rpc->systemrpc_getWirelessList('wlan0');
//			print($res);die();
			if($res && count($res) > 0)
			{
				foreach ($res as $k => $v)		
				{
					$volume[$k]  = ($v->encryption == "NONE") ? 1 : 0;
					$edition[$k] = $v->strength;			
				}
				array_multisort($volume, SORT_DESC, $edition, SORT_DESC, $res);
			}
			$this->renderPartial('_wifiList', array('list' => $res));
		}
	}
	public function actionInternet($lay = NULL){
		$this->title = 'Сетевое подключение';

		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();	
//		$res = $rpc->systemrpc_setWirelessConfig($this->iface);
		$res = $rpc->systemrpc_getWirelessConfig($this->iface);
		$resLocal = $rpc->systemrpc_getEthernetConfig("eth0");
//		print_r($resLocal); die();
		if(is_object($res) && !$lay)
			if($res->mode == 'wap')
				$lay = 1;
			else if($res->mode == 'wlan' && $res->ip)
				$lay = 2;
			else if(is_object($resLocal) && $resLocal->ip)
				$lay = 3;
		
		$this->render('internet', array('lay' => $lay, 'current' => $res, 'resLocal' => $resLocal));
	}
	public function actionConnectWFFinal()
	{
		require_once( Yii::app()->basePath . "/vendor/sysapi/rpc.php" );
		$rpc = new Rpc();
		$config = array();

		$config['mode'] = "wlan";
		$config['ssid'] = $_GET['name'];
		if($_GET['encryption'] != "NONE")
			$config['pass'] = $_GET['pass'];
		$config['encryption'] = $_GET['encryption'];
		$res = $rpc->systemrpc_setWirelessConfig("wlan0", $config);
		print_r($res);
		die('done');
		
	}
	public function actionConnectWF($name, $encryption){
		$this->title = 'Сетевое подключение';
		if(isset($_POST['pass']))
		{
		}
		
		$this->render('connectWF', array('name' => $name, 'encryption' => $encryption));
	}
	public function actionDeviceSettings(){
		$this->title = 'Настройки прибора'; 
		
		$this->render('deviceSettings');
	}
	public function actionDeviceSettings2(){
		$this->title = 'Настройки прибора(2)'; 
		
		$this->render('deviceSettings2');
	}
	public function actionAjaxCheckMail(){
		$res = FreakMailer::SendEmail();
		echo $res;
	}
	public function actionAjaxSaveEmailParam($id, $val){
		$model = EmailParams::model()->findByPk($id);
		if($model)
		{
			$model->isOn = $val;
			$model->save();
			echo "success";
			
		}
		else
			echo "not found";
		Yii::app()->end();
	}
	public function actionEmailParamsSettings(){
		$this->title = 'Настройка уведомлений'; 
		$model = EmailParams::model()->findAll();
		$this->render('emailParamsSettings', array('settings' => $model));
	}
	public function actionSmtpSettings(){
		$this->title = 'Настройка почты'; 
		$this->render('smtpSettings', array('settings' => EmailSettings::model()->findByPk(1)));
	}
	public function actionChangeEmailSettings($param){ 
		$this->title = 'Настройка почты';
		$settings = EmailSettings::model()->findByPk(1);
		if(isset($_POST['name']))
		{
			$settings->{$param} = $_POST['name'];
			$settings->save();
			$this->redirect(array('site/smtpSettings'));
		}
		$titleAdd = isset($settings->attributeLabels()[$param]) ? $settings->attributeLabels()[$param] : '';
		$this->render('_formName', array('title' => "Введите ".$titleAdd, 'link_back' => array('site/smtpSettings'), 'hideRus' => 1, 'hideEng' => ($param == "time_check" || $param == "smtp_port") ? 1 : 0,
									'val' => $param == "smtp_password" ? '' : $settings->{$param}));
	}
	public function actionLightSensor(){
		$this->title = 'Настройка освещенности';
		$settings = Settings::model()->findByPk(1);
		if(isset($_GET['val']))
		{
			$settings->lightSensorType = $_GET['val'];
			$settings->save();
			$this->redirect(array('lightSensor'));
		}
		if(isset($_POST['Settings']))
		{
			$settings->attributes=$_POST['Settings'];
			if($settings->save())
				$this->redirect(array('command/indexNew'));
		}
		$this->render('lightSensor', array('settings' => $settings));
	}
	public function actionWorkingCapacity(){
		$this->title = 'Рабочая емкость';
		$model = Settings::model()->findByPk(1);
		if(isset($_POST['Settings']))
		{
			$model->attributes=$_POST['Settings'];
			if($model->save())
				$this->redirect(array('site/deviceSettings'));
		}
		
		$this->render('workingCapacity', array('model' => $model));
	}
	
	public function actionServiceData(){
		
		$model = Regulated::getRegulated();
		$this->render('serviceData', array('model' => $model));
	}
	public function actionSpecialSettings(){
		$this->title = "Специальные настройки";
		$buttons = SpSettingsBtns::model()->findAll();
		$this->render('specialSettings', array('buttons' => $buttons));
	}
	public function actionExecSpecialSettings($id){
		$button = SpSettingsBtns::model()->findByPk($id);
		if($button)
		{
			 list($controller) = Yii::app()->createController('Command');
			 $arg = 0;
			 if($button->off == 0)
				 $arg = 1;
			 else if($button->last == 1)
				 $arg = 0;
			 else 
				 $arg = 1;
				 
   			 $controller->execCommandByCron($button->type, $button->command, $arg);
			 $button->last = $arg;
			 $button->save();
		}
        if (!Yii::app()->request->isAjaxRequest)
		{
				$this->redirect(array('site/specialSettings'));
		}
		
//		$this->render('specialSettings', array('buttons' => $buttons));
	}
	
	public function actionGetValues(){
		$criteria = new CDbCriteria;
		$count = 50 * 90;
		$criteria->addCondition("date > ".(time() - 60 * 60 * 24 * 90));
		$pointsCnt = Data::model()->count($criteria);
		$cnt = (int)($pointsCnt / $count);
		$criteria->addCondition("(id % $cnt) = 0");
		$points = Data::model()->findAll($criteria);
		$result = array();
		
		foreach($points as $one)
		{
			$result[] = array('date' => date("d-m-Y H:i:s", $one->date), "value" => ($one->ph) ? $one->ph : 0);
		}
		$this->renderJSON($result);
	}
    public function actionViewOneGraph($param) {
		$model = new Data;
		if(!isset($model->getMetaData()->tableSchema->columns[$param]))
			die('error param');
		$options = array();
		$model = new Profile;
		$settings = Settings::model()->findByPk(1);
		if($settings->active_profile_id && $settings->active_profile_id > 0)
		{
			$model = Profile::model()->findByPk($settings->active_profile_id);
			$cycle = $model->getActiveCycle($settings->active_start_day);
			if($param == "wtemp")
			{
				$p_min = "tank_t_min";
				$p_max = "tank_t_max";
			}
			else if($param == "temp")
			{
				$p_min = "microclimate_t_day";
				$p_max = "microclimate_t_night";
			}
			else
			{
				$p_min = $param."_min";
				$p_max = $param."_max";
			}
			if(isset($cycle->{$p_min}))
				$options['min'] = $cycle->{$p_min};
			if(isset($cycle->{$p_max}))
				$options['max'] = $cycle->{$p_max};
			
		}
		$time0 = time() - 60 * 60 * 24 * 100;
		$cnPoint = 50;
		$time = time();
		$result = array();
		while ($time0 < $time)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = "date > $time0 AND date <= ($time0 + 60 * 60 * 24)";
			$count = Data::model()->count($criteria);
			$step = (int)($count / $cnPoint);
			$criteria->addCondition("id % $step = 0");
			$criteria->select = "date, $param";
			$points = Data::model()->findAll($criteria);
			foreach($points as $one)
			{
				if( !Data::IsErrorValue( $param, $one->{$param}))
					$result[] = array('date' => date("d-m-Y H:i:s", $one->date), "value" => ($one->{$param}) ? $one->{$param} : 0);
			}
			$time0 += 60 * 60 * 24;
		}
		
		$this->render('viewonegraph', array('result' => $result, 'param' => $param, 'options' => $options));		
	}
	public function actionGetTimeLeft(){
		$time = isset($_GET['dateTo']) ?  strtotime($_GET['dateTo'])  : 0;
		$time0 = isset($_GET['dateFrom']) ?  strtotime($_GET['dateFrom'])  : 0;
		$cnPoint = isset($_GET['cnPoint']) ? $_GET['cnPoint'] : 0; 		// per hour
		if($time > 0 && $time0 > 0 && $cnPoint > 0)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = "date > $time0 AND date <= $time";
			$cnHour = (int)(($time - $time0) / (60 * 24));
			$count = Data::model()->count($criteria);
			$step = 120 / $cnPoint;
			if($step > 1)
				$count /= $step;
			
			$sec = (int)($count / 400); // примерное время, 400 получена эксперементально
			$sec += 2; // небольшая коррекция
			$count = (int)$count;
			$str = "Время загрузки <b style = 'color:#00C1FF'>$sec сек</b>";
			if($count >= Data::POINT_LIMIT)
				$str .= "<br><b style = 'color:red'>(Лимит - ".Data::POINT_LIMIT.")</b>";
			echo $str;
		}
		else
			echo ":(";
		Yii::app()->end();
	}
	public function actionViewgraph(){
		$this->render('viewgraph');
	}
	public function actionExecWaterCommand($command){
		$path = '/home/pi/ramdisk/waterCommand';
		file_put_contents($path, $command);
		echo "success";
	}
    public function actionViewGraphRender() {

		$t0 = time();
		$max_cnRegul_point = 7;
		$time0 = time() - 60 * 60 * 24 * 40;
		$cnPoint = isset($_GET['cnPoint']) ? $_GET['cnPoint'] : 1; 		// per hour
		
		
		$time = isset($_GET['dateTo']) ?  strtotime($_GET['dateTo'])  : time();
		$time0 = isset($_GET['dateFrom']) ?  strtotime($_GET['dateFrom'])  : $time0;
		$result = array();
//		while ($time0 < $time)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = "date > $time0 AND date <= $time";
			$count = Data::model()->count($criteria);
			
//			echo (time() - $t0)."<br>";
			$cnHour = (int)(($time - $time0) / (60 * 24));
			$step = (int)(120 / $cnPoint);
			if($step > 1)
				$count /= $step;
			$criteria->limit = Data::POINT_LIMIT;
			if($step > 1)
				$criteria->addCondition("id % $step = 0");
//			$criteria->limit = 10000;
			$points = Data::model()->findAll($criteria);
//			echo (time() - $t0)."<br>";
			foreach($points as $one)
			{
				$result['rastvor']['ph'][] = array('date' => $one->date_norm, "value" => ($one->ph) ? $one->ph : 0);
				$result['rastvor']['tds'][] = array('date' => $one->date_norm, "value" => ($one->tds) ? $one->tds : 0);
				$result['rastvor']['wlevel'][] = array('date' => $one->date_norm, "value" => ($one->wlevel) ? $one->wlevel : 0);
				$result['rastvor']['wtemp'][] = array('date' => $one->date_norm, "value" => ($one->wtemp) ? $one->wtemp : 0);
				$result['climat']['co2'][] = array('date' => $one->date_norm, "value" => ($one->co2) ? $one->co2 : 0);
				$result['climat']['hum'][] = array('date' => $one->date_norm, "value" => ($one->hum) ? $one->hum : 0);
				$result['climat']['light'][] = array('date' => $one->date_norm, "value" => ($one->light) ? $one->light : 0);
				$result['climat']['temp'][] = array('date' => $one->date_norm, "value" => ($one->temp) ? $one->temp : 0);
				
			}
			$criteria = new CDbCriteria;
			$criteria->condition = "date > $time0 AND date <= $time";
			$count = RegulatedPH::model()->count($criteria);
			$step = 1;
			if($count > $max_cnRegul_point)
				$step = (int)($count / $max_cnRegul_point);
			if($step > 1)
				$criteria->addCondition("id % $step = 0");
			$pointsRegPH = RegulatedPH::model()->findAll($criteria);
//			foreach($pointsRegPH as $one)
//				echo date("Y-m-d H:i:s", $one->date)."<br/>";
//			echo count($pointsRegPH);die();
			$criteria = new CDbCriteria;
			$criteria->condition = "date > $time0 AND date <= $time";
			$count = RegulatedTDS::model()->count($criteria);
			$step = 1;
			if($count > $max_cnRegul_point)
				$step = (int)($count / $max_cnRegul_point);
			if($step > 1)
				$criteria->addCondition("id % $step = 0");
			
			$pointsRegTDS = RegulatedTDS::model()->findAll($criteria);
			
			
//			$time0 += 60 * 60 * 24;
		}

			$this->renderPartial('_viewgraph',array(
				'result'=>$result,
				'pointsRegPH' => $pointsRegPH,
				'pointsRegTDS' => $pointsRegTDS,
			), false, true);
	}
 
    public function actionIndex() {


			

	
		$result = array(
			'Баланс pH' => array('val' => Data::getCurrentValue('ph', false), 'error' => Data::getErrorValue('ph'), 'time' => Data::getLastUpdateValue('ph'),'post_text' => 'pH', 'name' => 'ph'),
			'Удобрение' => array('val' => Data::getCurrentValue('tds', false), 'valLabel' => (((int)((Data::getCurrentValue('tds', false) / 500) * 100 + 0.5)) / 100.0)." EC",'error' => Data::getErrorValue('tds'), 'time' => Data::getLastUpdateValue('tds'), 'post_text' => 'ppm', 'name' => 'tds'),
			't° раствора' => array('val' => Data::getCurrentValue('wtemp', false), 'error' => Data::getErrorValue('wtemp'), 'time' => Data::getLastUpdateValue('wtemp'), 'post_text' => '°C', 'name' => 'wtemp'),
			't° воздуха' => array('val' => Data::getCurrentValue('temp', false), 'error' => Data::getErrorValue('temp'), 'time' => Data::getLastUpdateValue('temp'), 'post_text' => '°C', 'name' => 'temp'),
			'Влажность' => array('val' => Data::getCurrentValue('hum', false), 'error' => Data::getErrorValue('hum'), 'time' => Data::getLastUpdateValue('hum'), 'post_text' => '%', 'name' => 'hum'),
			'Свет' => array('val' => Data::getCurrentValue('light', false), 'error' => Data::getErrorValue('light'), 'time' => Data::getLastUpdateValue('light'), 'post_text' => 'lux', 'name' => 'light'),
			'Вода' => array('val' => Data::getCurrentValue('wlevel', false), 'error' => Data::getErrorValue('wlevel'), 'time' => Data::getLastUpdateValue('wlevel'), 'post_text' => 'level', 'name' => 'wlevel'),
			'CO2' => array('val' => Data::getCurrentValue('co2', false), 'error' => Data::getErrorValue('co2'), 'time' => Data::getLastUpdateValue('co2'), 'post_text' => 'ppm', 'name' => 'co2'),
		);
		foreach($result as &$one)
		{
			$interval = 1; 
			$a = (int)((time() - $one['time']) / $interval);
			if($a > 4)
			{
				if($a > 9)
					$a = 9;
				$color = "#$a$a"."99"."$a$a";
			}
			else
				$color = false;
			$one['val_color'] = $color;
		}
		
		$settings = Settings::model()->findByPk(1);
		if($settings->active_profile_id && $settings->active_profile_id > 0)
		{
			foreach($result as &$one)
			{
				$model = Profile::model()->findByPk($settings->active_profile_id);
				$cycle = $model->getActiveCycleP($settings->active_start_day, true);
				$param = $one['name'];
				if($param == "wtemp")
				{
					$p_min = "tank_t_min";
					$p_max = "tank_t_max";
				}
				else if($param == "temp")
				{
					$p_min = "microclimate_t_day";
					$p_max = "microclimate_t_night";
				}
				else
				{
					$p_min = $param."_min";
					$p_max = $param."_max";
				}
				$min = -1;
				if(isset($cycle->{$p_min}))
					$min = $cycle->{$p_min};
				if(isset($cycle->{$p_max}))
					$max = $cycle->{$p_max};
				$val = trim($one['val']); if($val == "nan" || $val == "null") $val = 0;
				$one['normal'] = ($min != -1 && ($val < $min || $val > $max)) ? 0 : 1;
//				echo $one['name']."  ".$one['normal']."  ".$val."  ".$min."<br/>";
			}
//			die();
		}
		$active = $settings->active_profile_id && $settings->active_profile_id > 0;

        if (Yii::app()->request->isAjaxRequest)
			$this->renderPartial('_curValues', array('result' => $result, 'active' => $active));
		else
			$this->render('index1', array('result' => $result, 'active' => $active));
    }

    public function actionLoadLst() {
        //$list = Yii::app()->db->createCommand("SELECT * FROM data WHERE date=(SELECT max(date) FROM data)")->queryAll();
        $ramdpath = Yii::app()->params['defaults']['ramdpath'];
      //  foreach ($list as $item) {
           echo "<div>";
           //echo $item["ph"];
           echo @file_get_contents($ramdpath."ph");
           echo "</div>";
           echo "<div>";
           //echo $item["hum"];
           echo @file_get_contents($ramdpath."hum");
           echo "</div>";
           echo "<div>";
           //echo $item["temp"];
           echo @file_get_contents($ramdpath."temp");
           echo "</div>";
           echo "<div>";
           //echo $item["wtemp"];
           echo @file_get_contents($ramdpath."wtemp");
           echo "</div>";
           echo "<div>";
           //echo $item["tds"];
           echo @file_get_contents($ramdpath."TDS");
           echo "</div>";
           echo "<div>";
           //echo $item["tds"];
           echo @file_get_contents($ramdpath."light");
           echo "</div>";

           echo "<div>";
           echo @file_get_contents($ramdpath."co2");
           echo "</div>";

           echo "<div>";
           echo @file_get_contents($ramdpath."wlevel");
           echo "</div>";

        //}
        Yii::app()->end();

    }


    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
			{
				
				if($error['code'] == 404)
					$this->redirect('/');
                $this->render('error', $error);
			}
        }
    }

	 
    public function actionLogin() {
		$this->layout = "columnLogin";
		
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
//			$model->username = 'admin';
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(array('site/login'));
    }

}
