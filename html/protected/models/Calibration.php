<?php

/**
 * This is the model class for table "calibration".
 *
 * The followings are the available columns in table 'calibration':
 * @property integer $id
 * @property double $calph1
 * @property double $calph2
 * @property double $caltds1
 * @property double $caltds2
 * @property integer $cp1raw
 * @property integer $cp2raw
 * @property integer $ct1raw
 * @property integer $ct2raw
 * @property double $k_pump_tds_a
 * @property double $k_pump_tds_b
 * @property double $k_pump_tds_c
 * @property double $k_pump_ph_up
 * @property double $k_pump_ph_down
 */
class Calibration extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calibration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('calph1, calph2, caltds1, caltds2, cp1raw, cp2raw, ct1raw, ct2raw, k_pump_tds_a, k_pump_tds_b, k_pump_tds_c, k_pump_ph_up, k_pump_ph_down', 'required'),
			array('cp1raw, cp2raw, ct1raw, ct2raw', 'numerical', 'integerOnly'=>true),
			array('calph1, calph2, caltds1, caltds2, k_pump_tds_a, k_pump_tds_b, k_pump_tds_c, k_pump_ph_up, k_pump_ph_down', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, calph1, calph2, caltds1, caltds2, cp1raw, cp2raw, ct1raw, ct2raw, k_pump_tds_a, k_pump_tds_b, k_pump_tds_c, k_pump_ph_up, k_pump_ph_down', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
//			'id' => 'ID',
			'calph1' => 'Calph1',
			'calph2' => 'Calph2',
			'caltds1' => 'Caltds1',
			'caltds2' => 'Caltds2',
			'cp1raw' => 'Cp1raw',
			'cp2raw' => 'Cp2raw',
			'ct1raw' => 'Ct1raw',
			'ct2raw' => 'Ct2raw',
			'k_pump_tds_a' => 'K Pump Tds A',
			'k_pump_tds_b' => 'K Pump Tds B',
			'k_pump_tds_c' => 'K Pump Tds C',
			'k_pump_ph_up' => 'K Pump Ph Up',
			'k_pump_ph_down' => 'K Pump Ph Down',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function IsEq($model){
		foreach($this->attributeLabels() as $k => $v){
			if($this->{$k} != $model->{$k})
				return false;
		}
		return true;
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('calph1',$this->calph1);
		$criteria->compare('calph2',$this->calph2);
		$criteria->compare('caltds1',$this->caltds1);
		$criteria->compare('caltds2',$this->caltds2);
		$criteria->compare('cp1raw',$this->cp1raw);
		$criteria->compare('cp2raw',$this->cp2raw);
		$criteria->compare('ct1raw',$this->ct1raw);
		$criteria->compare('ct2raw',$this->ct2raw);
		$criteria->compare('k_pump_tds_a',$this->k_pump_tds_a);
		$criteria->compare('k_pump_tds_b',$this->k_pump_tds_b);
		$criteria->compare('k_pump_tds_c',$this->k_pump_tds_c);
		$criteria->compare('k_pump_ph_up',$this->k_pump_ph_up);
		$criteria->compare('k_pump_ph_down',$this->k_pump_ph_down);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Calibration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
