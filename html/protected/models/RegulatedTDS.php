<?php

/**
 * This is the model class for table "regulatedTDS".
 *
 * The followings are the available columns in table 'regulatedTDS':
 * @property integer $id
 * @property integer $date
 * @property string $regulatedTDS
 * @property string $TdsAOn
 * @property string $TdsBOn
 * @property string $TdsCOn
 */
class RegulatedTDS extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regulatedTDS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, regulatedTDS, TdsAOn, TdsBOn, TdsCOn', 'required'),
			array('date', 'numerical', 'integerOnly'=>true),
			array('regulatedTDS, TdsAOn, TdsBOn, TdsCOn', 'length', 'max'=>6),
			array('date_norm', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, regulatedTDS, TdsAOn, TdsBOn, TdsCOn', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'regulatedTDS' => 'Regulated Tds',
			'TdsAOn' => 'Tds Aon',
			'TdsBOn' => 'Tds Bon',
			'TdsCOn' => 'Tds Con',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	 public function getDiscription(){
		 $arr = $this->attributeLabels();
		 $str = date("Y-m-d H:i:s", $this->date);
		 if($this->regulatedTDS)
			$str .= "<br>".$arr['regulatedTDS']." : ".$this->regulatedTDS;
		if($this->TdsAOn)
			$str .= "<br>".$arr['TdsAOn']." : ".$this->TdsAOn;
		if($this->TdsBOn)
			$str .= "<br>".$arr['TdsBOn']." : ".$this->TdsBOn;
		if($this->TdsCOn)
			$str .= "<br>".$arr['TdsCOn']." : ".$this->TdsCOn;
		return $str;
	 }
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date);
		$criteria->compare('regulatedTDS',$this->regulatedTDS,true);
		$criteria->compare('TdsAOn',$this->TdsAOn,true);
		$criteria->compare('TdsBOn',$this->TdsBOn,true);
		$criteria->compare('TdsCOn',$this->TdsCOn,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegulatedTDS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
