<?php

/**
 * This is the model class for table "data_min".
 *
 * The followings are the available columns in table 'data_min':
 * @property integer $id
 * @property integer $date
 * @property string $ph_min
 * @property string $ph_max
 * @property string $ph_open
 * @property string $ph_close
 * @property string $hum_min
 * @property string $hum_max
 * @property string $hum_open
 * @property string $hum_close
 * @property string $temp_min
 * @property string $temp_max
 * @property string $temp_open
 * @property string $temp_close
 * @property string $wtemp_min
 * @property string $wtemp_max
 * @property string $wtemp_open
 * @property string $wtemp_close
 * @property string $tds_min
 * @property string $tds_max
 * @property string $tds_open
 * @property string $tds_close
 */
class DataMin extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_min';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'numerical', 'integerOnly'=>true),
			array('ph_min, ph_max, ph_open, ph_close', 'length', 'max'=>5),
			array('hum_min, hum_max, hum_open, hum_close, temp_min, temp_max, temp_open, temp_close, wtemp_min, wtemp_max, wtemp_open, wtemp_close, tds_min, tds_max, tds_open, tds_close', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, ph_min, ph_max, ph_open, ph_close, hum_min, hum_max, hum_open, hum_close, temp_min, temp_max, temp_open, temp_close, wtemp_min, wtemp_max, wtemp_open, wtemp_close, tds_min, tds_max, tds_open, tds_close', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'id',
			'date' => 'Date',
			'ph_min' => 'Ph min',
			'ph_max' => 'Ph max',
			'ph_open' => 'Ph open',
			'ph_close' => 'Ph close',
			'hum_min' => 'Humidity min',
			'hum_max' => 'Humidity max',
			'hum_open' => 'Humidity open',
			'hum_close' => 'Humidity close',
			'temp_min' => 'Tmperature min',
			'temp_max' => 'Tmperature max',
			'temp_open' => 'Tmperature open',
			'temp_close' => 'Tmperature close',
			'wtemp_min' => 'WaterTemp min',
			'wtemp_max' => 'WaterTemp max',
			'wtemp_open' => 'WaterTemp open',
			'wtemp_close' => 'WaterTemp close',
			'tds_min' => 'Tds min',
			'tds_max' => 'Tds max',
			'tds_open' => 'Tds open',
			'tds_close' => 'Tds close',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date);
		$criteria->compare('ph_min',$this->ph_min,true);
		$criteria->compare('ph_max',$this->ph_max,true);
		$criteria->compare('ph_open',$this->ph_open,true);
		$criteria->compare('ph_close',$this->ph_close,true);
		$criteria->compare('hum_min',$this->hum_min,true);
		$criteria->compare('hum_max',$this->hum_max,true);
		$criteria->compare('hum_open',$this->hum_open,true);
		$criteria->compare('hum_close',$this->hum_close,true);
		$criteria->compare('temp_min',$this->temp_min,true);
		$criteria->compare('temp_max',$this->temp_max,true);
		$criteria->compare('temp_open',$this->temp_open,true);
		$criteria->compare('temp_close',$this->temp_close,true);
		$criteria->compare('wtemp_min',$this->wtemp_min,true);
		$criteria->compare('wtemp_max',$this->wtemp_max,true);
		$criteria->compare('wtemp_open',$this->wtemp_open,true);
		$criteria->compare('wtemp_close',$this->wtemp_close,true);
		$criteria->compare('tds_min',$this->tds_min,true);
		$criteria->compare('tds_max',$this->tds_max,true);
		$criteria->compare('tds_open',$this->tds_open,true);
		$criteria->compare('tds_close',$this->tds_close,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DataMin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
