<?php

/**
 * This is the model class for table "regulated".
 *
 * The followings are the available columns in table 'regulated':
 * @property integer $id
 * @property integer $date
 * @property string $regulatedph
 * @property string $regulatedtds
 * @property string $ph1
 * @property string $ph2
 * @property string $reg_delay
 * @property string $tdsA
 * @property string $tdsB
 * @property string $tdsC
 * @property string $ph_delay
 * @property string $tds
 * @property string $hum
 */
class Regulated extends CActiveRecord
{
	public $maxid;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'regulated';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'numerical', 'integerOnly'=>true),
			array('regulatedph, ph1, ph2', 'length', 'max'=>4),
			array('regulatedtds, tdsA, tdsB, tdsC, ph_delay, tds, hum', 'length', 'max'=>6),
			array('reg_delay', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, regulatedph, regulatedtds, ph1, ph2, reg_delay, tdsA, tdsB, tdsC, ph_delay, tds, hum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'regulatedph' => 'Regulatedph',
			'regulatedtds' => 'Regulatedtds',
			'ph1' => 'Ph1',
			'ph2' => 'Ph2',
			'reg_delay' => 'Reg Delay',
			'tdsA' => 'Tds A',
			'tdsB' => 'Tds B',
			'tdsC' => 'Tds C',
			'ph_delay' => 'Ph Delay',
			'tds' => 'Tds',
			'hum' => 'Hum',
		);
	}
	public static function getRegulated(){
		$model = new Regulated;

		$criteria = new CDbCriteria;
		$criteria->limit = 1;
		$criteria->order = "id DESC";

		$criteria->condition='regulatedph != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->regulatedph = $product->regulatedph;
		
		$criteria->condition='regulatedtds != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
		{
			$model->regulatedtds = $product->regulatedtds;
			$model->date = $product->date;
		}
		
		$criteria->condition='PhDownOn != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->PhDownOn = $product->PhDownOn;
		
		$criteria->condition='PhUpOn != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->PhUpOn = $product->PhUpOn;
		
		$criteria->condition='TdsAOn != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->TdsAOn = $product->TdsAOn;
		
		$criteria->condition='TdsBOn != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->TdsBOn = $product->TdsBOn;
		
		$criteria->condition='TdsCOn != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->TdsCOn = $product->TdsCOn;
		
		$criteria->condition='reg_delay != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
			$model->reg_delay = $product->reg_delay;
		
		$criteria->condition='ph1 != ""';
		$product = Regulated::model()->find($criteria);
		if($product)
		{
			$model->ph1 = $product->ph1;
			$model->ph2 = $product->ph2;
			$model->tdsA = $product->tdsA;
			$model->tdsB = $product->tdsB;
			$model->tdsC = $product->tdsC;
			$model->tds = $product->tds;
			$model->ph_delay = $product->ph_delay;
		}
		return $model;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date);
		$criteria->compare('regulatedph',$this->regulatedph,true);
		$criteria->compare('regulatedtds',$this->regulatedtds,true);
		$criteria->compare('ph1',$this->ph1,true);
		$criteria->compare('ph2',$this->ph2,true);
		$criteria->compare('reg_delay',$this->reg_delay,true);
		$criteria->compare('tdsA',$this->tdsA,true);
		$criteria->compare('tdsB',$this->tdsB,true);
		$criteria->compare('tdsC',$this->tdsC,true);
		$criteria->compare('ph_delay',$this->ph_delay,true);
		$criteria->compare('tds',$this->tds,true);
		$criteria->compare('hum',$this->hum,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Regulated the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
