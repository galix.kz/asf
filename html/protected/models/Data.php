<?php

/**
 * This is the model class for table "data".
 *
 * The followings are the available columns in table 'data':
 * @property integer $id
 * @property string $date
 * @property string $ph
 * @property string $hum
 * @property string $temp
 * @property string $wtemp
 * @property string $tds
 */
class Data extends CActiveRecord
{
	const POINT_LIMIT = 10000;
	const ERROR_VAL = 65535;
	const CO2_RAZOGREV_VAL = -111;
	const CO2_ERROR_VAL = 0;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ph', 'length', 'max'=>5),
			array('hum, temp, wtemp, tds', 'length', 'max'=>4),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, ph, hum, temp, wtemp, tds', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'id',
			'date' => 'Date',
			'ph' => 'Баланс pH',
			'tds' => 'Удобрение',
			'hum' => 'Влажность',
			'temp' => 't° воздуха',
			'wtemp' => 't° раствора',
			'light' => 'Свет',
			'wlevel' => 'Вода',
			'co2' => 'CO2',
		);
	}
	static public function getCurrentValue($name_val, $notCorrectVal = true)
	{
//		if($name_val == "tds") $name_val = "TDS";
		
        $ramdpath = Yii::app()->params['defaults']['ramdpath'];// "/home/pi/ramdisk/";
		$res = @file_get_contents($ramdpath.$name_val);
		if($name_val == 'wlevel')
		{
			$res = Data::GetWaterLevelByMode();
		}
		if($notCorrectVal) return $res;
		switch($name_val)
		{
			case "wlevel":
				switch($res)
				{
					case 0: $res = 'Пусто'; break;
					case 1: $res = 'Мало'; break;
					case 2: $res = 'Средне'; break;
					case 3: $res = 'Полный'; break;
					default : $res = "Ошибка";
				}
				break;
			case "co2":
				if($res == Data::CO2_RAZOGREV_VAL)
					$res = "Разогрев";
			case "wtemp":
			case "temp":
			case "hum":
			case "light":
				if(Data::getErrorValue($name_val) == "Нет связи с датчиком")
					$res = "Ошибка";
				break;
			
		}
		return $res;
	}
	static public function getErrorValue($name_val){
        $ramdpath = Yii::app()->params['defaults']['ramdpath'];// "/home/pi/ramdisk/";
		$val = @file_get_contents($ramdpath.$name_val);
		$cal = Calibration::model()->findByPk(1);
		switch($name_val){
			case "ph":
				if($cal->calph1 <= $cal->calph2 && $cal->cp1raw >= $cal->cp2raw)
					return "Датчик ph откалиброван неправильно";
				if($cal->calph1 >= $cal->calph2 && $cal->cp1raw <= $cal->cp2raw)
					return "Датчик ph откалиброван неправильно";
				$val_raw = Data::getCurrentValue('ph_raw', true);
				break;
			case "tds":
				if($cal->caltds1 > $cal->caltds2 && $cal->ct1raw < $cal->ct2raw)
					return "Датчик tds откалиброван неправильно";
				if($cal->caltds1 < $cal->caltds2 && $cal->ct1raw > $cal->ct2raw)
					return "Датчик tds откалиброван неправильно";
				$val_raw = Data::getCurrentValue('tds_raw', true);
				break;
			case "wtemp":
				if($val == -1100)
					return "Нет связи с датчиком";
				break;
			case "temp":
			case "hum":
				if($val == 998)
					return "Нет связи с датчиком";
				break;
			case "light":
				if($val == (int)(Data::ERROR_VAL / 1.2))
					return "Нет связи с датчиком";
				break;
			case "co2":
				if($val == 0)
					return "Нет связи с датчиком";
				break;
				
		}
		return NULL;
		
	}
	static public function IsErrorValue($name_val, $val){
		$array_error = array(
			'wtemp' => -1100,
			'temp' => 998,
			'hum' => 998,
			'light' => (int)(Data::ERROR_VAL / 1.2),
			'co2' => 0
		);
		if(isset($array_error[$name_val]))
			if( $array_error[$name_val] == $val )
				return "Нет связи с датчиком";
		return NULL;
	}
	static public function GetWaterLevelByMode(){
		$mode = 1;
		if($mode == 1)
		{
			$wLevel = array();
			$wLevel[0] = Data::getCurrentValue('wlvl1', true);
			$wLevel[1] = Data::getCurrentValue('wlvl2', true);
			$wLevel[2] = Data::getCurrentValue('wlvl3', true);
			
			$norm_lvl = false;
			if($wLevel[2] == $norm_lvl && $wLevel[1] == $norm_lvl && $wLevel[0] == $norm_lvl)
				return 3;
			else if($wLevel[2] != $norm_lvl && $wLevel[1] == $norm_lvl && $wLevel[0] == $norm_lvl)
				return 2;
			else if($wLevel[2] != $norm_lvl && $wLevel[1] != $norm_lvl && $wLevel[0] == $norm_lvl)
				return 1;
			else if($wLevel[2] != $norm_lvl && $wLevel[1] != $norm_lvl && $wLevel[0] != $norm_lvl)
				return 0;
			return Data::ERROR_VAL;
		}
		return Data::ERROR_VAL;
	}
	static public function getLastUpdateValue($name_val)
	{
//		if($name_val == "tds") $name_val = "TDS";
        $ramdpath = Yii::app()->params['defaults']['ramdpath'];// "/home/pi/ramdisk/";
		if(file_exists($ramdpath.$name_val))
			$res = filemtime($ramdpath.$name_val);
		else 
			$res = NULL;
//		return 1511860567;
		return $res;
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ph',$this->ph,true);
		$criteria->compare('hum',$this->hum,true);
		$criteria->compare('temp',$this->temp,true);
		$criteria->compare('wtemp',$this->wtemp,true);
		$criteria->compare('tds',$this->tds,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Data the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
