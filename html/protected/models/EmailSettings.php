<?php

/**
 * This is the model class for table "emailSettings".
 *
 * The followings are the available columns in table 'emailSettings':
 * @property integer $id
 * @property string $smtp_host
 * @property integer $smtp_port
 * @property string $smtp_username
 * @property string $smtp_password
 * @property integer $time_check
 * @property integer $time_last
 */
class EmailSettings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emailSettings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//			array('smtp_host, smtp_port, smtp_username, smtp_password, time_check, time_last', 'required'),
			array('smtp_port, time_check, time_last', 'numerical', 'integerOnly'=>true),
			array('smtp_host, smtp_username, smtp_password', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, smtp_host, smtp_port, smtp_username, smtp_password, time_check, time_last', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'smtp_host' => 'Smtp Хост',
			'smtp_port' => 'Smtp Порт',
			'smtp_username' => 'Логин (example@mail.ru)',
			'smtp_password' => 'Пароль',
			'time_check' => 'Time Check',
			'time_last' => 'Time Last',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('smtp_host',$this->smtp_host,true);
		$criteria->compare('smtp_port',$this->smtp_port);
		$criteria->compare('smtp_username',$this->smtp_username,true);
		$criteria->compare('smtp_password',$this->smtp_password,true);
		$criteria->compare('time_check',$this->time_check);
		$criteria->compare('time_last',$this->time_last);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
