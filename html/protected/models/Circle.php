<?php

/**
 * This is the model class for table "circle".
 *
 * The followings are the available columns in table 'circle':
 * @property integer $id
 * @property integer $profile_id
 * @property string $name
 * @property integer $day
 * @property integer $hour_light
 * @property integer $hour_dark
 * @property string $temp
 * @property string $wtemp
 * @property string $ph1
 * @property string $ph2
 * @property string $ph_delay
 * @property string $tds
 * @property string $tdsMin
 * @property string $tdsMax
 * @property string $tdsA
 * @property string $tdsB
 * @property string $tdsC
 * @property string $humMin
 * @property string $reg_delay
 *
 * The followings are the available model relations:
 * @property Profile $profile
 */
class Circle extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'circle';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profile_id, name, day, hour_light, hour_dark', 'required'),
			array('profile_id, day, hour_light, hour_dark', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('temp, wtemp, ph1, ph2, ph_delay, tds, tdsMin, tdsMax, tdsA, tdsB, tdsC, humMin, reg_delay', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, profile_id, name, day, hour_light, hour_dark, temp, wtemp, ph1, ph2, ph_delay, tds, tdsMin, tdsMax, tdsA, tdsB, tdsC, humMin, reg_delay', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profile' => array(self::BELONGS_TO, 'Profile', 'profile_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'profile_id' => 'Профиль',
			'name' => 'Name',
			'day' => 'Дни от начала цикла',
			'hour_light' => 'Часов света',
			'hour_dark' => 'Часов темноты',
			'temp' => 'Температура воздуха',
			'wtemp' => 'Температура воды',
			'ph1' => 'Ph1',
			'ph2' => 'Ph2',
			'ph_delay' => 'ph_delay',
			'tds' => 'Tds',
			'tdsMin' => 'tds Min',
			'tdsMax' => 'tds Max',
			'tdsA' => 'tdsA',
			'tdsB' => 'tdsB',
			'tdsC' => 'tdsC',
			'humMin' => 'Humidity Min',
			'reg_delay' => 'reg_delay',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('day',$this->day);
		$criteria->compare('hour_light',$this->hour_light);
		$criteria->compare('hour_dark',$this->hour_dark);
		$criteria->compare('temp',$this->temp,true);
		$criteria->compare('wtemp',$this->wtemp,true);
		$criteria->compare('ph1',$this->ph1,true);
		$criteria->compare('ph2',$this->ph2,true);
		$criteria->compare('ph_delay',$this->ph_delay,true);
		$criteria->compare('tds',$this->tds,true);
		$criteria->compare('tdsMin',$this->tdsMin,true);
		$criteria->compare('tdsMax',$this->tdsMax,true);
		$criteria->compare('tdsA',$this->tdsA,true);
		$criteria->compare('tdsB',$this->tdsB,true);
		$criteria->compare('tdsC',$this->tdsC,true);
		$criteria->compare('humMin',$this->humMin,true);
		$criteria->compare('reg_delay',$this->reg_delay,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Circle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
