<?php

/**
 * This is the model class for table "cycle".
 *
 * The followings are the available columns in table 'cycle':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $day_count
 * @property double $ph_min
 * @property double $ph_max
 * @property integer $ph_ml
 * @property integer $soil_a
 * @property integer $soil_b
 * @property integer $soil_c
 * @property integer $tds_min
 * @property integer $tds_max
 * @property integer $tds_gradual
 * @property integer $tank_t_min
 * @property integer $tank_t_max
 * @property integer $tank_v
 * @property string $light_from
 * @property string $light_to
 * @property integer $light_gradual
 * @property integer $hum_min
 * @property integer $hum_max
 * @property integer $microclimate_t_day
 * @property integer $microclimate_t_night
 * @property integer $co2_min
 * @property integer $co2_max
 */
class Cycle extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cycle';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('parent_id, day_count, ph_min, ph_max, ph_ml, soil_a, soil_b, soil_c, tds_min, tds_max, tds_gradual, tank_t_min, tank_t_max, tank_v, light_from, light_to, light_gradual, hum_min, hum_max, microclimate_t_day, microclimate_t_night, co2_min, co2_max', 'required'),
            array('parent_id, day_count, ph_ml, soil_a, soil_b, soil_c, tds_min, tds_max,
			tds_gradual, tank_t_min, tank_t_max, tank_v, light_gradual, hum_min, hum_max, microclimate_t_day, microclimate_t_night, co2_min, co2_max', 'numerical', 'integerOnly'=>true),
			array('co2_max', 'correctMax', 'max'=>4000, 'data-slick' => '8'),
            array('ph_min, ph_max', 'numerical'),
            array('light_from, light_to, co2_time1_from, co2_time1_to, co2_time2_from, co2_time2_to, co2_time3_from, co2_time3_to,', 'length', 'max'=>10),
            array('light_from, light_to', 'correctTime', 'empty' => false, 'data-slick' => '5'),
            array('co2_time1_from, co2_time1_to, co2_time2_from, co2_time2_to, co2_time3_from, co2_time3_to', 'correctTime', 'empty' => true, 'data-slick' => '9'),
			array('ph_min', 'correctMinMax', 'val' => 'ph_max', 'data-slick' => '1'),
			array('tds_min', 'correctMinMax', 'val' => 'tds_max', 'data-slick' => '3'),
			array('tank_t_min', 'correctMinMax', 'val' => 'tank_t_max', 'data-slick' => '4'),
			array('hum_min', 'correctMinMax', 'val' => 'hum_max', 'data-slick' => '6'),
			array('co2_min', 'correctMinMax', 'val' => 'co2_max', 'data-slick' => '8'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent_id, day_count, ph_min, ph_max, ph_ml, soil_a, soil_b, soil_c, tds_min, tds_max, tds_gradual, tank_t_min, tank_t_max, tank_v, light_from, light_to, light_gradual, hum_min, hum_max, microclimate_t_day, microclimate_t_night, co2_min, co2_max', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent',
            'day_count' => 'Day Count',
            'ph_min' => 'Ph Min',
            'ph_max' => 'Ph Max',
            'ph_ml' => 'Ph Ml',
            'soil_a' => 'Soil A',
            'soil_b' => 'Soil B',
            'soil_c' => 'Soil C',
            'tds_min' => 'Tds Min',
            'tds_max' => 'Tds Max',
            'tds_gradual' => 'Tds Gradual',
            'tank_t_min' => 'Tank T Min',
            'tank_t_max' => 'Tank T Max',
            'tank_v' => 'Tank V',
            'light_from' => 'Режим дня Восход',
            'light_to' => 'Режим дня Закат',
            'light_gradual' => 'Режим дня Плавно',
            'hum_min' => 'Hum Min',
            'hum_max' => 'Hum Max',
            'microclimate_t_day' => 'Microclimate T Day',
            'microclimate_t_night' => 'Microclimate T Night',
            'co2_min' => 'Co2 Min',
            'co2_max' => 'Co2 Max',
			'co2_time1_from' => 'CO2 - расписание подачи от(1)',
			'co2_time1_to' => 'CO2 - расписание подачи до(1)',
			'co2_time2_from' => 'CO2 - расписание подачи от(2)',
			'co2_time2_to' => 'CO2 - расписание подачи до(2)',
			'co2_time3_from' => 'CO2 - расписание подачи от(3)',
			'co2_time3_to' => 'CO2 - расписание подачи до(3)',
        );
    }
	
	 public function correctMax($attribute, $params){
		if($this->{$attribute} > $params['max'])
			$this->addError($attribute, '<a href = "#" class = "errorSlick" data-slick = "'.$params['data-slick'].'">Значение '.$this->getAttributeLabel($attribute).' не может быть больше '.$params['max'].'</a>');
	 }
	 public function correctMinMax($attribute, $params){
		if($this->{$attribute} > $this->{$params['val']})
			$this->addError($attribute, '<a href = "#" class = "errorSlick" data-slick = "'.$params['data-slick'].'">Значение '.$this->getAttributeLabel($attribute).' не может быть больше '.$this->getAttributeLabel($params['val']).'</a>');
	 }
	 public function correctTime($attribute, $params){
		if(empty($this->{$attribute})){
			if(!$params['empty'])
				$this->addError($attribute, '<a href = "#" class = "errorSlick" data-slick = "'.$params['data-slick'].'">'.$this->getAttributeLabel($attribute)."</a>  не может быть пустым");
		}
		else
		{
			$arr = explode(":", $this->{$attribute}); 
			if(count($arr) != 2)
				$this->addError($attribute, '<a href = "#" class = "errorSlick" data-slick = "'.$params['data-slick'].'">'.$this->getAttributeLabel($attribute)."</a>  заполнен неправильно");
			else
			{
				foreach($arr as $v)
				{
					if(strlen($v) > 2 || strlen($v) < 1)
					{
						$this->addError($attribute, '<a href = "#" class = "errorSlick" data-slick = "'.$params['data-slick'].'">'.$this->getAttributeLabel($attribute)."</a>  заполнен неправильно");
						return;
					}
				}
			}
		}
				
		 
		
	 }
/*	 
	public function GetCorrectLightFrom2(){
		$settings = Settings::model()->findByPk(1);
		$cycle_next = NULL;
		if($settings->active_profile_id)
		{
			$profile = Profile::model()->findByPk($settings->active_profile_id);
			if($profile)
			{
				$index = 0;
				$cycle = $profile->getActiveCycle($settings->active_start_day, $index);
				if(isset($profile->circles[$index + 1]))
					$cycle_next = $profile->circles[$index + 1];
			}
		}
		return $this->GetCorrectLightFrom($cycle_next, $settings);
	}
	public function GetCorrectLightTo2(){
		$settings = Settings::model()->findByPk(1);
		$cycle_next = NULL;
		if($settings->active_profile_id)
		{
			$profile = Profile::model()->findByPk($settings->active_profile_id);
			if($profile)
			{
				$index = 0;
				$cycle = $profile->getActiveCycle($settings->active_start_day, $index);
				if(isset($profile->circles[$index + 1]))
					$cycle_next = $profile->circles[$index + 1];
			}
		}
		return $this->GetCorrectLightTo($cycle_next, $settings);
	}
	public function GetCorrectLightFrom($cycle_next, $settings){
		$light_from = $this->light_from;
		if($this->light_gradual && $cycle_next)
		{
			$cnDay = (int)((time() - $settings->active_start_day) / (60 * 60 * 24));
			$cnDayAll = $this->day_count;

			$to_add = $this->IsTimeDifference($this->light_from, $cycle_next->light_from);
			$light_from = $this->AddToTime($light_from, ($cnDay / $cnDayAll) * $to_add);
		}
		return $light_from;
	}
	public function GetCorrectLightTo($cycle_next, $settings){
		$light_to = $this->light_to;
		if($this->light_gradual && $cycle_next)
		{
			$cnDay = (int)((time() - $settings->active_start_day) / (60 * 60 * 24));
			$cnDayAll = $this->day_count;

			$to_add = $this->IsTimeDifference($this->light_to, $cycle_next->light_to);
			$light_to = $this->AddToTime($light_to, ($cnDay / $cnDayAll) * $to_add);
		}
		return $light_to;
	}
*/
	public function IsNightWithDelay() // Только для охлаждения света ( розетка 4 )
	{
		$min_delay = 5;
		$light_from = $this->light_from;
		$light_to = $this->light_to;
		$light_to = $this->AddToTime($light_to, $min_delay);
		return !($this->IsInTimeDiap($light_from, $light_to, date("H:i")));
	}
	public function IsNight()
	{
		$light_from = $this->light_from;
		$light_to = $this->light_to;
		return !($this->IsInTimeDiap($light_from, $light_to, date("H:i")));
	}
	public function AddToTime($time0, $addMin)
	{
		$arrT = explode(":", $time0);
		$h = (int)$arrT[0];
		$min = (int)$arrT[1];
		$min += $addMin;
		while($min >= 60)
		{
			$min -= 60;
			$h++;
		}
		while($h >= 24)
		{
			$h -= 24;
		}
		if($min < 10)
			$min = "0".$min;
		return $h.":".$min;
	}
	private function IsInTimeDiap($time1, $time2, $time)
	{
	//	echo "time1 = $time1; time2 = $time2\r\n";
		$diff = $this->IsTimeDifference($time1, $time2);
		if($diff < 0)
		{
			$timeToAdd = $this->IsTimeDifference($time1, "24:00");
//			echo "timeToAdd = $timeToAdd\r\n";die();
			return $this->IsInTimeDiap($this->AddToTime($time1, $timeToAdd), $this->AddToTime($time2, $timeToAdd), $this->AddToTime($time, $timeToAdd));
		}
//		echo "time1 = $time1; time2 = $time2\r\n";die();
		$time1 = (int)(str_replace(":", "", $time1));
		$time2 = (int)(str_replace(":", "", $time2));
		$time = (int)(str_replace(":", "", $time));
		return $time1 <= $time && $time < $time2;
	}
	public function IsTimeDifference($time1, $time2){
		$arrT = explode(":", $time1);
		$h1 = (int)$arrT[0];
		$min1 = (int)$arrT[1];
		
		$arrT = explode(":", $time2);
		$h2 = (int)$arrT[0];
		$min2 = (int)$arrT[1];
		
		return ($h2 - $h1) * 60 + $min2 - $min1;
	}	
	public function IsIncludeCo2(){
		for($i = 1; $i < 4; $i++)
		{
			$tFr = "co2_time".$i."_from";
			$tTo = "co2_time".$i."_to";
			if($this->{$tFr} && $this->{$tTo})
			{
				if($this->IsInTimeDiap($this->{$tFr}, $this->{$tTo}, date("H:i")))
				{
//					echo "111\t\n";
					return true;
				}
			}
		}
//					echo "222\t\n";
		return false;
	}
	
	
	
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('parent_id',$this->parent_id);
        $criteria->compare('day_count',$this->day_count);
        $criteria->compare('ph_min',$this->ph_min);
        $criteria->compare('ph_max',$this->ph_max);
        $criteria->compare('ph_ml',$this->ph_ml);
        $criteria->compare('soil_a',$this->soil_a);
        $criteria->compare('soil_b',$this->soil_b);
        $criteria->compare('soil_c',$this->soil_c);
        $criteria->compare('tds_min',$this->tds_min);
        $criteria->compare('tds_max',$this->tds_max);
        $criteria->compare('tds_gradual',$this->tds_gradual);
        $criteria->compare('tank_t_min',$this->tank_t_min);
        $criteria->compare('tank_t_max',$this->tank_t_max);
        $criteria->compare('tank_v',$this->tank_v);
        $criteria->compare('light_from',$this->light_from,true);
        $criteria->compare('light_to',$this->light_to,true);
        $criteria->compare('light_gradual',$this->light_gradual);
        $criteria->compare('hum_min',$this->hum_min);
        $criteria->compare('hum_max',$this->hum_max);
        $criteria->compare('microclimate_t_day',$this->microclimate_t_day);
        $criteria->compare('microclimate_t_night',$this->microclimate_t_night);
        $criteria->compare('co2_min',$this->co2_min);
        $criteria->compare('co2_max',$this->co2_max);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cycle the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}