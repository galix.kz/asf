<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property integer $id
 * @property string $name
 * @property string $comments
 *
 * The followings are the available model relations:
 * @property Circle[] $circles
 */
class Profile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>250),
			array('comments', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, comments', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'circles' => array(self::HAS_MANY, 'Cycle', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	 public function getNumberCycle($cycle){
		 $cnt = 0;
		 foreach($this->circles as $one)
		 {
			 $cnt++;
			 if($one->id == $cycle->id)
				 return $cnt;
		 }
		 return false;
	 }
	 public function getActiveCycle($srartDay, &$index = -1) // $srartDay - format time()
	 {
		 if($this->id == 1)
		 {
			 $index = 0;
			 return $this->circles[0];
		 }
		 $cnDay = (int)((time() - $srartDay) / (60 * 60 * 24));
		 $cn1 = 0;
		 foreach ($this->circles as $k => $one)
		 {
			 if($one->day_count + $cn1 > $cnDay)
			 {
				 if($index != -1)
				 {
					 $index = $k;
				 }
				 return $one;
			 }
			 $cn1 += $one->day_count;
				 
		 }
		 return NULL;
	 }
	 public function getActiveCycleP($srartDay, $correctValues = false) // $srartDay - format time(); new version getActiveCycle
	 {
//		 $correctValues = false;
		 if($this->id == 1)
		 {
			 $index = 0;
			 return $this->circles[0];
		 }
		 $cnDay = (int)((time() - $srartDay) / (60 * 60 * 24));
		 $cn1 = 0;
		 $result = NULL;
		 foreach ($this->circles as $one)
		 {
			 if($one->day_count + $cn1 > $cnDay)
			 {
				 $index = $cn1;
				 $result = $one;
				 break;
			 }
			 $cn1 += $one->day_count;
				 
		 }
		 if($result && $correctValues)
		 {
			 if(isset($this->circles[++$index]))
			 {
				
				$cycle_next = $this->circles[$index];
				$cnDay = (int)((time() - $srartDay) / (60 * 60 * 24)) - $cn1;
				$cnDayAll = $result->day_count;
				
				if($result->tds_gradual)
				{
					$result->tds_min = $result->tds_min + ($cnDay / $cnDayAll) * ($cycle_next->tds_min - $result->tds_min);
					$result->tds_max = $result->tds_max + ($cnDay / $cnDayAll) * ($cycle_next->tds_max - $result->tds_max);
				}
				if($result->light_gradual)
				{
					$to_add = $result->IsTimeDifference($result->light_from, $cycle_next->light_from);
					$result->light_from = $result->AddToTime($result->light_from, (int)(($cnDay / $cnDayAll) * $to_add));

					$to_add = $result->IsTimeDifference($result->light_to, $cycle_next->light_to);
//					echo $to_add; die();
					$result->light_to = $result->AddToTime($result->light_to, (int)(($cnDay / $cnDayAll) * $to_add));
//					echo $result->light_to; die();
				}
				
			 }
		 }
		 return $result;
	 }
	 
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'comments' => 'Comments',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comments',$this->comments,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
