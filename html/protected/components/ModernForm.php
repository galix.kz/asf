<?php 
class ModernForm extends CActiveForm
{
	public function sliderChoose($model, $atr, $minVal, $step, $cnStep = 8)
	{
		$init = -1;
		$arrVal = array();
		if(!$model->{$atr})
			$model->{$atr} = $minVal;
		for($i = 0; $i < $cnStep; $i++)
		{
			$val = $minVal + $step * $i;
			$arrVal[] = $val;
			if($val == $model->{$atr})
				$init = $i;
			
			if($init == -1 && $model->{$atr} > $val && $model->{$atr} < ($val + $step))
			{
				$arrVal[] = $model->{$atr};
				$init = $i + 1;
			}
		}
		if($init == -1)
		{
			$init = $cnStep;
			$arrVal[] = $model->{$atr};
		}
		$out = '
						<div class="select-values" data-slick=\'{"initialSlide": '.$init.'}\'>
							<div class="slide">&nbsp;'.$this->hiddenField($model,$atr).'</div>';
							foreach($arrVal as $oneVal)
								$out .= '<div class="slide"><input type = "text" class = "inputbox" value = "'.($oneVal).'"/></div>';
							$out .= '
							<div class="slide">&nbsp;</div>
							
						</div>
		';
		
		return $out;
	}
	public static function sliderChooseNoModel($minVal, $step, $cnStep = 8, $current = NULL, $attrName = '')
	{
		$init = -1;
		if($current == NULL)
			$current = $minVal;
//		$curValue = $minVal;
		$arrVal = array();
/*		if($current != NULL)
			for($i = 0; $i < $cnStep; $i++)
				if($minVal + $step * $i == $current)
				{
					$init = $i;
					$curValue = $current;
				}
*/				
		for($i = 0; $i < $cnStep; $i++)
		{
			$val = $minVal + $step * $i;
			$arrVal[] = $val;
			if($val == $current)
				$init = $i;
			
			if($init == -1 && $current > $val && $current < ($val + $step))
			{
				$arrVal[] = $current;
				$init = $i + 1;
			}
		}
		if($init == -1)
		{
			$init = $cnStep;
			$arrVal[] = $current;
		}				
				
		$out = '
						<div class="select-values" data-slick=\'{"initialSlide": '.$init.'}\'>
							<div class="slide">&nbsp;<input type = "hidden" '.($attrName ? ' name = '.$attrName : '').' value = "'.$current.'"></div>';
							foreach($arrVal as $oneVal)
								$out .= '<div class="slide"><input type = "text" class = "inputbox" value = "'.($oneVal).'"/></div>';
							$out .= '
							<div class="slide">&nbsp;</div>
							
						</div>
		';
		
		return $out;
	}
	public function sliderChooseTime($model, $atr, $hour, $cnStep = 8)
	{
		$init = 0;
		
		if(!$model->{$atr})
			$model->{$atr} = $hour.':00';
		$arrVal = array();
		$arrCur = explode(":", $model->{$atr});
		$curOur = isset($arrCur[0]) ? (int)$arrCur[0] : -1;
		$curMin = isset($arrCur[1]) ? (int)$arrCur[1] : -1;
		for($i = 0; $i < $cnStep; $i++)
		{
			$h = $hour;
			$min = $i * 10;
			while($min >= 60)
			{
				$min -= 60;
				$h++;
			}
			$min_int = $min;
			
			if($min == 0)
				$min = '00';
			$arrVal[] = "$h:$min";
			if($curOur == $h && $curMin == $min_int)
			{
				$init = $i;
			}
			else if($curOur == $h && $curMin > $min_int && $curMin < $min_int + 10)
			{
				$init = $i + 1;
				$arrVal[] = $curOur.":".($curMin < 10 ? "0".$curMin : $curMin);
			}

		}
		$out = '
						<div class="select-values" data-slick=\'{"initialSlide": '.$init.'}\'>
							<div class="slide">&nbsp;'.$this->hiddenField($model,$atr).'</div>';
							foreach($arrVal as $oneVal)
							{
								$out .= '<div class="slide"><input type = "text" class = "inputbox" value = "'.$oneVal.'"/></div>';
							}
							$out .= '
							<div class="slide">&nbsp;</div>
							
						</div>
		';
		
		return $out;
		
	}
	public function sliderChooseTimeCO2($model, $atr, $hour, $cnStep = 8)
	{
		$init = 0;
		$arrVal = array();
		if($model->{$atr})
		{
			$arrCur = explode(":", $model->{$atr});
			$curOur = isset($arrCur[0]) ? (int)$arrCur[0] : -1;
			$curMin = isset($arrCur[1]) ? (int)$arrCur[1] : -1;
		}
		for($i = 0; $i < $cnStep; $i++)
		{
			$h = $hour;
			$min = $i * 10;
			while($min >= 60)
			{
				$min -= 60;
				$h++;
			}
			$min_int = $min;
			
			if($min == 0)
				$min = '00';
			$arrVal[] = "$h:$min";
			if($model->{$atr} && $curOur == $h && $curMin == $min_int)
			{
				$init = $i + 1;
			}
			else if($model->{$atr} && $curOur == $h && $curMin > $min_int && $curMin < $min_int + 10)
			{
				$init = $i + 2;
				$arrVal[] = $curOur.":".($curMin < 10 ? "0".$curMin : $curMin);
			}
		}
		$out = '
						<div class="select-values" data-slick=\'{"initialSlide": '.$init.'}\'>
							<div class="slide">&nbsp;'.$this->hiddenField($model,$atr).'</div>
							<div class="slide" empty="1">--:--</div>
							';
							foreach($arrVal as $oneVal)
							{
								$out .= '<div class="slide"><input type = "text" class = "inputbox" value = "'.$oneVal.'"/></div>';
							}
							$out .= '
							<div class="slide">&nbsp;</div>
							
						</div>
		';
		
		return $out;
		
	}
	public function sliderChoose2($model, $atr, $values, $valName = NULL)
	{
		$init = 0;
		$cnStep = count($values);
		for($i = 0; $i < $cnStep; $i++)
			if($values[$i] == $model->{$atr})
				$init = $i;
			
		if(!$model->{$atr})
			$model->{$atr} = $values[0];
		if($valName == NULL)
			$valName = $values;
		
		$out = '
						<div class="select-values" data-slick=\'{"initialSlide": '.$init.'}\'>
							<div class="slide">&nbsp;'.$this->hiddenField($model,$atr).'</div>';
							for($i = 0; $i < $cnStep; $i++)
								$out .= '<div class="slide" data-val = "'.$values[$i].'">'.($valName[$i]).'</div>';
							$out .= '
							<div class="slide">&nbsp;</div>
							
						</div>
		';
		
		return $out;
	}	
}


?>