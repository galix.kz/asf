<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$users=array(
			// username => password
			//'demo'=>'demo',
			'admin'=>'vaduduki',
		);  
		$password = Settings::model()->findByPk(1)->password;
		$this->errorCode=self::ERROR_NONE;
		if(md5($this->password) == Settings::model()->findByPk(1)->password)
		{
			$this->username = "user";
		}
		else if((md5($this->password) == Settings::model()->findByPk(1)->super_password))
		{
			$this->username = "admin";
		}
		else
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
/*		if(md5($this->password) != $password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
*/		
		return !$this->errorCode;
	}
}