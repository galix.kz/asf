<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$pathToMailer = '/var/www/html/protected/extensions/';
require $pathToMailer.'PHPMailer/src/Exception.php';
require $pathToMailer.'PHPMailer/src/PHPMailer.php';
require $pathToMailer.'PHPMailer/src/SMTP.php';
/*
class FreakMailer2 extends PHPMailer
{
    var $priority = 3;
    var $to_name;
    var $to_email;
    var $From = null;
    var $FromName = null;
    var $Sender = null;
  
    function FreakMailer()
    {
      
		$confile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configSMTP'];
		$config = new CConfiguration($confile);
		if($config['smtp_mode'] == 'enabled')
		{
			$this->Host = $config['smtp_host'];
			$this->Port = $config['smtp_port'];
			if($config['smtp_username'] != '')
			{
				$this->SMTPAuth  = true;
				$this->Username  = $config['smtp_username'];
				$this->Password  =  $config['smtp_password'];
			}
			$this->Mailer = "smtp";
		}
		if(!$this->From)
		{
			$this->From = $config['from_email'];
		}
		if(!$this->FromName)
		{
			$this-> FromName = $config['from_name'];
		}
		if(!$this->Sender)
		{
			$this->Sender = $config['from_email'];
		}
		$this->Priority = $this->priority;
    }
}
*/
class FreakMailer
{
	static function SendEmail($message = NULL){
		$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		$config = EmailSettings::model()->findByPk(1);
		$sn = Settings::GetSN();
		try {
		//Server settings
			$mail->CharSet = 'utf-8';
			$mail->Timeout = 10;
			$mail->SMTPDebug = 0;                                 // Enable verbose debug output
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = $config->smtp_host;  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = $config->smtp_username;                 // SMTP username
			$mail->Password = $config->smtp_password;                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = $config->smtp_port;                                    // TCP port to connect to

			$mail->setFrom($config->smtp_username, 'OverGrower');
			$mail->addAddress($config->smtp_username, 'OverGrower');     // Add a recipient

			//Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = "Уведомление ( $sn )";
			
			$mail->Body    = 'Это тестовое сообщение <b>Жирное</b>';
			if($message)
				$mail->Body = $message;
//			$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		//	print_r($mail); die();
			$mail->send();
			
			$str = 1;
		} catch (Exception $e) {
			$str = 'Message could not be sent.';
			$str .= 'Mailer Error: ' . $mail->ErrorInfo;
		}
		return $str;
	}
}