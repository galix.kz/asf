<?php
//require_once( Yii::app()->basePath . "/vendor/eazy-jsonrpc/src/BaseJsonRpcClient.php" );
require_once( "/var/www/html/protected/vendor/eazy-jsonrpc/src/BaseJsonRpcClient.php" );

class Rpc extends BaseJsonRpcClient {
    private $failed = false;

    function __construct() {
        parent::__construct( "http://localhost:8080/");
        $this->UseObjectsInResults = true;
    }

    protected function processCalls( $calls ) {
        $this->failed = parent::processCalls( $calls ) === false;
    }

    public function __call( $method, $parameters = array() ) {
        $call = parent::__call( $method, $parameters );
        if ( $this->failed || $call->HasError() ) {
            echo "<!--Error: ". json_encode( $call->Error ) . "-->";
            return false;
        }
        return $call->Result;
    }

    public static function getInterfaceInfo( $interface, &$state, &$hwaddr, &$ipaddr ) {
    	$rxs = array(
    		"state ([A-Z]+) group",
    		"(\b(?:[0-9A-F]{2}:){5}[0-9A-F]{2}\b)",
    		"(\b(?:\d{1,3}\.){3}\d{1,3}\b)"
    	);

    	$iplink = shell_exec( "ip addr show $interface" );
    	$mux = array( &$state, &$hwaddr, &$ipaddr );

    	foreach ( $rxs as $index => $regex ) {
    		$matches = array();

    		if ( preg_match( "/$regex/i", $iplink, $matches ) != 0 && count( $matches ) > 1 ) {
    			$mux[ $index ] = $matches[ 1 ];
    		}

    	}

    	return $state == "UP";
    }

}


?>
