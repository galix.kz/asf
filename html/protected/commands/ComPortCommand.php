<?php 
class ComPortCommand extends CConsoleCommand {
	
	public $settings;
	public $cal;
	public $ph_array;
	public $ph_cur_nb;
	const PH_AVR_CN_POINT = 20;

	private function my_init(){
		$this->ph_array = array_fill(0, $this::PH_AVR_CN_POINT, Data::ERROR_VAL);
		$this->ph_cur_nb = 0;
		$this->refreshProfile();
		
	}
	
    public function run($args) {      
		sleep(10);
		$rampath = "/home/pi/ramdisk/";
		$path_time_last_recieve = "/home/pi/ramdisk/last_recieve";
		$fileLog = "/var/log/comPortLog.txt";


		$time0 = time();
		exec('stty -F /dev/ttyS0 115200 -echo');
		$h = fopen('/dev/ttyS0', "rb");
		if(!$h) die("Arduino not found");
		$cnt = 0; $cntLimit = 14;
		$arrayCorrectParam = array('AirTemp' => 'temp', 'Humidity' => 'hum', 'WaterTemp' => 'wtemp', 'tds1' => 'tds_min', 'hum' => 'hum_min');
		$nbStr = 0;
		$debug = true; 
		$this->my_init();
		$timeLastRefresh = time();
		
		while(true)
		{
		ob_start();
		if(time() - $timeLastRefresh > 5)
		{
			$this->refreshProfile();
			$timeLastRefresh = time();
		}
		$str = '';
		if($debug) echo date("Y-m-d H:i:s")."  Начинаем fread "."\r\n";
		while( ($c = fread($h, 1)) != chr(0xA) ) { $str .= $c; }
		file_put_contents($path_time_last_recieve, time().";".date('Y-m-d H:i:s'));
		if($debug) echo date("Y-m-d H:i:s")."  ".$str."\r\n";
		if(strlen($str) > 3)
		{
			$result = array();
			$res1 = explode(";",$str);
			foreach($res1 as $v1)
			{
				if($v1)
				{
					$v1 = trim($v1);
					$res2 = explode("=", $v1);
					$name =$res2[0];
					if(isset($arrayCorrectParam[$name]))
						$name = $arrayCorrectParam[$name];
					if($name != '' && isset($res2[1]))
					{
						$val = trim($res2[1]);
						if($val == "null" || $val == "nan")
							$val = -1;
						if($name == "Ph")
						{
							$result["ph_raw"] = $val;
							if($val != Data::ERROR_VAL)
							{
								// avr
								$val = $this->AddAndGetAvrPh($val);
								// calibration
								if($this->cal->cp2raw - $this->cal->cp1raw != 0)
									$val = ($this->cal->calph2 - $this->cal->calph1) / ($this->cal->cp2raw - $this->cal->cp1raw) * ($val - $this->cal->cp1raw) +  $this->cal->calph1;
//								$val = (($this->cal->calph2 - $this->cal->calph1) / ($this->cal->cp2raw - $this->cal->cp1raw) * $val) - ($this->cal->calph2 - $this->cal->calph1) / ($this->cal->cp2raw - $this->cal->cp1raw) * $this->cal->cp1raw + $this->cal->calph1;
								if($val < 0) $val = 0;
								else $val = ((int)($val * 100)) / 100.0;
							}
						}
						if($name == "TDS")
						{
							$result["tds_raw"] = $val;
							if($val != Data::ERROR_VAL)
							{
								// calibration
								if($this->cal->ct2raw - $this->cal->ct1raw != 0)
									$val = ($this->cal->caltds2 - $this->cal->caltds1) / ($this->cal->ct2raw - $this->cal->ct1raw) * ($val - $this->cal->ct1raw) +  $this->cal->caltds1;
//								$val = (($this->cal->caltds2 - $this->cal->caltds1) / ($this->cal->ct2raw - $this->cal->ct1raw) * $val) - ($this->cal->caltds2 - $this->cal->caltds1) / ($this->cal->ct2raw - $this->cal->ct1raw) * $this->cal->ct1raw + $this->cal->caltds1;
								if($val < 0) $val = 0;
								else $val = (int)($val);
							}
						}
						if($name == "wtemp" || $name == "temp")
						{
							$val = (int)($val * 10) / 10.0;
						}
						if($name == "hum")
						{
							if($val < 1)
								$val = 1;
							$val = (int)($val + 0.5);
						}
						if($name != '')
						$result[$name] = $val;
					}
				}
			
			}
			foreach($result as $k => $v) // сохраняем в файл
			{
				@file_put_contents($rampath. mb_strtolower ($k), $v);
			}
			if(isset($result['Ph'])) // текущие значения с датчиков
			{
				if($cnt >= $cntLimit)
				{
					$cnt = 0;
					if($this->settings->active_profile_id > 0) 
					{
						if($debug) echo date("Y-m-d H:i:s")."  Пытаемся записать в бд"."\r\n";
						$result['date'] = time();
						$result['date_norm'] = date("Y-m-d H:i:s");
						if(isset($result['curtime']))
							unset($result['curtime']);
						if(isset($result['ph_raw']))
							unset($result['ph_raw']);
						if(isset($result['tds_raw']))
							unset($result['tds_raw']);
						if(isset($result['wlvl1']))
							unset($result['wlvl1']);
						if(isset($result['wlvl2']))
							unset($result['wlvl2']);
						if(isset($result['wlvl3']))
							unset($result['wlvl3']);
						$strKeys = implode(",", array_keys($result));
						$strResult = "'".implode("','", $result)."'";
						$sql = "INSERT INTO data($strKeys) VALUES($strResult)";
						$connection=Yii::app()->db; 
						$command=$connection->createCommand($sql);
						$rowCount=$command->execute(); // execute the non-query SQL
					}
				}
				else
					$cnt++;
			}
			else if (isset($result['RegulatedPH']) || isset($result['RegulatedTds'])) // значения после регулировки
			{
				if($this->settings->active_profile_id > 0) 
				{
					if($debug) echo date("Y-m-d H:i:s")."  Пытаемся записать в бд(RegulatedPH)"."\r\n";
					$result['date'] = time();
//					$result['date_norm'] = date("Y-m-d H:i:s");
					$strKeys = implode(",", array_keys($result));
					$strResult = "'".implode("','", $result)."'";
					$tableName = isset($result['RegulatedPH']) ? "regulatedPH" : "regulatedTDS";
					$sql = "INSERT INTO $tableName ($strKeys) VALUES($strResult)";
					if($debug) echo $sql."\r\n";
					$connection=Yii::app()->db; 
					$command=$connection->createCommand($sql);
					$rowCount=$command->execute(); // execute the non-query SQL
					
//					execSql($sql);
				}
			}
		}
		$strOut = ob_get_contents();
		ob_end_clean();
		file_put_contents($fileLog, $strOut, FILE_APPEND);
		$nbStr++;
//		if(filesize($fileLog) > 10000000) // 10 мб
		if($nbStr > 20000) // 10 мб
		{
			$this->ResizeLogs($fileLog);
			$nbStr = 0;
		}			
		}
	}
	private function AddAndGetAvrPh($ph){
		$this->ph_array[$this->ph_cur_nb++] = $ph;
		if($this->ph_cur_nb >= $this::PH_AVR_CN_POINT)
			$this->ph_cur_nb = 0;
		$result = 0; $cn = 0;
		for($i = 0; $i < $this::PH_AVR_CN_POINT; $i++)
		{
			if($this->ph_array[$i] != Data::ERROR_VAL)
			{
				$result += $this->ph_array[$i];
				$cn++;
			}
		}
		return $cn == 0 ? 0 : $result / $cn;
	}
	function ResizeLogs($fileLog){
//		global $fileLog;
		$offset = 10000;
		
		$rs = fopen($fileLog,'rb');
		if ( $rs === false ) return;
			 
		fseek($rs, $offset, SEEK_END);
		$buffer = fread($rs, $offset);
		fclose($rs);		
		
		file_put_contents($fileLog, $buffer);
	}
	
	private function refreshProfile(){
		$this->settings = Settings::model()->findByPk(1);
		
		
		$this->cal = Calibration::model()->findByPk(1);
	}
}
?>