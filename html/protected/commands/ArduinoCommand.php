<?php 
class ArduinoCommand extends CConsoleCommand {
	
	public $cycle;
	public $reg_delay;
	public $pump_work_second;
	public $settings;
	public $cal;
	public $waterCommand = "COM_NONE";
	public $waterCommandParam;
	
	public $fileLog;
	public $pmode = 1;
    public $PORT = '/dev/ttyS0';
	
	
	
    public function run($args) {      
	
		sleep(3);
		$timeLastRefresh = time();
		$this->refreshProfile();
		$this->fileLog = "/var/log/arduinoCom.txt";
		$this->addLog("Start");
		file_put_contents($this->fileLog, '');
		$timeLastSend = time();
		$arrayPumps = array("PH_UP" => 0, "PH_DOWN" => 1, "TDS_A" => 2, "TDS_B" => 3, "TDS_C" => 4);
		$arrayRozetka = array(1 => false, 2 => false, 3 => false, 4 => false, 5 => false, 6 => false);
		$timeLastReg = time();
		$testNb = 0;
		while(true)
		{
//			echo $testNb++."\r\n";
			$curT = time();
			if($curT - $timeLastRefresh > 20)
			{
				$this->refreshProfile();
				$timeLastRefresh = time();
			}
// ---------Start check Regulated -----------------------------------------------			
			if($this->IsGrowing() && $this->cycle != NULL && $this->TankIsNormal())
			{
				if($curT - $timeLastReg >= $this->reg_delay * 60) // check reg_delay
				{
					$timeAdd = 0;
					$cur_ph = Data::getCurrentValue('ph');
					if($cur_ph != Data::ERROR_VAL)
					{
						$cur_ph = (float)$cur_ph;
						if($cur_ph < $this->cycle->ph_min)
						{
							$timePump = (int)($this->cal->k_pump_ph_up * $this->cycle->ph_ml);
							if($timePump > 0)
							{
								$this->doCommand("pump", $arrayPumps["PH_UP"], $timePump);
								$model = new RegulatedPH;
								$model->date = time();
								$model->date_norm = date("Y-m-d H:i:s");
								$model->regulatedPH = $cur_ph;
								$model->PhUpOn = $this->cycle->ph_ml;
								$model->save();
							}
						}
						else if($cur_ph > $this->cycle->ph_max)
						{
							$timePump = (int)($this->cal->k_pump_ph_down * $this->cycle->ph_ml);
							if($timePump > 0)
							{
								$this->doCommand("pump", $arrayPumps["PH_DOWN"], $timePump); 
								$model = new RegulatedPH;
								$model->date = time();
								$model->date_norm = date("Y-m-d H:i:s");
								$model->regulatedPH = $cur_ph;
								$model->PhDownOn = $this->cycle->ph_ml;
								$model->save();
							}
							$timeAdd += $timePump;
						}
					} 
					$cur_tds = Data::getCurrentValue('tds');
					if($cur_tds != Data::ERROR_VAL)
					{
						$cur_min_tds = $this->cycle->tds_min;

/*						if($this->cycle->tds_gradual && $this->cycle_next) // постепенной изменение
						{
							$cnDay = (int)((time() - $this->settings->active_start_day) / (60 * 60 * 24));
							$cnDayAll = $this->cycle->day_count;
							$cur_min_tds = $cur_min_tds + ($cnDay / $cnDayAll) * ($this->cycle_next->tds_min - $this->cycle->tds_min);
							
						}
*/

						
						if($cur_tds < $cur_min_tds)
						{
							$timePump = (int)($this->cal->k_pump_tds_a * $this->cycle->soil_a);
							$timePump = ($this->settings->volume_cap / 100.0) * $timePump;
							$timePump = ((int)($timePump * 100 + 0.5)) / 100.0;
							if($timePump > 0)
								$this->doCommand("pump", $arrayPumps["TDS_A"], $timePump);
							$timeAdd += $timePump;
							$timePump = (int)($this->cal->k_pump_tds_b * $this->cycle->soil_b);
							$timePump = ($this->settings->volume_cap / 100.0) * $timePump;
							$timePump = ((int)($timePump * 100 + 0.5)) / 100.0;
							if($timePump > 0)
								$this->doCommand("pump", $arrayPumps["TDS_B"], $timePump);
							$timeAdd += $timePump;
							$timePump = (int)($this->cal->k_pump_tds_c * $this->cycle->soil_c);
							$timePump = ($this->settings->volume_cap / 100.0) * $timePump;
							$timePump = ((int)($timePump * 100 + 0.5)) / 100.0;
							if($timePump > 0)
								$this->doCommand("pump", $arrayPumps["TDS_C"], $timePump);
							$timeAdd += $timePump;

							$model = new RegulatedTDS;
							$model->date = time();
							$model->date_norm = date("Y-m-d H:i:s");
							$model->regulatedTDS = $cur_tds;
							$model->TdsAOn = $this->cycle->soil_a;
							$model->TdsBOn = $this->cycle->soil_b;
							$model->TdsCOn = $this->cycle->soil_c;
							$model->save();

							
						}
						else 
						{
							// наполняем бак
						}
					}
					$timeAdd /= 1000; // mls to sec
					$timeLastReg = $curT + $timeAdd;
				}
				
			}
// ---------end check Regulated -----------------------------------------------			
// ---------Start check co2 ---------------------------------------------------		
			if(!$this->IsGrowing() || $this->cycle == NULL) // нет выращивания или нет активного профиля
			{
				$arrayRozetka[1] = false;
				$arrayRozetka[2] = false;
			}
			else
			{
				$cur_co2 = Data::getCurrentValue('co2', true);
//				echo "12\r\n";
				if(!empty($cur_co2) && $cur_co2 != Data::CO2_ERROR_VAL && $cur_co2 != Data::CO2_RAZOGREV_VAL && $this->cycle->IsIncludeCo2())
				{
//					echo "123\r\n";
					if($cur_co2 < $this->cycle->co2_min)
					{
						$arrayRozetka[1] = true;
						$arrayRozetka[2] = false;
					}
					else if ($cur_co2 >= $this->cycle->co2_min && $cur_co2 <= $this->cycle->co2_max)
					{
						$arrayRozetka[1] = false;
						$arrayRozetka[2] = false;
					}
					else if($cur_co2 > $this->cycle->co2_max)
					{
						$arrayRozetka[1] = false;
						$arrayRozetka[2] = true;
					}
				}
				else
				{
					$arrayRozetka[1] = false;
					$arrayRozetka[2] = true;
//					echo "123";die();
				}
				
			}
// ---------end check co2 -------------------------------------------------
// ---------Start check light ---------------------------------------------
			if(!$this->IsGrowing() || $this->cycle == NULL) // нет выращивания или нет активного профиля
			{
				$arrayRozetka[3] = false;
				$arrayRozetka[4] = false;
			}
			else{
				if($this->pmode == 1){
					if($this->settings->lightSensorType == 1)
					{
						if($this->cycle->IsNightWithDelay())
						{
							$arrayRozetka[3] = false;
							$arrayRozetka[4] = false;
	//						echo "2222\r\n";
						}
						else if($this->cycle->IsNight())
						{
							$arrayRozetka[3] = false;
							$arrayRozetka[4] = true;
						}
						else // day
						{
							$arrayRozetka[3] = true;
							$arrayRozetka[4] = true;
						}
					}
					else if($this->settings->lightSensorType == 2)
					{
						if($this->cycle->IsNight())
						{
							$arrayRozetka[3] = false;
							$arrayRozetka[4] = false;
						}
						else // day
						{
							$cur_lux = Data::getCurrentValue('light');
							if($cur_lux != Data::ERROR_VAL)
							{
								if($cur_lux < $this->settings->lightDosvetka)
								{
									$arrayRozetka[3] = true;
									$arrayRozetka[4] = true;
								}
								else
								{
									$arrayRozetka[3] = false;
									$arrayRozetka[4] = false;
								}
							}
						}
					}
				}
			}

// ---------end check light ------------------------------------------------
// ---------Start check WaterLevel -----------------------------------------
			$water_mode = 1;
			if($water_mode == 1)
			{
				$cur_wlevel = Data::GetWaterLevelByMode();
				$this->CheckWaterCommand();
				if($cur_wlevel == Data::ERROR_VAL)
				{
					$arrayRozetka[5] = false;
					$arrayRozetka[6] = false;
				}
				else
				{
					if($this->waterCommand == "COM_SLIV")
					{
						 if($cur_wlevel > 0)
						 {
							 $arrayRozetka[5] = true;
							 $arrayRozetka[6] = false;
						 }
						 else{
							 $arrayRozetka[5] = false;
							 $arrayRozetka[6] = false;
							 $this->waterCommand = "COM_NONE";
						 }
					}
					else if ($this->waterCommand == "COM_ZALIV")
					{
						if($cur_wlevel < 3)
						{
							 $arrayRozetka[5] = false;
							 $arrayRozetka[6] = true;
						}
						else{
							 $arrayRozetka[5] = false;
							 $arrayRozetka[6] = false;
							 $this->waterCommand = "COM_NONE";
						}
					}
					else if ($this->waterCommand == "COM_ZAMENA")
					{
						if($this->waterCommandParam == 0)
						{
							if($cur_wlevel > 0)
							{
								$arrayRozetka[5] = true;
								$arrayRozetka[6] = false;
							}
							else{
								$arrayRozetka[5] = false;
								$arrayRozetka[6] = false;
								$this->waterCommandParam = 1;
							}
						}
						if($this->waterCommandParam == 1)
						{
							if($cur_wlevel < 3)
							{
								 $arrayRozetka[5] = false;
								 $arrayRozetka[6] = true;
							}
							else{
								 $arrayRozetka[5] = false;
								 $arrayRozetka[6] = false;
								 $this->waterCommand = "COM_NONE";
							}
						}
					}
					else {
						if($this->IsGrowing())
						{
							if($cur_wlevel <= 1)
							{
								$arrayRozetka[5] = false;
								$arrayRozetka[6] = true;
							}
							if($cur_wlevel == 3)
							{
								$arrayRozetka[5] = false;
								$arrayRozetka[6] = false;
							}
						}
						else{
							$arrayRozetka[5] = false;
							$arrayRozetka[6] = false;
						}
					}
				}
			}
			
// ---------end check WaterLevel -------------------------------------------
// ---------Start correct pins ------------------------------------------------
			if(time() - $timeLastSend > 2)
			{
//				$arrayRozetka = array('1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false);
				$arrayCurrentPins = array();
				for($i = 1; $i < 7; $i++)
				{
					$cur_val_pin = (bool)Data::getCurrentValue("power$i");
					if($cur_val_pin != $arrayRozetka[$i])
					{
						$this->doCommand("set", "power$i", $arrayRozetka[$i] ? 1 : 0);
						$timeLastSend = time();
					}
				}
				
			}



// ---------end correct pins ------------------------------------------------
			sleep(1);
		}
		echo "123";
	}
	public function CheckWaterCommand(){
		$path = '/home/pi/ramdisk/waterCommand';
		if(file_exists($path)){
			$this->waterCommand = file_get_contents($path);
			$this->waterCommandParam = 0;
			unlink($path);
		}
	}
	public function TankIsNormal(){
		$wlevel = Data::GetWaterLevelByMode();
		return $wlevel == 3 || $wlevel == 2;
	}
	public function GetLevelByMode1($wLevel){
		$norm_lvl = false;
		if($wLevel[2] == $norm_lvl && $wLevel[1] == $norm_lvl && $wLevel[0] == $norm_lvl)
			return 3;
		else if($wLevel[2] != $norm_lvl && $wLevel[1] == $norm_lvl && $wLevel[0] == $norm_lvl)
			return 2;
		else if($wLevel[2] != $norm_lvl && $wLevel[1] != $norm_lvl && $wLevel[0] == $norm_lvl)
			return 1;
		else if($wLevel[2] != $norm_lvl && $wLevel[1] != $norm_lvl && $wLevel[0] != $norm_lvl)
			return 0;
		return Data::ERROR_VAL;
	}
	private function IsGrowing(){
		return $this->settings->active_profile_id != 0;
	}
/*	private function IsIncludeCo2(){
		for($i = 1; $i < 4; $i++)
		{
			$tFr = "co2_time".$i."_from";
			$tTo = "co2_time".$i."_to";
			if($this->cycle->{$tFr} && $this->cycle->{$tTo})
			{
				if($this->IsInTimeDiap($this->cycle->{$tFr}, $this->cycle->{$tTo}, date("H:i")))
				{
//					echo "111\t\n";
					return true;
				}
			}
		}
//					echo "222\t\n";
		return false;
	}
	private function IsNightWithDelay(){
		
		if($this->cycle == NULL) return false;
		
		$min_delay = 5;
//		echo $this->cycle->light_from."  ".$this->AddToTime($this->cycle->light_to, $min_delay)."\r\n";
//		echo $this->IsInTimeDiap($this->cycle->light_from, $this->AddToTime($this->cycle->light_to, $min_delay), date("H:i")) ? 1 : 0;
//		echo "\r\n";
		return !($this->IsInTimeDiap($this->cycle->light_from, $this->AddToTime($this->cycle->light_to, $min_delay), date("H:i")));
		
	}
	private function IsNight(){
		if($this->cycle == NULL) return false;
		return !($this->IsInTimeDiap($this->cycle->light_from, $this->cycle->light_to, date("H:i")));
		
	}
	private function AddToTime($time0, $addMin)
	{
		$arrT = explode(":", $time0);
		$h = (int)$arrT[0];
		$min = (int)$arrT[1];
		$min += $addMin;
		while($min >= 60)
		{
			$min -= 60;
			$h++;
		}
		while($h >= 24)
		{
			$h -= 24;
		}
		if($min < 10)
			$min = "0".$min;
		return $h.":".$min;
	}
	private function IsInTimeDiap($time1, $time2, $time)
	{
	//	echo "time1 = $time1; time2 = $time2\r\n";
		$diff = $this->IsTimeDifference($time1, $time2);
		if($diff < 0)
		{
			$timeToAdd = $this->IsTimeDifference($time1, "24:00");
//			echo "timeToAdd = $timeToAdd\r\n";die();
			return $this->IsInTimeDiap($this->AddToTime($time1, $timeToAdd), $this->AddToTime($time2, $timeToAdd), $this->AddToTime($time, $timeToAdd));
		}
//		echo "time1 = $time1; time2 = $time2\r\n";die();
		$time1 = (int)(str_replace(":", "", $time1));
		$time2 = (int)(str_replace(":", "", $time2));
		$time = (int)(str_replace(":", "", $time));
		return $time1 <= $time && $time < $time2;
	}
	private function IsTimeDifference($time1, $time2){
		$arrT = explode(":", $time1);
		$h1 = (int)$arrT[0];
		$min1 = (int)$arrT[1];
		
		$arrT = explode(":", $time2);
		$h2 = (int)$arrT[0];
		$min2 = (int)$arrT[1];
		
		return ($h2 - $h1) * 60 + $min2 - $min1;
	}
*/	
	private function refreshProfile(){
		$settings = Settings::model()->findByPk(1);
		$this->reg_delay = $settings->reg_delay;
		$this->pump_work_second = $settings->pump_work_second;
		$this->cycle = NULL;
		if($settings->active_profile_id)
		{
			$profile = Profile::model()->findByPk($settings->active_profile_id);
			if($profile)
			{
				$index = 0;
				$this->cycle = $profile->getActiveCycleP($settings->active_start_day, true);
			}
		}
		$this->settings = $settings;
		$this->cal = Calibration::model()->findByPk(1);
	}
	 public function doCommand($prefix, $command, $arg){
		$precom = $prefix . ',' . $command . ',' . $arg;
        $com = 'echo "' . $precom . '"';
        $com = $com . ' > '.$this->PORT;
        
        $out = "";
        $p = proc_open($com, array(1 => array('pipe', 'w'),
            2 => array('pipe', 'w')), $io);

        while (!feof($io[1])) {
            $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
        }
        while (!feof($io[2])) {
            $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
        }

        fclose($io[1]);
        fclose($io[2]);
        proc_close($p);
		$this->addLog($precom);
	 }
	 private function addLog($str){
		$str = date('Y-m-d H:i:s - ').$str."\r\n";
		file_put_contents($this->fileLog, $str, FILE_APPEND);
		echo $str."\r\n";
		 
	 }
	
}
?>