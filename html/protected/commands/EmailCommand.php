<?php 
class EmailCommand extends CConsoleCommand {
	
    public function run($args) {    
//		echo date("Y-m-d H:i:s  :  ")."Started\r\n";
//		die();
		$settings = Settings::model()->findByPk(1);
		$email_settings = EmailSettings::model()->findByPk(1);
		if($email_settings->time_check > 0 && $settings->IsGrowing())
		{
			$cycle = NULL;
			$profile = Profile::model()->findByPk($settings->active_profile_id);
			if($profile)
			{
				$cycle = $profile->getActiveCycle($settings->active_start_day);
			}
			if($cycle != NULL)
			{
					
				$paramsModel = EmailParams::model()->findAll("isOn > 0");
				$message = '';
				foreach($paramsModel as $one)
				{
					if($one->param == "light")
					{
						if($settings->lightSensorType == 1 && $this->IsDay($cycle)) // светокультура
						{
							$cur_val = Data::getCurrentValue($one->param);
							if($settings->lightKultura > $cur_val)
							{
								if($one->time == 0)
								{
									$one->time = time();
									$one->save();
								}
								else if ($one->time + $email_settings->time_check * 60 < time())
								{
									$pp = $settings->lightKultura;
									$message .= "Warning (днем слишком темно)<br>
									Текущее значение = $cur_val lux<br>
									Минимальное значение = $pp lux<br><br>";
									$one->time = 0;
									$one->save();
								}
							}
							else
							{
								if($one->time != 0){
									$one->time = 0;
									$one->save();
								}
							}
						}
						else
						{
							if($one->time != 0){
								$one->time = 0;
								$one->save();
							}
						}
					}
					else if($one->param == "wlevel")
					{
						$cur_val = Data::getCurrentValue($one->param, true);
						if($cur_val == 0 || $cur_val == 1) // пусто или мало
						{
							if($one->time == 0)
							{
								$one->time = time();
								$one->save();
							}
							else if($one->time + $email_settings->time_check * 60 < time())
							{
								$message .= "Уровень воды на минимуме <br>
								Текущее значение = '".Data::getCurrentValue($one->param)."'<br><br>";
								$one->time = 0;
								$one->save();
							}
						}
						else
						{
							if($one->time != 0)
							{
								$one->time = 0;
								$one->save();
							}
						}
					}
					else 
					{
						if($one->param == "co2")
						{
							if(!$this->IsIncludeCo2($cycle))
							{
								if($one->time != 0)
								{
									$one->time = 0;
									$one->save();
								}
								continue;
							}
						}
						
						$cur_val = Data::getCurrentValue($one->param);
						if($cur_val > $cycle->{$one->param_max})
						{
							if($one->time == 0)
							{
								$one->time = time();
								$one->save();
							}
							else if ($one->time + $email_settings->time_check * 60 < time())
							{
								$pp = $cycle->{$one->param_max};
								$message .= "Превышено максимальное значение параметра \"$one->name\"<br>
								Текущее значение = $cur_val<br>
								Максимальное значение = $pp<br><br>";
								$one->time = 0;
								$one->save();
							}
						}
						else if($cur_val < $cycle->{$one->param_min})
						{
							if($one->time == 0)
							{
								$one->time = time();
								$one->save();
							}
							else if ($one->time + $email_settings->time_check * 60 < time())
							{
								$pp = $cycle->{$one->param_min};
								$message .= "Превышено минимальное значение параметра \"$one->name\"<br>
								Текущее значение = $cur_val<br>
								Минимальное значение = $pp<br><br>";
								$one->time = time();
								$one->save();
							}
						}
						else 
						{
							if($one->time != 0){
								$one->time = 0;
								$one->save();
							}
						}
					}
				}
				if($message)
				{
					$res = FreakMailer::SendEmail($message);
					echo date("Y-m-d H:i:s  : ")." res = $res, message = $message\r\n";
				}
				
/*				$email_settings = EmailSettings::model()->findByPk(1);
				if($email_settings->time_check > 0 && time() > $email_settings->time_last + $email_settings->time_check * 60)
				{
					$email_settings->time_last = time();
					$email_settings->save();
					$cur_tds = Data::getCurrentValue('tds');
					$message = '';
					if($cur_tds > $cycle->tds_max)
					{
						$message .= "Превышено максимальное значение TDS.<br>
						Текущее значение = $cur_tds<br>
						Максимальное значение = $cycle->tds_max";
					}
					if($message)
					{
						$res = FreakMailer::SendEmail($message);
						echo date("Y-m-d H:i:s  :  ").$res."\r\n";
					}

					
				}
*/				
			}
		}
		else
		{
			$sql = "UPDATE emailParams SET time = 0";
			$connection=Yii::app()->db; 
			$command=$connection->createCommand($sql);
			$rowCount=$command->execute(); // execute the non-query SQL
			
		}

	}
	private function IsDay($cycle){
		if($cycle == NULL) return false;
		return $this->IsInTimeDiap($cycle->light_from, $cycle->light_to, date("H:i"));
	}
	private function AddToTime($time0, $addMin)
	{
		$arrT = explode(":", $time0);
		$h = (int)$arrT[0];
		$min = (int)$arrT[1];
		$min += $addMin;
		while($min >= 60)
		{
			$min -= 60;
			$h++;
		}
		while($h >= 24)
		{
			$h -= 24;
		}
		if($min < 10)
			$min = "0".$min;
		return $h.":".$min;
	}
	private function IsInTimeDiap($time1, $time2, $time)
	{
	//	echo "time1 = $time1; time2 = $time2\r\n";
		$diff = $this->IsTimeDifference($time1, $time2);
		if($diff < 0)
		{
			$timeToAdd = $this->IsTimeDifference($time1, "24:00");
//			echo "timeToAdd = $timeToAdd\r\n";die();
			return $this->IsInTimeDiap($this->AddToTime($time1, $timeToAdd), $this->AddToTime($time2, $timeToAdd), $this->AddToTime($time, $timeToAdd));
		}
//		echo "time1 = $time1; time2 = $time2\r\n";die();
		$time1 = (int)(str_replace(":", "", $time1));
		$time2 = (int)(str_replace(":", "", $time2));
		$time = (int)(str_replace(":", "", $time));
		return $time1 <= $time && $time < $time2;
	}
	private function IsTimeDifference($time1, $time2){
		$arrT = explode(":", $time1);
		$h1 = (int)$arrT[0];
		$min1 = (int)$arrT[1];
		
		$arrT = explode(":", $time2);
		$h2 = (int)$arrT[0];
		$min2 = (int)$arrT[1];
		
		return ($h2 - $h1) * 60 + $min2 - $min1;
	}
	private function IsIncludeCo2($cycle){
		for($i = 1; $i < 4; $i++)
		{
			$tFr = "co2_time".$i."_from";
			$tTo = "co2_time".$i."_to";
			if($cycle->{$tFr} && $cycle->{$tTo})
			{
				if($this->IsInTimeDiap($cycle->{$tFr}, $cycle->{$tTo}, date("H:i")))
				{
					return true;
				}
			}
		}
		return false;
	}
	
}
?>