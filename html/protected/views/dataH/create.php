<?php
/* @var $this DataHController */
/* @var $model DataH */

$this->breadcrumbs=array(
	'Data Hs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DataH', 'url'=>array('index')),
	array('label'=>'Manage DataH', 'url'=>array('admin')),
);
?>

<h1>Create DataH</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>