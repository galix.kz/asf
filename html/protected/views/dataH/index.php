<?php
/* @var $this DataHController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Data Hs',
);

$this->menu=array(
	array('label'=>'Create DataH', 'url'=>array('create')),
	array('label'=>'Manage DataH', 'url'=>array('admin')),
);
?>

<h1>Data Hs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
