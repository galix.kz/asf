<?php
/* @var $this DataHController */
/* @var $model DataH */

$this->breadcrumbs=array(
	'Data Hs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DataH', 'url'=>array('index')),
	array('label'=>'Create DataH', 'url'=>array('create')),
	array('label'=>'Update DataH', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DataH', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DataH', 'url'=>array('admin')),
);
?>

<h1>View DataH #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'ph_min',
		'ph_max',
		'ph_open',
		'ph_close',
		'hum_min',
		'hum_max',
		'hum_open',
		'hum_close',
		'temp_min',
		'temp_max',
		'temp_open',
		'temp_close',
		'wtemp_min',
		'wtemp_max',
		'wtemp_open',
		'wtemp_close',
		'tds_min',
		'tds_max',
		'tds_open',
		'tds_close',
	),
)); ?>
