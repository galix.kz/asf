<?php
/* @var $this DataHController */
/* @var $model DataH */

$this->breadcrumbs=array(
	'Data Hs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DataH', 'url'=>array('index')),
	array('label'=>'Create DataH', 'url'=>array('create')),
	array('label'=>'View DataH', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DataH', 'url'=>array('admin')),
);
?>

<h1>Update DataH <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>