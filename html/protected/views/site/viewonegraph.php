<?php


?>
<script>
			var chartData1 = <?php echo CJSON::encode($result);?>;

			

			AmCharts.makeChart("chartdiv", {
				type: "stock",
				dataDateFormat: "DD-MM-YYYY JJ:NN:SS",
				dataSets: [{
					fieldMappings: [{
						fromField: "value",
						toField: "value"
					}],
					dataProvider: chartData1,
					categoryField: "date",
				  
				}],

				panels: [{
					title: "Value",
					stockGraphs: [{
						id: "g1",
						lineColor: "#009DC8",
						lineThickness: 3,
						negativeBase: 6.5,
						negativeLineColor: "#00C800",
						type: "smoothedLine",
						descriptionField: "description",
						bulletBorderThickness: 1,
						bulletBorderAlpha: 1, 
						bulletSize: 3,
						bulletColor: "#00C800",
						bulletBorderColor: "#00C800",
						balloonText: "<div style='margin:5px;'>[[category]]<br>[[value]]<br>[[description]]</div>",
						dateFormat: "DD-MM-YYYY JJ:NN:SS",
						balloonColor: "#00C800",
						valueField: "value",
						bullet: "round",
						useDataSetColors: false
					}, 
					{
						id: "g2",
						lineColor: "#00C800",
						lineThickness: 3,
						lineAlpha: 0,
						negativeLineColor: "#009DC8",
						negativeBase: 4.5,
						negativeLineAlpha: 1,
						showBalloon: false,
						type: "smoothedLine",
						valueField: "value",
						useDataSetColors: false
					}],
					valueAxes: [{
						axisAlpha: 0,
						position: "left",
						fontSize: 10,
						color: "#666666",
						type: "value"
					}],
					categoryAxis: {
						minPeriod: 'ss',
						fontSize: 10,
						color: "#666666",
						dateFormats: [
							{period:'ss',format:'JJ:NN:SS'},
							{period:'mm',format:'JJ:NN:SS'},
							{period:'hh',format:'JJ:NN:SS'},
							{period:'DD',format:'DD-MM-YYYY'},
							{period:'MM',format:'DD-MM-YYYY'},
							{period:'YYYY',format:'DD-MM-YYYY'}
						],
						parseDates: true
					}
				}],
				balloon: {
					animationDuration: 0,
					fadeOutDuration: 0,
					showBullet: true
				},
				categoryAxesSettings: {
					minPeriod: "ss"
				},
				
				chartScrollbarSettings: {
					graph: "g1",
					gridAlpha: 0,
					color: "#888888",
					scrollbarHeight: 40,
					backgroundAlpha: 1,
					backgroundColor: "#F5F6F8",
					selectedBackgroundAlpha: 1,
					selectedBackgroundColor: "#ffffff",
					graphFillAlpha: 0,
					autoGridCount: true,
					selectedGraphFillAlpha: 0,
					graphLineAlpha: 1,
					graphLineColor: "#ACBDCA",
					selectedGraphLineColor: "#48B811",
					selectedGraphLineAlpha: 1,
					oppositeAxis: true,
					autoGridCount: false,
					dragIconHeight: 40,
					dragIconWidth: 2
				},
				chartCursorSettings: {
					categoryBalloonDateFormats:[
							{period:'ss',format:'DD-MM-YYYY JJ:NN:SS'}, 
							{period:'mm',format:'DD-MM-YYYY JJ:NN:SS'}, 
							{period:'hh',format:'DD-MM-YYYY JJ:NN:SS'}, 
							{period:'DD',format:'DD-MM-YYYY JJ:NN:SS'}, 
							{period:'MM',format:'DD-MM-YYYY JJ:NN:SS'}, 
							{period:'YYYY',format:'DD-MM-YYYY JJ:NN:SS'}
						],
					cursorColor: "#000000",
					valueLineEnabled: false,
					valueLineBalloonEnabled: false,
					categoryBalloonEnabled: false,
					valueBalloonsEnabled: true,
					cursorAlpha: 0,
					valueLineAlpha: 0
				},
				periodSelector: {
					inputFieldsEnabled: false,
					periodsText: "",
					periods: [{
						period: "DD",
						count: 1,
						label: "день"
					},
					{
						period: "DD",
						count: 7,
						label: "1нед"
					},
					{
						period: "MM",
						count: 1,
						label: "мес"
					},
					{
						period: "MM",
						count: 3,
						selected: true,
						label: "3мес"
					},
					{
						period: "MAX",
						label: "Все время"
					}]
				},
				panelsSettings: {
					usePrefixes: true,
					accessible: true
				},
				export: {
					enabled: false
				}
			});

</script>
<?php $arr = Data::model()->attributeLabels(); ?>
		<div class="graph-info row">
			<p class="namevalue"><span class="name"><?php if(isset($arr[$param])) echo $arr[$param];?></span> <span class="value val-online" data-val = '<?php echo $param;?>' ><?php echo Data::getCurrentValue($param);?></span></p>
			<?php if ($options) : ?>
			<p class="minmax">мин <?php echo $options['min'];?> - макс <?php echo $options['max'];?></p>
			<?php else : ?>
			<p class="minmax">&nbsp;</p>
			<?php endif; ?>
			
		</div>
		<div class="graph-block row"><div id="chartdiv" style="width:100%; height:420px;"></div></div>
		<div class="btns-btm btns-btm-param row">
			<a class="btn" href="#">Все графики</a>
			<a class="btn" href="#">Калибровка</a>
		</div>


