<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	var isConnect = false;
	$('#dalee').click(function(){
		if($('#wifi-form').find('input[name=encryption]').val() != "NONE" && $('#to_paste').val().length < 8)
		{
			alert('Пароль должен быть не менее 8 символов');
			return false;
		}
		if(isConnect)
			return false;
		isConnect = true;
		
		$('#loading').show();
		var pop = $('.botpopup');
		pop.removeClass('active');
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/connectWFFinal");?>',
			data: $('#wifi-form').serialize(),
			success: function(html){
				console.log(html);
				
				var url = '<?php echo $this->createUrl("site/internet"); ?>';
				window.location.href = url;
			}
			
		});	
		return false;
	});
<?php if($encryption == "NONE") : ?>
	$('#dalee').click();
<?php endif; ?>
});
</script>
		<div class="profile">
			<div class="plabel">
<?php if($encryption != "NONE") : ?>
				Введите пароль<br>
<?php endif; ?>
				<span>Подключение к <?php echo $name; ?></span>
			</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wifi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<input type = "hidden" name = "name" value = "<?php echo $name;?>" /> 
	<input type = "hidden" name = "encryption" value = "<?php echo $encryption;?>" />
<?php if($encryption != "NONE") : ?>
	<div class="row">
		<input type = "text" value = "" name = "pass" id = "to_paste" class = "inputbox" />
	</div>
<?php endif; ?>
	<div class="btns">
		<?php echo CHtml::link('Отмена',array('site/internet'), array('class' => 'btn btn-l')); ?>
		<?php echo CHtml::submitButton('Далее', array('class' => 'btn btn-r', 'id' => 'dalee')); ?>
	</div>
<?php $this->endWidget(); ?>
		<div class = "lay" id = "loading" style = "display:none;">
			<p style="text-align:center;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" alt="" /></p>
		
		</div>
<?php $this->renderPartial('/profile/_pop_keybo', array('hideEng' => isset($hideEng) ? $hideEng : 0, 'hideRus' => isset($hideRus) ? $hideRus : 0) ); ?>


</div><!-- form -->