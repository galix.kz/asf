
<?php if($active) : ?>
<?php foreach($result as $name => $val) : ?>
			<a href="<?php echo $this->createUrl("site/viewonegraph", array('param' => $val['name']));?>" class="itm" <?php if($val['error']) echo "style='padding-bottom:8px;'"; ?>>
				<div class="name"><?php echo $name;?><?php if($val['error']) : ?> <p class = "mini-error"><?php echo $val['error']; ?></p><?php endif; ?></div>
				<div <?php if($val['val_color']) echo "style='color:".$val['val_color']."'"; ?>  class="value <?php if(!$val['normal']) echo "warning";?>"><?php echo $val['val'];?> 
				<span><?php echo $val['post_text'];?></span>
				<?php if(isset($val['valLabel'])) : ?>
				<label><?php echo $val['valLabel']; ?></label>
				<?php endif; ?>
				</div>
			</a>
<?php endforeach; ?>
<?php else : ?>
<?php foreach($result as $name => $val) : ?>
			<div class="itm noactive">
				<div class="name"><?php echo $name;?></div>
				<div <?php if($val['val_color']) echo "style='color:".$val['val_color']."'";?>  class="value"><?php echo $val['val'];?> <span><?php echo $val['post_text'];?></span>
				<?php if(isset($val['valLabel'])) : ?>
				<label><?php echo $val['valLabel']; ?></label>
				<?php endif; ?>
				</div>
			</div>
<?php endforeach; ?>
<?php endif; ?>
