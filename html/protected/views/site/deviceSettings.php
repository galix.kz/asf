<?php
/* @var $this ProfileController */
/* @var $dataProvider CActiveDataProvider */

?>
<div class = "deviceSettings">
<div class="main-menu bold">
<?php echo CHtml::link('<div class="name">Калибровка</div>', array('command/indexNew'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Рабочая емкость</div>', array('site/workingCapacity'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Сетевое подключение</div>', array('site/internet'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Установка времени</div>', array('site/timeSettings'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Установка пароля</div>', array('site/changePassword'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Обновление прибора</div>', array('site/autoUpdate'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Настройки почты SMTP</div>', array('site/smtpSettings'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Почтовые уведомления</div>', array('site/emailParamsSettings'), array('class' => 'itm'));?>
</div>
</div>
