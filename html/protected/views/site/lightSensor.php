<script>
$(document).ready(function(){
	$('#type_internet').click(function(){
		$(this).parent().find('.botpopup').addClass('active');
		return false;
	});
	$('#otmena').click(function(){
		$(this).parents('.botpopup').removeClass('active');
		return false;
	});
	$('#submit_frm').click(function(){
		$('#settings-form-cycle').submit();
		return false;
	})
	
});
function changeLightSensor(val){
	var url = '<?php echo $this->createUrl("site/lightSensor"); ?>';
	url += '&val=' + val;
	window.location.href = url;
	
}
</script>
<div class = "lightSensor">
	<div class = "head">
		<label>Режим освещения</label> <a class = "btn" href = "#" id = "type_internet">Выбрать</a>
		<div class = "value" id = "cur_val">
		<?php if($settings->lightSensorType == 1) echo "Светокультура";?>
		<?php if($settings->lightSensorType == 2) echo "Досветка";?>
		</div>
		<div class="botpopup">
			<br/>
			<h4>Выберите способ освещения</h4>
			<br/>
			<div class = "row">
				<input type = "radio" id = "t_wifi_point" name = "lightSensor" <?php if($settings->lightSensorType == 1) echo "checked";?> value = '1' onclick = "changeLightSensor(1)" /><label for = "t_wifi_point">Светокультура</label>
			</div>
			<div class = "row">
				<input type = "radio" id = "t_wifi_connect" name = "lightSensor" <?php if($settings->lightSensorType == 2) echo "checked";?> value = '2' onclick = "changeLightSensor(2)" /><label for = "t_wifi_connect">Досветка</label>
			</div>
			<div class = "btns">
				<a href = "#" id = "otmena">ОТМЕНА</a>
			</div>
		</div>
	</div>
	<div class = "cont">
<?php if($settings->lightSensorType > 0) : ?>
<?php $form=$this->beginWidget('ModernForm', array(
	'id'=>'settings-form-cycle',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
			<div class = "top">
				Освещенность <br/>min/lux
			</div>
			<div class = "mid" >
			<?php if($settings->lightSensorType == 1) : ?>
			<?php echo $form->sliderChoose($settings, 'lightKultura', 100, 100, 20); ?>
			<?php elseif($settings->lightSensorType == 2): ?>
			<?php echo $form->sliderChoose($settings, 'lightDosvetka', 500, 500, 20); ?>
			<?php endif; ?>
			</div>
			<div class = "btns">
				<?php echo CHtml::link('НАЗАД', array('command/indexNew'), array('class' => 'btn btn-l')); ?>
				<?php echo CHtml::link('СОХРАНИТЬ', '#', array('class' => 'btn btn-r', 'id' => 'submit_frm')); ?>
			</div>
<?php $this->endWidget(); ?>	
<?php else : ?>
			<div class = "btns btns_bot">
				<?php echo CHtml::link('НАЗАД', array('command/indexNew'), array('class' => 'btn btn-l')); ?>
			</div>
<?php endif; ?>
	</div>
</div>