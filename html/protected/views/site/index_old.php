<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;

//$path = Yii::app()->controller->createUrl("site/load2sec");
//Yii::app()->clientScript->registerScript("load2sec", "$(function() {function callAjax(){ $('#myDiv').load('".$path."');} setInterval(callAjax, 2000 );});");

Yii::app()->clientScript->registerScript("ref2sec", "
$(function(){    
    window.onload = setupRefresh;

    function setupRefresh() {
      //setTimeout('refreshPage();', 5000); 
      setInterval(refreshPage, 5000 );
    }
    function refreshPage() {
       window.location = 'http://178.49.196.18/index.php?r=site/index';
    }
});    
");

?>


<?php
$list = Yii::app()->db->createCommand("SELECT * FROM data where date =(SELECT max(date) FROM data)")->queryAll();

$rsPh = array();
$rsHum = array();
$rsTemp = array();
$rsWTemp = array();
$rsTds = array();
$W = 200;
$H = 200;

foreach ($list as $item) {

    $rsPh[] = array('Label', 'Value');
    $rsPh[] = array('Ph', (float) $item['ph']);

    $rsHum[] = array('Label', 'Value');
    $rsHum[] = array('Hum', (float)$item['hum']);


    $rsTemp[] = array('Label', 'Value');
    $rsTemp[] = array('Temp', (float) $item['temp']);

    $rsWTemp[] = array('Label', 'Value');
    $rsWTemp[] = array('W Temp', (float) $item['wtemp']);
    
    $rsTds[] = array('Label', 'Value');
    $rsTds[] = array('Tds', (float) $item['tds']);

    
            
    $rawData = array(
        array('id' => $item['id'],
            'date' => $item['date'],
            'ph' => $item['ph'],
            'hum' => $item['hum'],
            'temp' => $item['temp'],
            'wtemp' => $item['wtemp'],
            'tds' => $item['tds']
        )
    );
}

// or using: $rawData=User::model()->findAll();
$arrayDataProvider = new CArrayDataProvider($rawData, array(
    'id' => 'id',
    'sort' => array(
        'attributes' => array(
            'date', 'ph', 'hum', 'temp', 'wtemp', 'tds'
        ),
    ),
        ));

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'data-grid',
    'dataProvider' => $arrayDataProvider,
    //'filter'=>$model,
    'columns' => array(
        //	'id',
        array('name' => 'date', 'type' => 'raw', 'value' => 'date("Y/m/d H:i:s", $data["date"]);'),
        'ph',
        'hum',
        'temp',
        'wtemp',
        'tds',
    )
));
?>
<center>
<div class="hzl">
<div>
<?php
$this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
    'data' => $rsHum,
    'options' => array(
        'majorTicks' => array("0", "25", "50", "75", "100"),
        'max' => 100,
        'min' => 0,
                    'greenFrom' => 40,
                    'greenTo' => 80,
                    'width' => $W,
                    'height' => $H,
                    'redFrom' => 80,
                    'redTo' => 100,
                    'yellowFrom' => 0,
                    'yellowTo' => 40,
        'minorTicks' => 25
    )
));
/*
$this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
    'data' => $rsPh,
    'options' => array(
        'majorTicks' => array("3", "4", "5", "6", "7"),
        'max' => 7,
        'min' => 3,
                    'greenFrom' => 5.5,
                    'greenTo' => 6.5,
                    'width' => $W,
                    'height' => $H,
                    'redFrom' => 6.5,
                    'redTo' => 7.5,
                    'yellowFrom' => 3,
                    'yellowTo' => 5.5,
        'minorTicks' => 10
    )
));*/
?>
    </div>
    <div>
            <?php
            $this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
                'data' => $rsPh,
                'options' => array(
                    'majorTicks' => array("5", "5.5", "6.0", "6.5", "7"),
                    'max' => 7,
                    'min' => 5,
                    'greenFrom' => 5.5,
                    'greenTo' => 6.5,
                    'width' => $W,
                    'height' => $H,
                    'redFrom' => 6.5,
                    'redTo' => 7.5,
                    'yellowFrom' => 4.5,
                    'yellowTo' => 5.5,
                    'minorTicks' => 5
                )
            ));
            ?>
        </div>
    <div>
<?php
$this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
    'data' => $rsTemp,
    'options' => array(
        'majorTicks' => array("10", "15", "20", "25", "30"),
        'max' => 30,
        'min' => 10,
        'greenFrom' => 18,
        'greenTo' => 27,
        'width' => $W,
        'height' => $H,
        'redFrom' => 27,
        'redTo' => 30,
        'yellowFrom' => 10,
        'yellowTo' => 18,
        'minorTicks' => 10
    )
));
?>
        </div>
    <div>
        <?php
            $this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
                'data' => $rsWTemp,
                'options' => array(
                    'majorTicks' => array("10", "15", "20", "25", "30"),
                    'max' => 30,
                    'min' => 10,
                    'greenFrom' => 18,
                    'greenTo' => 24,
                    'width' => $W,
                    'height' => $H,
                    'redFrom' => 24,
                    'redTo' => 30,
                    'yellowFrom' => 10,
                    'yellowTo' => 18,
                    'minorTicks' => 10
                )
            ));
            ?>
        </div>
    <div>
            <?php
            $this->widget('ext.google.HzlVisualizationChart', array('visualization' => 'Gauge', 'packages' => 'gauge',
                'data' => $rsTds,
                'options' => array(
                    'majorTicks' => array("200", "400", "600", "800", "1000"),
                    'max' => 1000,
                    'min' => 200,
                    'greenFrom' => 400,
                    'greenTo' => 800,
                    'width' => $W,
                    'height' => $H,
                    'redFrom' => 800,
                    'redTo' => 1000,
                    'yellowFrom' => 0,
                    'yellowTo' => 400,
                    'minorTicks' => 10
                )
            ));
            ?>
        </div>
    <div class="hzl_clear"></div>
</div>

</center>