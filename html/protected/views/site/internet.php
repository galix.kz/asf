<?php $settings = Settings::model()->findByPk(1); ?>
<script>
$(document).ready(function(){
	$('#type_internet').click(function(){
		$(this).parent().find('.botpopup').addClass('active');
		return false;
	});
	$('#otmena').click(function(){
		$(this).parents('.botpopup').removeClass('active');
		return false;
	});
	$('#t_wifi_point').click(function(){
		$('.lay').hide();
		$('#wifi_point').show();
		$('#cur_val').text($(this).next().text());
		var pop = $(this).parents('.botpopup');
		pop.removeClass('active');
	});
	$('#t_wifi_ethernet').click(function(){
		$('.lay').hide();
		$('#wifi_ethernet').show();
		$('#cur_val').text($(this).next().text());
		var pop = $(this).parents('.botpopup');
		pop.removeClass('active');
	});
		
	$('#t_wifi_connect').click(function(){
		$('.lay').hide();
		$('#loading').show();
		$('#cur_val').text($(this).next().text());
		var pop = $(this).parents('.botpopup');
		pop.removeClass('active');
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxGetWifiList");?>',
			success: function(html){
				$('#loading').hide();
				$('#wifi_list').show();
				$('#wifi_list').html(html);
				$('.list_href').slick({
						infinite: false,
						slidesToShow: 4,
						slidesToScroll: 4,
						vertical: true,
						verticalSwiping: true,
						prevArrow: '<a type="button" class="slick-prev slick-com">ВВЕРХ</a>',
						nextArrow: '<a type="button" class="slick-next slick-com">ВНИЗ</a>',
					});			
			}
			
		});	
	
	});
	$('#create_point').click(function(){
		$('.lay').hide();
		$('#loading').show();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/createPoint");?>',
			success: function(html){
				console.log(html);
				
				var url = '<?php echo $this->createUrl("site/internet"); ?>';
				window.location.href = url;
			}
			
		});	
		return false;		
	});
	
	$('.internet').on('click', '#otkl', function(){
		$('.lay').hide();
		$('#loading').show();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/deleteLAN");?>',
			success: function(html){
				console.log(html);
				
				var url = '<?php echo $this->createUrl("site/internet"); ?>';
				window.location.href = url;
			}
			
		});	
		return false;		
	});
	$('#wifi_list').on('click', '#refresh_list', function(){
		$('.lay').hide();
		$('#loading').show();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxGetWifiList");?>',
			success: function(html){
				$('#loading').hide();
				$('#wifi_list').show();
				$('#wifi_list').html(html);
				$('.list_href').slick({
						infinite: false,
						slidesToShow: 4,
						slidesToScroll: 4,
						vertical: true,
						verticalSwiping: true,
						prevArrow: '<a type="button" class="slick-prev slick-com">ВВЕРХ</a>',
						nextArrow: '<a type="button" class="slick-next slick-com">ВНИЗ</a>',
					});			
			}
			
		});	
		return false;
	});
	<?php if ($lay == 1) :?>
		$('#t_wifi_point').click();
	<?php elseif ($lay == 2) : ?>
		$('#t_wifi_connect').click();
	<?php elseif ($lay == 3) : ?>
		$('#t_wifi_ethernet').click();
	<?php endif; ?>
});
</script>

<div class = "internet">
	<div class = "head">
		<label>Способ подключения</label> <a class = "btn" href = "#" id = "type_internet">Выбрать</a>
		<div class = "value" id = "cur_val">Не выбрано</div>
		<div class="botpopup">
			<br/>
			<h4>Выберите способ подключения</h4>
			<br/>
			<div class = "row">
				<input type = "radio" id = "t_wifi_point" name = "wifi" /><label for = "t_wifi_point">Точка доступа Wi-Fi</label>
			</div>
			<div class = "row">
				<input type = "radio" id = "t_wifi_connect" name = "wifi" /><label for = "t_wifi_connect">Подключение по Wi-Fi</label>
			</div>
			<div class = "row">
				<input type = "radio" id = "t_wifi_ethernet" name = "wifi" /><label for = "t_wifi_ethernet">Подключение по Ethernet</label>
			</div>
			<div class = "btns">
				<a href = "#" id = "otmena">ОТМЕНА</a>
			</div>
		</div>
	</div>
	<div class = "cont">
		<div class = "lay" id = "loading">
			<p style="text-align:center;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" alt="" /></p>
		
		</div>
		
		<div id = "wifi_list" class = "lay">
		</div>
		<div id = "wifi_point" class = "lay">
<?php if (is_object($current) && $current->mode == "wap" && $current->ip) : ?>
	<label>Состояние - <b style = "color:green;">Подключен к сети</b></label><br/><br/>
			Название - <?php echo $current->ssid; ?><br/><br/>
			Пароль - <?php echo $current->pass;?><br/><br/>
			Безопасность - <?php echo $current->encryption;?><br/><br/>
			Ip адрес - <?php echo $current->ip; ?><br/><br/>
			<a class = "btn" href = "#" id = "otkl">Отключить</a>
<?php else : ?>
			<div class = "row">
				<label>Название сети</label> <?php echo CHtml::link('Выбрать', array('site/changeWifiPoint'), array('class' => 'btn')); ?>
				<div class = "value"><?php echo $settings->default_wifi_name_point;?></div>
			</div>
			<div class = "row">
				<label>Безопасность</label> 
				<div class = "value">WPA2-PSK</div>
			</div>
			<div class = "row">
				<label>Пароль</label> <?php echo CHtml::link('Выбрать', array('site/changeWifiPointPassword'), array('class' => 'btn')); ?> 
				<div class = "value"><?php echo $settings->default_wifi_name_password_point;?></div>
			</div>
			<div class = "rowBtns">
				<a class = "btn" id = "create_point">Создать</a>
			</div>
<?php endif; ?>			
		</div>
		<div id = "wifi_ethernet" class = "lay">
		<?php if (is_object($resLocal) && $resLocal->ip) : ?>
			<label style = "font-size: 23px;">Состояние - <b style = "color:green;">подключен по кабелю</b></label><br/><br/>
			<div class = "row">
				<label>IP-адрес</label> 
				<div class = "value"><?php echo $resLocal->ip;?></div>
			</div>
			<div class = "row">
				<label>MAC-адрес</label> 
				<div class = "value"><?php echo $resLocal->mac;?></div>
			</div>
			<?php else : ?>
			<label style = "font-size: 23px;">Состояние - <b style = "color:red;">кабель не подключен</b></label><br/><br/>
			<?php endif; ?>
		</div>
	</div>
</div>