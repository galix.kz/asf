<?php

/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */
/*
$regdata = Regulated::model()->findAll(array('order' => 'date', 
            //'condition' => 'FROM_UNIXTIME(date) >= NOW() - INTERVAL 12 HOUR',
            'condition'=>'date > (SELECT max(date) FROM data)-604800',        
            )
        );
*/

Yii::app()->clientScript->registerScript("lastload", "

$(function(){    

    window.onload = setupRefresh;



    function setupRefresh() {

      //setTimeout('refreshPage();', 5000); 
      refreshPage();
      setInterval(refreshPage, 5000 );

    }



    

    

    function feach(last_val, val, i, min, max){

            /*

            if (last_val>val){

                $('#img'+i).removeClass('s_img2');

                $('#img'+i).removeClass('s_img3');

                $('#img'+i).addClass('s_img1');

            } else if (last_val<val){

                $('#img'+i).removeClass('s_img1');

                $('#img'+i).removeClass('s_img2');

                $('#img'+i).addClass('s_img3');

            } else{

                $('#img'+i).removeClass('s_img1');

                $('#img'+i).removeClass('s_img3');

                $('#img'+i).addClass('s_img2');

            }

            */

            if (val>min && val<max){

                $('#sensor'+i).removeClass('red');

                $('#img'+i).removeClass('red');

                $('#img'+i).addClass('s_img2');

            } else if (val<min){

                $('#img'+i).addClass('s_img3');

                $('#sensor'+i).addClass('red');

                $('#img'+i).addClass('red');

            } else if (val>max){

                $('#img'+i).addClass('s_img1');

                $('#sensor'+i).addClass('red');

                $('#img'+i).addClass('red');

            }



    }



    var last_ph;

    var last_hum;

    var last_temp;

    var last_wtemp;

    var last_tds;
    
    var last_light;

    var last_co2;

    var last_wlevel;


    

    function refreshPage() {

       $(document).ready(function(){

       $('#lasttable').load('/index.php?r=site/loadTable', 
        function() {
            $('#ph1_val').html($('#ph1').html());
            $('#ph2_val').html($('#ph2').html());
	}
       );
       
       
       var milliseconds = new Date().getTime();

    //   $('#snapshot_img').prop('src', 'http://178.49.196.18:310/?action=snapshot&t='+milliseconds);

        $('#lastload').load('/index.php?r=site/loadLst', {}, 

            function(){

           

            $('#lastload div').each(function (i) {

    		if ( i !== 7 ) {
                    $('#sensor'+i).html($(this).html());
		} else {
		    var n = 0;
		    try {
			n = parseInt($(this).html());
		    } catch (e) {
			n = -1;
		    }
		    var sensor_html = 'null';

		    switch ( n ) {
			case 0:
			    sensor_html = 'Empty';
			    break;
                        case 1:
			    sensor_html = 'Low';
                            break;
                        case 2:
			    sensor_html = 'Medium';
                            break;
                        case 3:
			    sensor_html = 'High';
                            break;

	    	    }
		    $('#sensor7').html(sensor_html);
	        } 

                if (i==0){

                    ph = $(this).html();
                    feach(last_ph, ph, i, $('#ph1').html(), $('#ph2').html());

                    last_ph = ph;

                } else if (i==1){
 
                    hum = $(this).html();

                    feach(last_hum, hum, i, 40, 80);

                    last_hum = hum;

                } else if (i==2){

                    temp = $(this).html();

                    feach(last_temp, temp, i, 18, 28);

                    last_temp = temp;

                } else if (i==3){

                    wtemp = $(this).html();

                    feach(last_wtemp, wtemp, i, 18, 24);

                    last_wtemp = wtemp;

                } else if (i==4){

                    tds = $(this).html();

                    feach(last_tds, tds, i, 100, 300);

                    last_tds = tds;

                } else if (i==5){

                    light = $(this).html();

                    feach(last_light, light, i, 0, 100000);

                    last_light = light;

                } else if (i==6){

                    co2 = $(this).html();

                    feach(last_co2, co2, i, 0, 10000);

                    last_co2 = co2;


                } else if (i==7){

                    wlevel = $(this).html();

                    feach(last_wlevel, wlevel, i, 0, 3);

                    last_wlevel = wlevel;
                
		}



            });            





            }

        );

       }); 

    }

    

    $(document).ready(function(){

        //$('#lastload').load('/index.php?r=site/loadLast');

    }); 

});    

");

?>

<div id="lasttable" class="lasttable" style="display:none;"></div>

<div id="lastload" style="display:none;"></div>

<div class="container">

<div class="sensor">
	<div class="indication">
    	<div class="sinf">pH</div>
        <div class="sinf-mini">pH</div>
    	<div id="sensor0" class="sdata"></div>
        
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">TDS</div>
        <div class="sinf-mini">ppm</div>
    	<div id="sensor4" class="sdata"></div>
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">Water Temp</div>
        <div class="sinf-mini">°C</div>
    	<div id="sensor3" class="sdata"></div>
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">Air Temp</div>
        <div class="sinf-mini">°C</div>
    	<div id="sensor2" class="sdata"></div>
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">Humidity</div>
        <div class="sinf-mini">%</div>
    	<div id="sensor1" class="sdata"></div>
    </div>
    <hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">Light</div>
        <div class="sinf-mini">lux</div>
        <div id="sensor5" class="sdata"></div>  
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">Water Level</div>
        <div class="sinf-mini">level</div>
        <div id="sensor7" class="sdata"></div>  
    </div>
	<hr>
</div>

<div class="sensor">
	<div class="indication">
    	<div class="sinf">CO2</div>
        <div class="sinf-mini">ppm</div>
        <div id="sensor6" class="sdata"></div>  
    </div>
	<hr>
</div>

<div class="clear"></div>

</div>



