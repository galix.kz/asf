<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	$('#to_paste').keypress( function(e) {
			return isAsci(e.charCode);
	});
	function isAsci(cCode){
		return /[a-zA-Z0-9\--\_]/.test(String.fromCharCode(cCode))
	}
});
</script>
		<div class="profile">
			<div class="plabel">
				<?php echo $title; ?>
<!--				<br>
				<span>(не более 18 символов)</span>
-->				
			</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>



	<div class="row">
		<input type = "text" value = "<?php if(isset($val)) echo $val;?>" name = "name" id = "to_paste" class = "inputbox" />
	</div>

	<div class="btns">
		<?php echo CHtml::link('Отмена',$link_back, array('class' => 'btn btn-l')); ?>
		<?php echo CHtml::submitButton('Далее', array('class' => 'btn btn-r')); ?>
	</div>

<?php $this->endWidget(); ?>
<?php $this->renderPartial('/profile/_pop_keybo', array('hideEng' => isset($hideEng) ? $hideEng : 0, 'hideRus' => isset($hideRus) ? $hideRus : 0) ); ?>

		

</div><!-- form -->