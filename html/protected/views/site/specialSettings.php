<div class = "specialSettings">
	<?php foreach( $buttons as $one ) : ?>
		<div class = "row1">
		<?php echo $one->name; ?>
		<?php if ($one->off && $one->last == 1) : ?>
		<?php echo CHtml::link('Выключить', array('site/ExecSpecialSettings', 'id' => $one->id), array('class' => 'btn btn_off')); ?>
		<?php else : ?>
		<?php echo CHtml::link('Включить', array('site/execSpecialSettings', 'id' => $one->id), array('class' => 'btn')); ?>
		<?php endif; ?>
		</div>
	<?php endforeach; ?>
</div>