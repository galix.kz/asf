<script>
$(document).ready(function(){
	function checkShow(){
		if($('#autoUpdate_id').prop('checked'))
		{
			$('#calendar').hide();
			$('#time_zone').show();
		}
		else
		{
			$('#calendar').show();
			$('#time_zone').hide();
		}
	}
	$('#autoUpdate_id').on('change',function(e) {
	   checkShow();
	});	
	checkShow();
})
</script>
<style type="text/css">
	.ui-datepicker{
		width: 460px!important;
	}
	.ui-datepicker td span, .ui-datepicker td a	{
		height: 40px!important;
	}
</style>
<div class = "timeSettings">
	<div class = "main">
	<h1>Установка времени</h1>
	<form method = 'post' id = "form_id">
	<input type = "hidden" name = "save" value = "1" />
	<div class = "rowS">
	<input name = "autoUpdate" type = "checkbox" class = "styler" value = "1" id = "autoUpdate_id" <?php if($cur_auto) echo "checked"; ?> /> <label for = "autoUpdate_id">Автоматическое обновление</label>
	</div>
	<div class = "rowS" id = "calendar">
<?php	
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

    $this->widget('CJuiDateTimePicker',array(
//        'model'=>$model, //Model object
		'name' => 'currentDate',
		'language' => 'ru',
        'attribute'=>'currentDate', //attribute name
		'value' => date("Y-m-d H:i"),
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'options'=>array(
			'dateFormat' => 'yy-mm-dd',
			'timeFormat' => 'hh:mm',
		), // jquery plugin options 
    ));
?>
	</div>
	<div class = "rowS" id = "time_zone">
	<select name = "timezone" style = "    width: 300px; height: 71px;  font-size: 25px;">
	<?php foreach ($tzList as $v) : ?>
	<option <?php if($v == $cur_zone) echo "selected"; ?> ><?php echo $v;?></option>
	<?php endforeach; ?>
	</select>
	</div>
	</form>
	
	</div>
	<div class = "footer_bottoms">
	<?php echo CHtml::link('НАЗАД',array('site/deviceSettings'), array('class' => 'standart_button_left'));?>
	<?php echo CHtml::link('УСТАНОВИТЬ',array('#'), array('class' => 'standart_button_right','onclick' => "$('#form_id').submit();return false;"));?>
	</div>
</div>