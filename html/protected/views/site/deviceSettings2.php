<?php
/* @var $this ProfileController */
/* @var $dataProvider CActiveDataProvider */

?>
<div class = "deviceSettings">
<div class="main-menu bold">
<?php echo CHtml::link('<div class="name">Специальные настройки</div>', array('site/specialSettings'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Сохранение калибровки</div>', array('command/showDefaultCal'), array('class' => 'itm'));?>
<?php echo CHtml::link('<div class="name">Сервисные данные</div>', array('site/serviceData'), array('class' => 'itm'));?>
</div>
</div>
