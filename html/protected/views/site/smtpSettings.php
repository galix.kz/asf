<?php
?>
<script>
$(document).ready(function(){
	$('#create_point').click(function(){
		$('#loading').show();
		$('#create_point').hide();
		$('#point_error').html('');
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxCheckMail"); ?>',

			success: function(html){
				if(html == '1')
				{
					$('#point_error').addClass('good');
					html = 'Успешно отправлено!';
				}
				else
					$('#point_error').removeClass('good');
				$('#point_error').html(html);
				$('#loading').hide();
				$('#create_point').show();
//				console.log(html);
			}
			
		});
		return false;
	});
});
</script>
<div class = "smtpSettings">
	<div class = "main">
		<div class = "row">
			<label>Хост</label>
			<?php echo CHtml::link('Изменить', array('site/changeEmailSettings', 'param' => 'smtp_host'), array('class' => 'btn'));?>
			<div class = "value"><?php echo $settings->smtp_host;?></div>
		</div>
		<div class = "row">
			<label>Порт</label>
			<?php echo CHtml::link('Изменить', array('site/changeEmailSettings', 'param' => 'smtp_port'), array('class' => 'btn'));?>
			<div class = "value"><?php echo $settings->smtp_port; ?></div>
		</div>
		<div class = "row">
			<label>Логин</label>
			<?php echo CHtml::link('Изменить', array('site/changeEmailSettings', 'param' => 'smtp_username'), array('class' => 'btn'));?>
			<div class = "value"><?php echo $settings->smtp_username;?></div>
		</div>
		<div class = "row">
			<label>Пароль</label>
			<?php echo CHtml::link('Изменить', array('site/changeEmailSettings', 'param' => 'smtp_password'), array('class' => 'btn'));?>
			<div class = "value"><?php 
				for($i = 0; $i < strlen($settings->smtp_password); $i++)
					echo "*";
//				echo $settings->smtp_password; 
			?></div>
		</div>
		<div class = "row">
			<label>Периодичность</label>
			<?php echo CHtml::link('Изменить', array('site/changeEmailSettings', 'param' => 'time_check'), array('class' => 'btn'));?>
			<div class = "value"><?php echo $settings->time_check == 0 ? "Никогда" : $settings->time_check." минут"; ?></div>
		</div>
		<div class="rowBtns">
			<div id = "loading" style = "display:none">
				<p style="text-align:center;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" alt="" /></p>
			</div>
			<p class = "error" id = "point_error"></p>
				<a class="btn" id="create_point">Проверить подключение</a>
		</div>
	</div>
	<div class = "">
		<?php echo CHtml::link('НАЗАД', array('site/deviceSettings'), array('class' => 'standart_button_center')); ?>
	</div>
</div>