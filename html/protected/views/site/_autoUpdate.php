		<form method = 'post' id = "form_id">
		<input type = "hidden" name = "save" value = "1" />
		<div class = "rowS">
			<input name = "autoUpdate" type = "checkbox" class = "styler" value = "1" id = "autoUpdate_id" <?php if($cur_autoUpdate) echo "checked"; ?> /> <label for = "autoUpdate_id">Автоматическое обновление</label>
		</div>
		<div class = "rowS">
			<label>Текущая версия прошивки : <?php echo $cur_ver;?></label>
		</div>
		<div class = "rowS">
			<?php if($lat_ver == $cur_ver) : ?>
			<label>Установлено последнее обновление</label>
			<?php else : ?>
			<label>Доступна новая версия: <?php echo $lat_ver;?></label>
			<?php echo CHtml::link('Обновить', array('site/autoUpdate', 'update' => 1), array('class' => 'btn', 'id' => 'btn_update')); ?>

			<?php endif; ?>
		</div>
		</form>