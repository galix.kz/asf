<script>
$(document).ready(function(){
	$('.water_command').click(function(){
		if($(this).hasClass('disabled')) return false;
		$(this).addClass('disabled');
		var command = $(this).attr('data_command');
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/execWaterCommand");?>',
			data : {command : command},
			success: function(html){
				console.log(html);
			}
			
		});	
		return false;
	});
});
</script>

<?php 
	$arrVol = array();
	$i = 0;
	$arrVol[$i++] = 3;
	$arrVol[$i++] = 5;
	$arrVol[$i++] = 8;
	for($k = 10; $k < 100; $k+= 10)
		$arrVol[$i++] = $k;
	for($k = 100; $k < 1000; $k+= 50)
		$arrVol[$i++] = $k;
	for($k = 1000; $k < 3000; $k+= 100)
		$arrVol[$i++] = $k;

	$arrPer = array();
	$arrPerName = array();
	$i = 0;
	$arrPerName[$i] = "1м";
	$arrPer[$i++] = 1;
	$arrPerName[$i] = "5м";
	$arrPer[$i++] = 5;
	$arrPerName[$i] = "10м";
	$arrPer[$i++] = 10;
	for($k = 30; $k < 1440; $k+= 30)
	{
		$hh = (int)($k / 60);
		if($hh > 0)
			$arrPerName[$i] = $hh."ч".($k % 60)."м";
		else
			$arrPerName[$i] = ($k % 60)."м";
		$arrPer[$i++] = $k;
	}
?>
<?php $form=$this->beginWidget('ModernForm', array(
	'id'=>'settings-form-cycle',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'id' => 'wc_form_id',
)); ?>
<div class = "workingCapacity">
	<div class = "name">Объем рабочей емкости, л</div>
<div style = "display:inline-block; width:40%;">
	<div class = "value"><?php echo $form->sliderChoose2($model, 'volume_cap', $arrVol); ?></div>
</div>	

<div style = "display: inline-block; width: 58%; vertical-align: top; margin-top: 20px;">
	<h2>Раствор</h2>
	<?php echo CHtml::link('ЗАМЕНИТЬ', array('#'), array('class' => 'btn water_command', 'data_command' => 'COM_ZAMENA')); // Замена раствора ?>		
	<?php echo CHtml::link('ЗАЛИТЬ', array('#'), array('class' => 'btn water_command', 'data_command' => 'COM_ZALIV')); // Замена раствора ?>		
</div>
	<div class = "name">Периодичность дозации</div>
<div class = "value"><?php echo $form->sliderChoose2($model, 'reg_delay', $arrPer, $arrPerName); ?>
</div>

</div>
<div class = "bottom" style = 'width: 94%;margin-left: 3%;text-align:left;'>
<?php echo CHtml::link('НАЗАД', array('site/deviceSettings'), array('class' => 'standart_button_left')); ?>
<?php echo CHtml::link('ПРИМЕНИТЬ', '#', array('class' => 'standart_button_right', 'onclick' => "$('#wc_form_id').submit();return false;")); ?>
</div>
<?php $this->endWidget(); ?>	