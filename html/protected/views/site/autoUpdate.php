<script>
$(document).ready(function(){
	$('.main').on('click', '#btn_update', function(){
		$("#main").hide();
		$('#loading').show();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxUpdatePribor"); ?>',

			success: function(html){
				ShowMain();
			}
			
		});
		return false;
	})
	function ShowMain(){
		$("#main").hide();
		$('#loading').show();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxAutoUpdate"); ?>',

			success: function(html){
				$("#main").html(html);
				$(".styler").styler();
				$("#loading").hide();
				$("#main").show();
				
			}
			
		});
	}
	ShowMain();
});

</script>
<div class = "autoUpdate standart_div">
	<div class = "main">
		<h1>Обновление прибора</h1>
		<h2>Серийный номер : <?php echo Settings::GetSN(); ?></h2>
		<div id = "main">
	

		</div>
		<div id = "loading" style = "display:none">
			<p style="text-align:center;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" alt="" /></p>
		</div>
		
	</div>
	<div class = "footer_bottoms">
	<?php echo CHtml::link('НАЗАД',array('site/deviceSettings'), array('class' => 'standart_button_left'));?>
	<?php echo CHtml::link('УСТАНОВИТЬ',array('#'), array('class' => 'standart_button_right','onclick' => "$('#form_id').submit();return false;"));?>
	</div>
	
</div>