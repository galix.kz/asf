<?php

if($result) : 
?>
<?php $arr = Data::model()->attributeLabels(); ?>
<script>
/**
 * Generate external data set selector
 */
//AmCharts.clear();



			AmCharts.makeChart("chartdiv", {
				type: "stock",
				dataDateFormat: "YYYY-MM-DD JJ:NN:SS",
				dataSets: [
				<?php foreach ($result['rastvor'] as $k => $v) : ?>
				{
			      title: "<?php echo (isset($arr[$k])) ? $arr[$k] : $k; ?>",
			      fieldMappings: [ {
			        fromField: "value",
			        toField: "value"
			      }],
			      dataProvider: <?php echo CJSON::encode($v);?>,
			      categoryField: "date",
				  <?php if($k == "ph" && $pointsRegPH) : ?>
				  stockEvents: [
				  <?php foreach($pointsRegPH as $v1) : ?>
				  {
						date: "<?php echo date("Y-m-d H:i:s", $v1->date);?>",
						type: "sign",
						backgroundColor: "#FF9900",
						borderColor: "#FF9900",
						showBullet: true,
						graph: "g1",
						text: "",
						description: "<?php echo $v1->getDiscription(); ?>"
				  },
				  <?php endforeach; ?>
				  ]
				  <?php endif; ?>
				  <?php if($k == "tds" && $pointsRegTDS) : ?>
				  stockEvents: [
				  <?php foreach($pointsRegTDS as $v1) : ?>
				  {
						date: "<?php echo date("Y-m-d H:i:s", $v1->date);?>",
						type: "sign",
						backgroundColor: "#FF9900",
						borderColor: "#FF9900",
						showBullet: true,
						graph: "g1",
						text: "",
						description: "<?php echo $v1->getDiscription(); ?>"
				  },
				  <?php endforeach; ?>
				  ]
				  <?php endif; ?>
			    },
				<?php endforeach; ?>
				<?php foreach ($result['climat'] as $k => $v) : ?>
				{
			      title: "<?php echo (isset($arr[$k])) ? $arr[$k] : $k; ?>",
			      fieldMappings: [ {
			        fromField: "value",
			        toField: "value"
			      }],
			      dataProvider: <?php echo CJSON::encode($v);?>,
			      categoryField: "date",
			    },
				<?php endforeach; ?>
				],

				panels: [{
					title: "Value",
					stockGraphs: [{
						id: "g1",
						lineColor: "#009DC8",
						lineThickness: 3,
						negativeBase: 6.5,
						negativeLineColor: "#00C800",
						type: "smoothedLine",
						descriptionField: "description",
						bulletBorderThickness: 1,
						bulletBorderAlpha: 1, 
						bulletSize: 3,
						bulletColor: "#00C800",
						bulletBorderColor: "#00C800",
						balloonText: "<div style='margin:5px;'>[[category]]<br>[[value]]<br>[[description]]</div>",
						dateFormat: "YYYY-MM-DD JJ:NN:SS",
						balloonColor: "#00C800",
						valueField: "value",
						bullet: "round",
						useDataSetColors: false
					}, 
					{
						id: "g2",
						lineColor: "#00C800",
						lineThickness: 3,
						lineAlpha: 0,
						negativeLineColor: "#009DC8",
						negativeBase: 4.5,
						negativeLineAlpha: 1,
						showBalloon: false,
						type: "smoothedLine",
						valueField: "value",
						useDataSetColors: false
					}],
					valueAxes: [{
						axisAlpha: 0,
						position: "left",
						fontSize: 10,
						color: "#666666",
						type: "value"
					}],
					categoryAxis: {
						minPeriod: 'ss',
						fontSize: 10,
						color: "#666666",
						dateFormats: [
							{period:'ss',format:'JJ:NN:SS'},
							{period:'mm',format:'JJ:NN:SS'},
							{period:'hh',format:'JJ:NN:SS'},
							{period:'DD',format:'YYYY-MM-DD'},
							{period:'MM',format:'YYYY-MM-DD'},
							{period:'YYYY',format:'YYYY-MM-DD'}
						],
						parseDates: true
					}
				}],
				balloon: {
					animationDuration: 0,
					fadeOutDuration: 0,
					showBullet: true
				},
				categoryAxesSettings: {
					minPeriod: "ss"
				},
				
				chartScrollbarSettings: {
					graph: "g1",
					gridAlpha: 0,
					color: "#888888",
					scrollbarHeight: 40,
					backgroundAlpha: 1,
					backgroundColor: "#F5F6F8",
					selectedBackgroundAlpha: 1,
					selectedBackgroundColor: "#ffffff",
					graphFillAlpha: 0,
					autoGridCount: true,
					selectedGraphFillAlpha: 0,
					graphLineAlpha: 1,
					graphLineColor: "#ACBDCA",
					selectedGraphLineColor: "#48B811",
					selectedGraphLineAlpha: 1,
					oppositeAxis: true,
					autoGridCount: false,
					dragIconHeight: 40,
					dragIconWidth: 2
				},
				chartCursorSettings: {
					categoryBalloonDateFormats:[
							{period:'ss',format:'YYYY-MM-DD JJ:NN:SS'}, 
							{period:'mm',format:'YYYY-MM-DD JJ:NN:SS'}, 
							{period:'hh',format:'YYYY-MM-DD JJ:NN:SS'}, 
							{period:'DD',format:'YYYY-MM-DD JJ:NN:SS'}, 
							{period:'MM',format:'YYYY-MM-DD JJ:NN:SS'}, 
							{period:'YYYY',format:'YYYY-MM-DD JJ:NN:SS'}
						],
					cursorColor: "#000000",
					valueLineEnabled: false,
					valueLineBalloonEnabled: false,
					categoryBalloonEnabled: false,
					valueBalloonsEnabled: true,
					cursorAlpha: 0,
					valueLineAlpha: 0
				},
/*				periodSelector: {
					inputFieldsEnabled: false,
					periodsText: "",
					periods: [
					{
						period: "hh",
						count: 1,
						label: "час"
					},
					{
						period: "DD",
						selected: true,
						count: 1,
						label: "день"
					},
					{
						period: "MM",
						count: 1,
						label: "мес"
					},
					{
						period: "MM",
						count: 3,
						label: "3мес"
					},
					{
						period: "MAX",
						label: "all time"
					}]
				},
*/
				panelsSettings: {
					usePrefixes: true,
					accessible: true
				},
				dataSetSelector: {
				    divId: "selector"
				},
				export: {
					enabled: false
				}
			});
			
$(document).ready(function(){
	$('#rastvor_id_href').click(function(){
		$('.li_climat').hide();
		$('.li_rastvor').show();
		$('#climat_id_href').removeClass('active');
		$('#rastvor_id_href').addClass('active');
		$('.li_rastvor').first().click();
		console.log('rastvor_id_href');
		return false;
	});
	$('#climat_id_href').click(function(){
		$('.li_rastvor').hide();
		$('.li_climat').show();
		$('.li_climat').first().click();
		$('#rastvor_id_href').removeClass('active');
		$('#climat_id_href').addClass('active');
		console.log('climat_id_href');
		return false;
	});
});
</script>


		<div class="graph-sel row">
			<a href="#" id = "climat_id_href" class="name">Климат</a>
			<a class="name active" id = "rastvor_id_href">Раствор</a>
		</div>
		<div class="graph-block row" id = "rastvor_id">
			<div id="selector"></div>
			<div id="chartdiv" style="width:100%; height:260px;"></div>
		</div>
<?php else : ?>
<h2 style = "text-align:center;color:#48B811;"> Нет точек</h2>
<?php endif;?>
