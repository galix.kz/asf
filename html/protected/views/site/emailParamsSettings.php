<?php
?>
<script>
$(document).ready(function(){
	function SaveParam(th){
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/ajaxSaveEmailParam");?>',
			data : {id : th.attr('data-id'), val : th.is(':checked') ? 1 : 0},
			success: function(html){
//				console.log(html);
			}
		});
	}
	function ShowSaveSuccess(){
		$('.afterSaveParam').fadeIn(500, function(){ $(this).fadeOut(500); });
	}
	$('#st_check_all').change(function(){
		console.log("cur_val_2 = " + $(this).is(':checked'));
		$('.st_check').prop('checked', $(this).is(':checked'));
		$('.st_check').each(function(){
			SaveParam($(this));
		});
		ShowSaveSuccess();
		
	});
	$('.st_check').change(function(){
		SaveParam($(this));
		ShowSaveSuccess();
	});
});
</script>
<div class = "emailParams">
	<div class = "main">
		<?php foreach ($settings as $one) : ?>
		<div class = "row">
			<label><?php echo $one->name; ?></label>
			<label class="switch">
				<input type = "checkBox" class = "st_check" data-id = "<?php echo $one->id; ?>" name = "emParam[<?php echo $one->id; ?>]" value = "1" <?php if ($one->isOn) echo "checked"; ?> />
				<div class="toggle"></div>
			</label>
		</div>
		<?php endforeach; ?>
		<div class = "row">
			<label>Вкл / Выкл всех уведомлений</label>
			<label class="switch">
				<input type = "checkBox" id = "st_check_all" name = "all" value = "1" checked />
				<div class="toggle"></div>
			</label>
		</div>
		<div class="rowBtns">
			<label class = "afterSaveParam" style = "display: none;" >Сохранено</label>
		</div>
	</div>
	<div class = "">
		<?php echo CHtml::link('НАЗАД', array('site/deviceSettings'), array('class' => 'standart_button_center')); ?>
	</div>

</div>