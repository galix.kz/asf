<style type="text/css">
	.ui-datepicker{
		width: 460px!important;
	}
	.ui-datepicker td span, .ui-datepicker td a	{
		height: 55px!important;
	}
</style>
<?php 
	
?>
<script>
AmCharts.addInitHandler(function(chart) {
  
  // check if divId is set in dataSetSelector
  if ( ! chart.dataSetSelector.divId )
    return;
  
  // build external selector
  var holder = document.getElementById(chart.dataSetSelector.divId);
  
  // create ul
  var ul = document.createElement("ul");
  
  // create <li> item for each data set
  for(var i = 0; i < chart.dataSets.length; i++) {
    // create li
    var ds = chart.dataSets[i];
    var li = document.createElement("li");
    li.innerHTML = ds.title;
    li.setAttribute("data-dataset", i);
    
    // append click events
    li.addEventListener("click", function() {
      chart.mainDataSet = chart.dataSets[this.getAttribute("data-dataset")];
      chart.validateData();
    });
    ul.appendChild(li);
  }
  
  holder.appendChild(ul);
  
  // disable the built-in data set selector
  chart.dataSetSelector = undefined;
  for(var i = 0; i < 4; i++)
		$('.graph-block #selector li:eq( ' + i + ' )').addClass('li_rastvor');
  for(var i = 4; i < 8; i++)
		$('.graph-block #selector li:eq( ' + i + ' )').addClass('li_climat').hide();
  $('.graph-block #selector li').first().addClass("active");  
 	$('.graph-block #selector li').click(function() {
		$(".graph-block #selector li").removeClass("active");
    	$(this).addClass("active");   
	});
/*  $('.graph-block #selector2 li').first().addClass("active");  
 	$('.graph-block #selector2 li').click(function() {
		$(".graph-block #selector2 li").removeClass("active");
    	$(this).addClass("active");   
		
	});
*/	
	
}, ["stock"]);

$(document).ready(function(){
	$('#viewgraph_form button').on('click',function(){
		if($(this).hasClass('unactive'))
			return false;
		$(this).addClass('unactive');
		$('#viewgraph').empty();
//		$('#viewgraph').hide();
		$('#loading').show();
		$.ajax({
				method: 'get',
				url: '<?php echo $this->createUrl("site/viewgraphRender");?>',
				data: $('#viewgraph_form form').serialize(),

				success: function(html){
					$('#viewgraph_form button').removeClass('unactive');
					$('#viewgraph').html(html);
					$('#loading').hide();
//					$('#viewgraph').show();
				}
				
		});	
		return false;
	});
	function GetTimeLeft(){
		$.ajax({
				method: 'get',
				url: '<?php echo $this->createUrl("site/getTimeLeft");?>',
				data: $('#viewgraph_form form').serialize(),

				success: function(html){
					$('#time_left_label').html(html);
				}
				
		});	
	}
	$('#viewgraph_form input').change(function(){
		GetTimeLeft();
	});
	GetTimeLeft();
	$('#loading').show();
	$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("site/viewgraphRender");?>',
				data: $('#viewgraph_form form').serialize(),

			success: function(html){
				$('#viewgraph_form button').removeClass('unactive');
				$('#viewgraph').html(html);
				$('#loading').hide();
			}
			
	});	
	
	$('.form_point li').click(function(){
		$('.form_point li').removeClass('active');
		$(this).addClass('active');
		$('#cnPoint_id').val($(this).attr('data-points'));
		GetTimeLeft();
	});
	
});
</script>
<div id = "viewgraph_form">
<form>
<table>
<tr>
<td>	<label for = "dateFrom"> Дата от </label> 
<?php	
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

    $this->widget('CJuiDateTimePicker',array(
//        'model'=>$model, //Model object
		'name' => 'dateFrom',
		'language' => 'ru',
        'attribute'=>'dateFrom', //attribute name
		'value' => (isset($_GET['dateFrom']) ? $_GET['dateFrom'] : date("Y-m-d H:i", time() - 60 * 60 * 24 * 30)),
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'options'=>array(
			'dateFormat' => 'yy-mm-dd',
			'timeFormat' => 'hh:mm',
		) // jquery plugin options 
    ));

?>
</td>
<td>	<label for = "dateTo"> Дата до </label> 
<?php	
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

    $this->widget('CJuiDateTimePicker',array(
//        'model'=>$model, //Model object
		'name' => 'dateTo',
		'language' => 'ru',
        'attribute'=>'dateTo', //attribute name
		'value' => (isset($_GET['dateTo']) ? $_GET['dateTo'] : date("Y-m-d H:i", time())),
        'mode'=>'datetime', //use "time","date" or "datetime" (default)
        'options'=>array(
			'dateFormat' => 'yy-mm-dd',
			'timeFormat' => 'hh:mm',
		) // jquery plugin options 
    ));

?>
</td>
<td class = "form_point">
	<input name = "cnPoint" value = '10' type = "hidden" id = "cnPoint_id"/>
<ul>
	<li data-points = '1'>1 т/ч</li>
	<li data-points = '10' class = "active">10 т/ч</li>
	<li data-points = '60'>60 т/ч</li>
	<li data-points = '120'>120 т/ч</li>
</li>
<!--	<input type = "radio" name = "cnPoint" value = "1" id = "cnPoint_1" <?php if(isset($_GET['cnPoint']) && $_GET['cnPoint'] == 1) echo "checked";?> /> <label for = "cnPoint_1">Редко ( &asymp; 1 точка / час )</label> <br>
	<input type = "radio" name = "cnPoint" value = "10" id = "cnPoint_2" <?php if(!isset($_GET['cnPoint']) || $_GET['cnPoint'] == 10) echo "checked";?> /> <label for = "cnPoint_2">Средне ( &asymp; 10 точек / час )</label><br>
	<input type = "radio" name = "cnPoint" value = "60" id = "cnPoint_3" <?php if(isset($_GET['cnPoint']) && $_GET['cnPoint'] == 60) echo "checked";?> /> <label for = "cnPoint_3">Часто ( &asymp; 60 точек / час )</label><br>
	<input type = "radio" name = "cnPoint" value = "120" id = "cnPoint_4" <?php if(isset($_GET['cnPoint']) && $_GET['cnPoint'] == 120) echo "checked";?> /> <label for = "cnPoint_4">Все ( &asymp; 120 точек / час )</label>
-->
</td>
	</tr>
	<tr>
	<td colspan = '2'>
		<label id = "time_left_label" style = "font-size: 15px"></label>
	</td>
		<td>
			<button class = "btn unactive">Показать</button>
		</td>
	</tr>
	
</table>
</form>
</div>
<p id="loading" style="display:none; text-align:center;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loading.gif" alt="" /></p>
<div id = "viewgraph">
</div>