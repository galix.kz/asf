<?php
/* @var $this DataController */
/* @var $data Data */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph')); ?>:</b>
	<?php echo CHtml::encode($data->ph); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hum')); ?>:</b>
	<?php echo CHtml::encode($data->hum); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp')); ?>:</b>
	<?php echo CHtml::encode($data->temp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds')); ?>:</b>
	<?php echo CHtml::encode($data->tds); ?>
	<br />


</div>