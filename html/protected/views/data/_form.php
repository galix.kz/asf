<?php
/* @var $this DataController */
/* @var $model Data */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'data-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph'); ?>
		<?php echo $form->textField($model,'ph',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ph'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hum'); ?>
		<?php echo $form->textField($model,'hum',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'hum'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp'); ?>
		<?php echo $form->textField($model,'temp',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'temp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp'); ?>
		<?php echo $form->textField($model,'wtemp',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wtemp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds'); ?>
		<?php echo $form->textField($model,'tds',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tds'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->