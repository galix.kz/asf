<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Параметры pH</title>
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_my.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.formstyler.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/style.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/plugins/export/export.css" type="text/css"  media="all">

		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/amcharts.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/serial.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/amstock.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/plugins/export/export.min.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/style.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/slick.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.formstyler.min.js" type="text/javascript"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts_m.js"></script>
</head>

<body>
	<div class="all">
		<div class="header">
			<div class="row">
				<a href="/" class="logo"></a>

			</div>
			<div class="row">
				<div class="heading"><?php echo $this->title; ?></div>
			</div>
		</div>
			<?php echo $content; ?>
		
	</div>	
	
</body>
</html>
