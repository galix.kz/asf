<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Параметры pH</title>
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_my.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_localhost.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.formstyler.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/style.css" type="text/css">
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/plugins/export/export.css" type="text/css"  media="all">

		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/amcharts.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/serial.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/amstock.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/adds/amcharts/plugins/export/export.min.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.3.min.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/style.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/slick.js" type="text/javascript"></script>
		<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.formstyler.min.js" type="text/javascript"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts_m.js"></script>
    <script>
function connect() {
//  var ws = new WebSocket("ws://172.16.29.192:8000/?user=<?php echo Settings::GetSN(); ?>");
  var ws = new WebSocket("ws://85.118.225.130:8000/?user=<?php echo Settings::GetSN(); ?>");
  var sn = <?php echo Settings::GetSN(); ?>;
  ws.onopen = function() {
    ws.send(JSON.stringify({name : sn, command : "setName"
    }));
  };

  ws.onmessage = function(e) {
	var res = JSON.parse(e.data);
	if(res.command == "message")
	{
//		console.log("activeCode = " + res.code);
		$('#server_message_id').html(res.message);
		$('#botpopup_server_message').addClass('active');
		ws.send(JSON.stringify({command : "message_answer", result : "success", con_id : res.con_id}));
	}
	else if(res.command == "screen")
	{
		  
/*		html2canvas(document.querySelector("#capture")).then(canvas => {
			var buf = canvas.toDataURL("image/jpeg", 0.1);
			console.log(buf);
			ws.send(buf);
//			document.body.appendChild(canvas)
		});
*/		
	}
  };

  ws.onclose = function(e) {
    setTimeout(function() {
      connect();
    }, 10000);
  };

  ws.onerror = function(err) {
    ws.close();
  };
}

connect();	

    </script>	
	
</head>

<body>
	<div class="all">
		<div class="header">
			<div class="row">
				<a href="/" class="logo"></a>
				<div class="status">
<?php 				
		 $settings = Settings::model()->findByPk(1);
		 if($settings->active_profile_id && $settings->active_profile_id > 0)
		 {
			 $model = Profile::model()->findByPk($settings->active_profile_id);
			 $model->getActiveCycle($settings->active_start_day, $ind);
			 echo "Фаза #".($ind+ 1)." день ".(strtotime('today') - $settings->active_start_day) / (24 * 60 * 60)."<br/>".$model->name;
			 
		 }
		 else 
			 echo "Ожидание..";
?>		 
				
				</div>
				<div class="menu">
					<span class="sandwich">
						<span class="sw-topper"></span>
						<span class="sw-bottom"></span>
						<span class="sw-footer"></span>
					</span>
				</div>
				<div class="time" id = "doc_time"><?php echo date("H:i"); ?></div>
				<div class="headmenu">
				<div class = "headmenu_head">
					МЕНЮ
				</div>
        <?php $this->widget('zii.widgets.CMenu',array(
                    'activateParents'=>true,
        'activeCssClass'=>'active',
        'items'=>array(
            array('label'=>'Управление выращиванием', 'url'=>array('/profile/current')),
            array('label'=>'Профили выращивания', 'url'=>array('/profile/index')),
//			array('label'=>'Настройки прибора', 'url'=>array('/site/contact')),
			array('label'=>'Графики', 'active'=>true, 'url'=>array('/site/viewgraph')),
			array('label'=>'Настройки прибора', 'url'=>array('/site/deviceSettings')),
//			array('label'=>'Настройки прибора (2)', 'url'=>array('/site/page', 'view'=>'settings')),
			array('label'=>'Калибровка', 'url'=>array('/command/indexNew')),
//			array('label'=>'О проекте - QR-code', 'url'=>array('/site/page', 'view'=>'about')),
//            array('label'=>'Войти', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
//            array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
        ),
    )); ?>				</div>
			</div>
			<div class="row">
				<div class="heading"><?php echo $this->title; ?></div>
			</div>
		</div>
			<?php echo $content; ?>
<script>
$(document).ready(function(){
	$('#btn_server_message_otmena').click(function(){
		$('#botpopup_server_message').removeClass('active');
		return false;
	})
});
</script>
				<div class = "botpopup_server_message" id = "botpopup_server_message">
					<div class = "top">Сообщение от сервера</div>
					<div class = "middle" id = "server_message_id">
					</div>
					<br/>
					<div class = "pop_btns">
						<a href = '#' class ="btn_otmena" id = "btn_server_message_otmena">ОТМЕНА</a>
					</div>
					
				</div>
		
	</div>	
	
</body>
</html>
