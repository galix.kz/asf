<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/add.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sensor.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/scripts.js"></script>
</head>

<body>

<div align="center" class="container" id="page">
	<div id="header" align="left">
		<div id="logo">
        	<a href="/">
        	<img alt="Over Grower logo" src="/images/overgrower-logo.png">
            </a>
        </div>
        <div class="button"></div>
        <div id="menu-top"><!-- mainmenu -->
        <?php $this->widget('zii.widgets.CMenu',array(
                    'activateParents'=>true,
        'activeCssClass'=>'active',
        'items'=>array(
            array('label'=>'Управление выращиванием', 'url'=>array('/command/index')),
			array('label'=>'Графики', 'active'=>true, 'url'=>'#', 'items'=>
            array(
                array('label'=>'За 5 сек', 'url'=>
                array('site/page', 'view'=>'5sec')), array('label'=>'За 1 час', 'url'=>
                array('site/page', 'view'=>'1hour')),
                array('label'=>'За 6 часов', 'url'=>array('site/page', 'view'=>'6hour')),
                array('label'=>'За 12 часов', 'url'=>'#', 'active'=>true, 'items'=>
                array(array('label'=>'Ph', 'url'=>
                array('site/page', 'view'=>'12hour', 'p'=>'ph')),
                array('label'=>'Hum', 'url'=>
                array('site/page', 'view'=>'12hour', 'p'=>'hum')), 
                array('label'=>'Temp', 'url'=>array('site/page', 'view'=>'12hour', 'p'=>'temp')),
                array('label'=>'Wtemp', 'url'=>array('site/page', 'view'=>'12hour', 'p'=>'wtemp')),
                array('label'=>'Tds', 'url'=>
                array('site/page', 'view'=>'12hour', 'p'=>'tds')),),),
                array('label'=>'За 1 день', 'url'=>array('site/page', 'view'=>'1day')),
                array('label'=>'За 1 неделю', 'url'=>array('site/page', 'view'=>'1week')),
                array('label'=>'За 1 месяц', 'url'=>array('site/page', 'view'=>'1month')),
                array('label'=>'За 3 месяца', 'url'=>array('site/page', 'view'=>'3month')),
            )),
			array('label'=>'Настройки прибора', 'url'=>array('/site/page', 'view'=>'settings')),
            array('label'=>'Войти', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
            array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
        ),
    )); ?>
        </div>
        
	</div>
    <!-- header -->
<div class="service_info" align="left">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>
</div>
	<div class="clear"></div>
<!--
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by OverGrower.<br />
		All Rights Reserved.<br />
                       
                        
	</div>
-->
    <!-- footer -->

</div><!-- page -->
</body>
</html>
