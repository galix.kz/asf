<?php
/* @var $this DataMinController */
/* @var $model DataMin */

$this->breadcrumbs=array(
	'Data Mins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DataMin', 'url'=>array('index')),
	array('label'=>'Create DataMin', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#data-min-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Data Mins</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'data-min-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date',
		'ph_min',
		'ph_max',
		'ph_open',
		'ph_close',
		/*
		'hum_min',
		'hum_max',
		'hum_open',
		'hum_close',
		'temp_min',
		'temp_max',
		'temp_open',
		'temp_close',
		'wtemp_min',
		'wtemp_max',
		'wtemp_open',
		'wtemp_close',
		'tds_min',
		'tds_max',
		'tds_open',
		'tds_close',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
