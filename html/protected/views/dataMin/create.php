<?php
/* @var $this DataMinController */
/* @var $model DataMin */

$this->breadcrumbs=array(
	'Data Mins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DataMin', 'url'=>array('index')),
	array('label'=>'Manage DataMin', 'url'=>array('admin')),
);
?>

<h1>Create DataMin</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>