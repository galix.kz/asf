<?php
/* @var $this DataMinController */
/* @var $data DataMin */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph_min')); ?>:</b>
	<?php echo CHtml::encode($data->ph_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph_max')); ?>:</b>
	<?php echo CHtml::encode($data->ph_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph_open')); ?>:</b>
	<?php echo CHtml::encode($data->ph_open); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph_close')); ?>:</b>
	<?php echo CHtml::encode($data->ph_close); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hum_min')); ?>:</b>
	<?php echo CHtml::encode($data->hum_min); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('hum_max')); ?>:</b>
	<?php echo CHtml::encode($data->hum_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hum_open')); ?>:</b>
	<?php echo CHtml::encode($data->hum_open); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hum_close')); ?>:</b>
	<?php echo CHtml::encode($data->hum_close); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_min')); ?>:</b>
	<?php echo CHtml::encode($data->temp_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_max')); ?>:</b>
	<?php echo CHtml::encode($data->temp_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_open')); ?>:</b>
	<?php echo CHtml::encode($data->temp_open); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_close')); ?>:</b>
	<?php echo CHtml::encode($data->temp_close); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp_min')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp_max')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp_open')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp_open); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp_close')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp_close); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds_min')); ?>:</b>
	<?php echo CHtml::encode($data->tds_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds_max')); ?>:</b>
	<?php echo CHtml::encode($data->tds_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds_open')); ?>:</b>
	<?php echo CHtml::encode($data->tds_open); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds_close')); ?>:</b>
	<?php echo CHtml::encode($data->tds_close); ?>
	<br />

	*/ ?>

</div>