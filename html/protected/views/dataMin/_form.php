<?php
/* @var $this DataMinController */
/* @var $model DataMin */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'data-min-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph_min'); ?>
		<?php echo $form->textField($model,'ph_min',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ph_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph_max'); ?>
		<?php echo $form->textField($model,'ph_max',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ph_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph_open'); ?>
		<?php echo $form->textField($model,'ph_open',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ph_open'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph_close'); ?>
		<?php echo $form->textField($model,'ph_close',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ph_close'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hum_min'); ?>
		<?php echo $form->textField($model,'hum_min',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'hum_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hum_max'); ?>
		<?php echo $form->textField($model,'hum_max',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'hum_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hum_open'); ?>
		<?php echo $form->textField($model,'hum_open',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'hum_open'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hum_close'); ?>
		<?php echo $form->textField($model,'hum_close',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'hum_close'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_min'); ?>
		<?php echo $form->textField($model,'temp_min',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'temp_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_max'); ?>
		<?php echo $form->textField($model,'temp_max',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'temp_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_open'); ?>
		<?php echo $form->textField($model,'temp_open',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'temp_open'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_close'); ?>
		<?php echo $form->textField($model,'temp_close',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'temp_close'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp_min'); ?>
		<?php echo $form->textField($model,'wtemp_min',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wtemp_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp_max'); ?>
		<?php echo $form->textField($model,'wtemp_max',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wtemp_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp_open'); ?>
		<?php echo $form->textField($model,'wtemp_open',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wtemp_open'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp_close'); ?>
		<?php echo $form->textField($model,'wtemp_close',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'wtemp_close'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds_min'); ?>
		<?php echo $form->textField($model,'tds_min',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tds_min'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds_max'); ?>
		<?php echo $form->textField($model,'tds_max',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tds_max'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds_open'); ?>
		<?php echo $form->textField($model,'tds_open',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tds_open'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds_close'); ?>
		<?php echo $form->textField($model,'tds_close',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'tds_close'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->