<?php
/* @var $this DataMinController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Data Mins',
);

$this->menu=array(
	array('label'=>'Create DataMin', 'url'=>array('create')),
	array('label'=>'Manage DataMin', 'url'=>array('admin')),
);
?>

<h1>Data Mins</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
