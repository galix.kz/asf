<?php
/* @var $this DataMinController */
/* @var $model DataMin */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph_min'); ?>
		<?php echo $form->textField($model,'ph_min',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph_max'); ?>
		<?php echo $form->textField($model,'ph_max',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph_open'); ?>
		<?php echo $form->textField($model,'ph_open',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph_close'); ?>
		<?php echo $form->textField($model,'ph_close',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hum_min'); ?>
		<?php echo $form->textField($model,'hum_min',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hum_max'); ?>
		<?php echo $form->textField($model,'hum_max',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hum_open'); ?>
		<?php echo $form->textField($model,'hum_open',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hum_close'); ?>
		<?php echo $form->textField($model,'hum_close',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_min'); ?>
		<?php echo $form->textField($model,'temp_min',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_max'); ?>
		<?php echo $form->textField($model,'temp_max',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_open'); ?>
		<?php echo $form->textField($model,'temp_open',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_close'); ?>
		<?php echo $form->textField($model,'temp_close',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wtemp_min'); ?>
		<?php echo $form->textField($model,'wtemp_min',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wtemp_max'); ?>
		<?php echo $form->textField($model,'wtemp_max',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wtemp_open'); ?>
		<?php echo $form->textField($model,'wtemp_open',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wtemp_close'); ?>
		<?php echo $form->textField($model,'wtemp_close',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tds_min'); ?>
		<?php echo $form->textField($model,'tds_min',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tds_max'); ?>
		<?php echo $form->textField($model,'tds_max',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tds_open'); ?>
		<?php echo $form->textField($model,'tds_open',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tds_close'); ?>
		<?php echo $form->textField($model,'tds_close',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->