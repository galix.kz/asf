<?php
/* @var $this DataMinController */
/* @var $model DataMin */

$this->breadcrumbs=array(
	'Data Mins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DataMin', 'url'=>array('index')),
	array('label'=>'Create DataMin', 'url'=>array('create')),
	array('label'=>'Update DataMin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DataMin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DataMin', 'url'=>array('admin')),
);
?>

<h1>View DataMin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'ph_min',
		'ph_max',
		'ph_open',
		'ph_close',
		'hum_min',
		'hum_max',
		'hum_open',
		'hum_close',
		'temp_min',
		'temp_max',
		'temp_open',
		'temp_close',
		'wtemp_min',
		'wtemp_max',
		'wtemp_open',
		'wtemp_close',
		'tds_min',
		'tds_max',
		'tds_open',
		'tds_close',
	),
)); ?>
