<?php
/* @var $this DataMinController */
/* @var $model DataMin */

$this->breadcrumbs=array(
	'Data Mins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DataMin', 'url'=>array('index')),
	array('label'=>'Create DataMin', 'url'=>array('create')),
	array('label'=>'View DataMin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DataMin', 'url'=>array('admin')),
);
?>

<h1>Update DataMin <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>