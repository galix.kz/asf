<?php
/* @var $this ProfileController */
/* @var $dataProvider CActiveDataProvider */

?>
<script>
$(document).ready(function(){
	$('#cal_default').click(function(){
		$('.botpopup2').addClass('active');
		return false;
	})
	$('#btn_otmena').click(function(){
		$('.botpopup2').removeClass('active');
		return false;
	})
});
</script>
<div class="calibr" style = "height: 550px;">
			<div class="itm">
				<div class="name">pH датчик</div>
				<?php echo CHtml::link('Калибровать', array('command/calPh'), array('class' => 'btn')); ?>
			</div>
			<div class="itm">
				<div class="name">TDS датчик</div>
				<?php echo CHtml::link('Калибровать', array('command/calTds'), array('class' => 'btn')); ?>
			</div>
			<div class="itm">
				<div class="name">Насосы</div>
				<?php echo CHtml::link('Калибровать', array('command/pumps'), array('class' => 'btn')); ?>
			</div>
			<div class="itm">
				<div class="name">Датчик света</div>
				<?php echo CHtml::link('Калибровать', array('site/lightSensor'), array('class' => 'btn')); ?>
			</div>
			<div class = "itm center">
				<?php if ($def) : ?>
				<p class = "def_text">Установлены стандартные настройки</p>
				<?php else : ?>
				<?php echo CHtml::link('Сбросить на стандартные', array('#'), array('class' => 'btn', 'id' => 'cal_default' , 'style' => 'font-size:22px')); ?>
				<?php endif; ?>
				
			</div>
				<div class = "botpopup2" style = "text-align:center; font-size: 32px;">
					<div class = "top">Стандартная калибровка</div>
					<div class = "middle">
						Сброс калибровочных настроек приведет к потерe текущих настроек<br/>
						Вы уверены?
					</div>
					<br/>
					<div class = "pop_btns">
						<?php echo CHtml::link('СБРОСИТЬ', array('command/loadDefaultCal'),array('class' => 'btn_del')); ?>
						<a href = '#' class ="btn_otmena" id = "btn_otmena">ОТМЕНА</a>
					</div>
					
				</div>

</div>
<div class = "">
	<?php echo CHtml::link('НАЗАД', array('site/deviceSettings'), array('class' => 'standart_button_center')); ?>
</div>
