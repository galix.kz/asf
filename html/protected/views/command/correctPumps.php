<?php $settings = Settings::model()->findByPk(1);?>
<script>

$(document).ready(function(){
	
	var countdown = $('#countdown span'),
//		but = $('button'),
		timer;
	function startCountdown(){
		var startFrom = <?php echo $settings->pump_work_second; ?>;
		countdown.text(startFrom).parent('div').show();
//		but.hide();
		timer = setInterval(function(){
			countdown.text(--startFrom);
			if(startFrom <= 0) {
				clearInterval(timer);
				$('.calibr-itm').removeClass('wait');
//				countdown.text('Прокачали');
//				but.show();
			}
		},1000);
	}	
	startCountdown();
	
});
</script>
		<div class="calibr-itm wait">
			<div class="heads" id = "countdown">Осталось секунд : <span> </span></div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pumps-form-form',
	'method' => 'get',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<!--			<input type = "hidden" name = "command" value = "" /> -->
			<div class="calibr-action">
				<h3><?php echo $title; ?></h3>
				<h3>Жидкости перекачано, мл:</h3>
				<div class="steps-step">
					<div class="row cont big-btm">
						<div class="col1-1">
							<?php echo ModernForm::sliderChooseNoModel(2, 1, 99, NULL, 'ml'); ?>
						</div>
					</div>
					<div class="row btms">
						<?php echo CHtml::link('Отмена',array('command/pumps'), array('class' => 'btn btn-l'));?>
						<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn btn-r')); ?>
					</div>
				</div>
			</div>
			<?php $this->endWidget(); ?>

		</div>