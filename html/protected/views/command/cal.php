<?php
/* @var $this CommandController */

$this->breadcrumbs = array(
    'Калибровка',
);

$this->menu=array(
        array('label'=>'Управление', 'url'=>array('command/index')),
);
Yii::app()->clientScript->registerScript("lastload", "

$(function(){    

    window.onload = setupRefresh;



    function setupRefresh() {

      //setTimeout('refreshPage();', 5000); 
      refreshPage();
      setInterval(refreshPage, 5000 );

    }

    function refreshPage() {

       $(document).ready(function(){

       $('#comtable').load('/index.php?r=site/loadTable2', 
        function() {
	}
       );
       });
   }
});

    function info(text){
        var textarea = document.getElementById('result');
        $('#result').append(text);
        textarea.scrollTop = textarea.scrollHeight;
    }
    
    function onTrigger(editId, tdId){
        var edit_vl = parseFloat($(editId).val());
        var td_vl = parseFloat($(tdId).html());
        
        if (edit_vl != td_vl){
             $(editId+'btn').trigger('click');
              info(editId+' Load..\\n');
        }else {
              info(editId+' Ok!\\n');
        }
    }

");       

?>
<div style="float:left;">
        <div id="comtable"></div>
</div>

<div style="float:left;margin: 0px auto;" id="frag-1">
<?php
$setPh1Url = CController::createUrl('command/commandcal', array('command' => 'ph1'));
$setPh2Url = CController::createUrl('command/commandcal', array('command' => 'ph2'));
$setphUpUrl = CController::createUrl('command/commandcal', array('command' => 'phUp'));
$setphDownUrl = CController::createUrl('command/commandcal', array('command' => 'phDown'));
$setTds1Url = CController::createUrl('command/commandcal', array('command' => 'tds1'));
$setTds2Url = CController::createUrl('command/commandcal', array('command' => 'tds2'));
$setTdsAUrl = CController::createUrl('command/commandcal', array('command' => 'tdsA'));
$setTdsBUrl = CController::createUrl('command/commandcal', array('command' => 'tdsB'));
$setPumpDelayUrl = CController::createUrl('command/commandcal', array('command' => 'pump_delay'));
$setTdsCUrl = CController::createUrl('command/commandcal', array('command' => 'tdsC'));
$setLightUrl = CController::createUrl('command/commandcal', array('command' => 'light'));

$setDefUrl = CController::createUrl('command/command', array('command' => 'def'));
$setSensorDefUrl = CController::createUrl('command/command', array('command' => 'sensor_def'));



function addAjax($command, $path) {
    if (!yii::app()->request->isAjaxRequest) {
        yii::app()->clientScript->registerScript($command.'btn', ""
            . "jQuery('body').on('click','#".$command."btn',function(){"
            . "var valArg = document.getElementById('".$command."').value;"
            . "valArg = '" . $path . "'+'&arg='+valArg;"
            . "jQuery.ajax({'url':valArg,'cache':false,"
            . "'success':function(html){"
            . "info(html+'\\n');"
            //. "jQuery('#result').append(html);"
            . "}"
            . "});"
            . "return false;"
            . "});");
    }
    
    
}

addAjax("ph1", $setPh1Url);
addAjax("ph2", $setPh2Url);
addAjax("phUp", $setphUpUrl);
addAjax("phDown", $setphDownUrl);
addAjax("tds1", $setTds1Url);
addAjax("tds2", $setTds2Url);
addAjax("tdsA", $setTdsAUrl);
addAjax("tdsB", $setTdsBUrl);
addAjax("tdsC", $setTdsCUrl);
addAjax("def", $setDefUrl);
addAjax("light", $setLightUrl);
addAjax("pump_delay", $setPumpDelayUrl);
addAjax("sensor_def", $setSensorDefUrl);

/*
addAjax("ph1", $setPh1Url);
addAjax("ph2", $setPh2Url);
addAjax("tds", $setTdsUrl);
addAjax("hum", $setHumUrl);
addAjax("reg_delay", $setReg_delayUrl);
addAjax("tdsa", $setTdsAUrl);
addAjax("tdsb", $setTdsBUrl);
addAjax("tdsc", $setTdsCUrl);
addAjax("ph_delay", $setPhDelayCUrl);
addAjax("prof", $getProfUrl);
addAjax("reset", $setResetUrl);
*/
?>

    <style>
        .ctrl_panel{
            width:270px;
            margin:0px;
            border:1px solid greenyellow;
        }

        .ctrl_panel td{
            border:1px solid greenyellow;
        }
        .ctrl_panel input[type='button']{
            width: 70px;
        }
        .ctrl_field{
            width:50px;
            margin:0px 10px 0px 10px;
        }
    </style>
<?php

function prTd($title, $button, $field = NULL) {
    echo "<tr>";
    echo "<td>";
    echo $title;
    echo "</td>";
    echo "<td>";
    echo $field . $button;
    echo "</td>";
    //echo "<br />";
    echo "</tr>";
}

function prLine(&$var, $title, $varname, $defval) {
    if (isset($_POST[$varname])) {
        $var = $_POST[$varname];
    } else {
        $var = $defval; //6.5;
    }
    prTd("$title: ", CHtml::button('Set', array('id' => $varname . 'btn')), CHtml::textField($varname, $var, array('id' => $varname, 'class' => 'ctrl_field'))
    );
}

function prLineCal(&$var, $title, $varname, $defval) {
    if (isset($_POST[$varname])) {
        $var = $_POST[$varname];
    } else {
        $var = $defval; //6.5;
    }
    prTd("$title: ", CHtml::button('Cal', array('id' => $varname . 'btn')), CHtml::textField($varname, $var, array('id' => $varname, 'class' => 'ctrl_field'))
    );
}

function prLineGet(&$var, $title, $varname, $defval) {
    if (isset($_POST[$varname])) {
        $var = $_POST[$varname];
    } else {
        $var = $defval; //6.5;
    }
    prTd("$title: ", CHtml::button('Get', array('id' => $varname . 'btn')), CHtml::textField($varname, $var, array('id' => $varname, 'class' => 'ctrl_field'))
    );
}

$form = $this->beginWidget('CActiveForm', [
    'id' => 'my-form',
    'enableAjaxValidation' => true,
    'clientOptions' => [
        'validateOnSubmit' => true
    ]
        ]);

echo "<table class='ctrl_panel'>";

$conffile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configfile'];
// load
$config = new CConfiguration($conffile);
// access and setup
//$config[$command] = $arg;
//echo $config['mySecondOption'];
 $ramdpath = Yii::app()->params['defaults']['ramdpath'];
$phconf = @file_get_contents($ramdpath."ph");
$tdsconf = @file_get_contents($ramdpath."tds");
prLineCal($ph1, "Ph1", "ph1", $phconf);
prLineCal($ph2, "Ph2", "ph2", $phconf);
prLineCal($tds1, "Tds1", "tds1", $tdsconf);
prLineCal($tds2, "Tds2", "tds2", $tdsconf);
prLine($def, "Def", "def", 1);
prLine($sensor_def, "sensor_def", "sensor_def", 1);
prLine($pump_delay, "pump_delay", "pump_delay", 100);
prLine($phUp, "phUp", "phUp", 0);
prLine($phDown, "phDown", "phDown", 0);
prLine($tdsA, "tdsA", "tdsA", 0);
prLine($tdsB, "tdsB", "tdsB", 0);
prLine($tdsC, "tdsC", "tdsC", 0);
prLine($light, "light", "light", 1);

/*
prLine($ph1, "Ph1", "ph1", $config['ph1']);
prLine($ph2, "Ph2", "ph2", $config['ph2']);
prLine($ph_delay, "ph_delay", "ph_delay", $config['ph_delay']);
prLine($tds, "Tds", "tds", $config['tds']);
prLine($tdsa, "TdsA", "tdsa", $config['tdsA']);
prLine($tdsb, "TdsB", "tdsb", $config['tdsB']);
prLine($tdsc, "TdsC", "tdsc", $config['tdsC']);
prLine($reg_delay, "Reg_delay", "reg_delay", $config['reg_delay']);
prLine($reboot, "Reset", "reset", 1);
prLineGet($prof, "prof", "prof", 1);
*/
echo "</table>";
$this->endWidget();

?>                
    

</div>

<div style="clear:both;">
    <textarea id="result" style="width: 100%;"></textarea>
</div>

