<?php
/* @var $this CommandController */

$this->breadcrumbs = array(
    'Управление',
);

$this->menu=array(
        array('label'=>'Создать профиль', 'url'=>array('profile/create')),
	array('label'=>'Профили ', 'url'=>array('profile/admin')),
	array('label'=>'Создать цикл', 'url'=>array('circle/create')),
        array('label'=>'Циклы', 'url'=>array('circle/admin')),
        array('label'=>'Калибровка', 'url'=>array('command/cal')),
        
);
Yii::app()->clientScript->registerScript("lastload", "

$(function(){    

    window.onload = setupRefresh;



    function setupRefresh() {

      //setTimeout('refreshPage();', 5000); 
      refreshPage();
      setInterval(refreshPage, 5000 );

    }

    function refreshPage() {

       $(document).ready(function(){

       $('#comtable').load('/index.php?r=site/loadTable2', 
        function() {
	}
       );
       });
   }
});

    function info(text){
        var textarea = document.getElementById('result');
        $('#result').append(text);
        textarea.scrollTop = textarea.scrollHeight;
    }
    
    function onTrigger(editId, tdId){
        var edit_vl = parseFloat($(editId).val());
        var td_vl = parseFloat($(tdId).html());
        
        if (edit_vl != td_vl){
             $(editId+'btn').trigger('click');
              info(editId+' Load..\\n');
        }else {
              info(editId+' Ok!\\n');
        }
    }

");       

$com = "pidof -x alfa_checker.sh";
$out = "";
$p = proc_open($com, array(1 => array('pipe', 'w'),
    2 => array('pipe', 'w')), $io);

while (!feof($io[1])) {
    $out .= htmlspecialchars(fgets($io[1]), ENT_COMPAT, 'UTF-8');
}
while (!feof($io[2])) {
    $out .= htmlspecialchars(fgets($io[2]), ENT_COMPAT, 'UTF-8');
}

fclose($io[1]);
fclose($io[2]);
proc_close($p);
echo "<span id='serviceinfo'>";
if ($out!="" && $out>0){
    echo "Сервис работает ID=".$out;
    $out = 1;
}else{ 
    $out = 0;  
    echo "Сервис не работает";
}    
echo "</span>";

if (!yii::app()->request->isAjaxRequest) {
    $onofUrl = CController::createUrl('command/commandonof', array('command' => 'onof'));
    yii::app()->clientScript->registerScript('btns', ""
            . "function check(el, vlcheck){"
            //. "var valArg = el.value;"
            . "if (vlcheck == 0){"
            . "el.value = 'Вкл';"
            . "return 0;"
            . "} else {"
            . "el.value = 'Выкл';"
            . "return 1;"
            . "}"
            . "}"
           // . "var r1val = check(document.getElementById('r1btn')," . $r1 . ");"
           // . "var r2val = check(document.getElementById('r2btn')," . $r2 . ");"
            . "var onofval = check(document.getElementById('onofbtn')," . $out . ");"
            //. "var coolval = check(document.getElementById('coolbtn')," . $cool . ");"
            //. "var onofledval = check(document.getElementById('onofledbtn')," . $led . ");"
            . "");
    
    yii::app()->clientScript->registerScript('onofbtn', ""
            . "jQuery('body').on('click','#onofbtn',function(){"
            //."alert('?');"
            . "var el = document.getElementById('onofbtn');"
            //. "var valArg = document.getElementById('arg').value;"
            . "if (el.value == 'Выкл'){"
            . "onofval = 0;"
            . "} else {"
            . "onofval = 1;"
            . "}"
            //."alert('!!');"
            . "valArg = '" . $onofUrl . "&val='+onofval+'&arg=null';"
            //."alert(valArg);"
            . "jQuery.ajax({'url':valArg,'cache':false,"
            . "'success':function(html){"
            //."alert(html);"
            . "jQuery('#result').html(html);"
            . "if (el.value == 'Выкл') {"
            . "el.value = 'Вкл';"
            . "info('Сервис остановлен'+'\\n');"
            . "}else{"
            . "el.value = 'Выкл';"
            . "info('Сервис запущен'+'\\n');"
            . "}"
            . "}"
            . "});"
            . "return false;"
            . "});");
}    
echo CHtml::button('?', array('id' => 'onofbtn'));
?>
<div style="float:left;">
        <div id="comtable"></div>
</div>





<div style="float:left;margin: 0px auto;" id="frag-1">
<?php
$setPh1Url = CController::createUrl('command/command', array('command' => 'ph1'));
$setPh2Url = CController::createUrl('command/command', array('command' => 'ph2'));
$setTdsUrl = CController::createUrl('command/command', array('command' => 'tds'));
$setHumUrl = CController::createUrl('command/command', array('command' => 'hum'));
$setTdsAUrl = CController::createUrl('command/command', array('command' => 'tdsA'));
$setTdsBUrl = CController::createUrl('command/command', array('command' => 'tdsB'));
$setTdsCUrl = CController::createUrl('command/command', array('command' => 'tdsC'));
$setPhDelayCUrl = CController::createUrl('command/command', array('command' => 'ph_delay'));
$setReg_delayUrl = CController::createUrl('command/command', array('command' => 'reg_delay'));
$setResetUrl = CController::createUrl('command/command', array('command' => 'reset'));
$getProfUrl = CController::createUrl('command/commandget', array('command' => 'prof'));
$setDayUrl = CController::createUrl('command/command', array('command' => 'day'));
$setNightUrl = CController::createUrl('command/command', array('command' => 'night'));
$setLightUrl = CController::createUrl('command/command', array('command' => 'light'));

function addAjax($command, $path) {
    if (!yii::app()->request->isAjaxRequest) {
        yii::app()->clientScript->registerScript($command.'btn', ""
            . "jQuery('body').on('click','#".$command."btn',function(){"
            . "var valArg = document.getElementById('".$command."').value;"
            . "valArg = '" . $path . "'+'&arg='+valArg;"
            . "jQuery.ajax({'url':valArg,'cache':false,"
            . "'success':function(html){"
            . "info(html+'\\n');"
            //. "jQuery('#result').append(html);"
            . "}"
            . "});"
            . "return false;"
            . "});");
    }
    
    
}

addAjax("ph1", $setPh1Url);
addAjax("ph2", $setPh2Url);
addAjax("tds", $setTdsUrl);
addAjax("hum", $setHumUrl);
addAjax("reg_delay", $setReg_delayUrl);
addAjax("tdsa", $setTdsAUrl);
addAjax("tdsb", $setTdsBUrl);
addAjax("tdsc", $setTdsCUrl);
addAjax("ph_delay", $setPhDelayCUrl);
addAjax("prof", $getProfUrl);
addAjax("reset", $setResetUrl);
addAjax("day", $setDayUrl);
addAjax("night", $setNightUrl);
addAjax("light", $setLightUrl);

?>

    <style>
        .ctrl_panel{
            width:250px;
            margin:0px;
            border:1px solid greenyellow;
        }

        .ctrl_panel td{
            border:1px solid greenyellow;
        }
        .ctrl_panel input[type='button']{
            width: 70px;
        }
        .ctrl_field{
            width:40px;
            margin:0px 0px 0px 10px;
        }
    </style>
<?php

function prTd($title, $button, $field = NULL) {
    echo "<tr>";
    echo "<td>";
    echo $title;
    echo "</td>";
    echo "<td>";
    echo $field . $button;
    echo "</td>";
    //echo "<br />";
    echo "</tr>";
}

function prLine(&$var, $title, $varname, $defval) {
    if (isset($_POST[$varname])) {
        $var = $_POST[$varname];
    } else {
        $var = $defval; //6.5;
    }
    prTd("$title: ", CHtml::button('Set', array('id' => $varname . 'btn')), CHtml::textField($varname, $var, array('id' => $varname, 'class' => 'ctrl_field'))
    );
}

function prLineGet(&$var, $title, $varname, $defval) {
    if (isset($_POST[$varname])) {
        $var = $_POST[$varname];
    } else {
        $var = $defval; //6.5;
    }
    prTd("$title: ", CHtml::button('Get', array('id' => $varname . 'btn')), CHtml::textField($varname, $var, array('id' => $varname, 'class' => 'ctrl_field'))
    );
}

$form = $this->beginWidget('CActiveForm', [
    'id' => 'my-form',
    'enableAjaxValidation' => true,
    'clientOptions' => [
        'validateOnSubmit' => true
    ]
        ]);

echo "<table class='ctrl_panel'>";

$conffile = Yii::getPathOfAlias('application.config').'/'.Yii::app()->params['defaults']['configfile'];
// load
$config = new CConfiguration($conffile);
// access and setup
//$config[$command] = $arg;
//echo $config['mySecondOption'];


prLine($ph1, "Ph1", "ph1", $config['ph1']);
prLine($ph2, "Ph2", "ph2", $config['ph2']);
prLine($ph_delay, "ph_delay", "ph_delay", $config['ph_delay']);
prLine($tds, "Tds", "tds", $config['tds']);
prLine($tdsa, "TdsA", "tdsa", $config['tdsA']);
prLine($tdsb, "TdsB", "tdsb", $config['tdsB']);
prLine($tdsc, "TdsC", "tdsc", $config['tdsC']);
prLine($reg_delay, "Reg_delay", "reg_delay", $config['reg_delay']);
prLine($reboot, "Reset", "reset", 1);
prLineGet($prof, "prof", "prof", 1);
prLine($day, "Day", "day", 12);
prLine($night, "Night", "night", 12);
prLine($light, "Light", "light", 0);

echo "</table>";
$this->endWidget();

try {
    
$profs = Profile::model()->findAll(array('order' => 'name'));
 
// при помощи listData создаем массив вида $ключ=>$значение
$list = CHtml::listData($profs, 'id', 'name');

echo CHtml::dropDownList('profile_id', $profs, 
              $list,
              array(
                  'options'=>array($config['profile_id']=> array('selected'=>true)),
                  'empty' => 'Select a profile',
                  'ajax' => array(
                    'type'=>'POST', 
                    'url'=>Yii::app()->createUrl('command/loadcircles'), //or $this->createUrl('loadcities') if '$this' extends CController
                    'update'=>'#circles',
                    //'success' => 'function(data){alert(data);}',
                    'data'=>array('profile_id'=>'js:this.value'),
                    ),
             )
        );

$circle = Circle::model()->findAll(array('order' => 'name','condition'=>'profile_id='.$config['profile_id']));
 
// при помощи listData создаем массив вида $ключ=>$значение
$list2 = CHtml::listData($circle, 'id', 'name');
/*yii::app()->clientScript->registerScript("loadcircle", "function(){"
        . ""
        . ""
        . ""
        . ""
        . "}");
 * 
 */
echo CHtml::dropDownList('circles', $circle, 
              $list2,
              array(
                  'empty' => 'Select circle',
                  'options'=>array($config['circle_id']=>array('selected'=>true)),
                  'ajax' => array(
                    'type'=>'POST', 
                    'url'=>Yii::app()->createUrl('command/loadcircle'), //or $this->createUrl('loadcities') if '$this' extends CController
                    //'update'=>'#circles',
                    'success' => 'function(data){var obj = $.parseJSON(data);'
                      . '$(\'#ph1\').val(obj[\'ph1\']);'
                      . '$(\'#ph2\').val(obj[\'ph2\']);'
                      . '$(\'#ph_delay\').val(obj[\'ph_delay\']);'
                      . '$(\'#tds\').val(obj[\'tds\']);'
                      . '$(\'#tdsa\').val(obj[\'tdsA\']);'
                      . '$(\'#tdsb\').val(obj[\'tdsB\']);'
                      . '$(\'#tdsc\').val(obj[\'tdsC\']);'
                      . '$(\'#reg_delay\').val(obj[\'reg_delay\']);'
                    //  .'alert(data);'
                      . '}',
                      
                    'data'=>array('circle_id'=>'js:this.value'),
                    ),
            )      
        );

echo CHtml::button('Set', array(
                  'ajax' => array(
                    'type'=>'POST', 
                    'url'=>Yii::app()->createUrl('command/setcircle'), //or $this->createUrl('loadcities') if '$this' extends CController
                    //'update'=>'#circles',
                    'success' => 'function(data){'
                      .'var interval = 2000;'
                      . 'setTimeout(function() { $(\'#profbtn\').trigger(\'click\');}, interval);'
                      . 'setTimeout(function() { onTrigger(\'#ph1\', \'#ph1_val\');}, interval*2);'
                      . 'setTimeout(function() { onTrigger(\'#ph2\', \'#ph2_val\');}, interval*3);'
                      . 'setTimeout(function() { onTrigger(\'#ph_delay\', \'#ph_delay_val\');}, interval*4);'
                      . 'setTimeout(function() { onTrigger(\'#tds\', \'#tds_val\');}, interval*5);'
                      . 'setTimeout(function() { onTrigger(\'#tdsa\', \'#tdsa_val\');}, interval*6);'
                      . 'setTimeout(function() { onTrigger(\'#tdsb\', \'#tdsb_val\');}, interval*7);'
                      . 'setTimeout(function() { onTrigger(\'#tdsc\', \'#tdsc_val\');}, interval*8);'
                      . 'setTimeout(function() { onTrigger(\'#reg_delay\', \'#reg_delay_val\');}, interval*9);'
                      . '}',
                    'data'=>array('circle_id'=>'js:circles.value', 'profile_id'=>'js:profile_id.value'),
                    ),
    
));

}
catch(Exception $ex) {
        echo $ex->getMessage();
}
?>                
    

</div>

<div style="clear:both;">
    <textarea id="result" style="width: 100%;"></textarea>
</div>
