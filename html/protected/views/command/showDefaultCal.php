<?php
/* @var $this ProfileController */
/* @var $dataProvider CActiveDataProvider */

?>
<div class="calibr" style = "height: 550px;">
			<h1>Сохранение заводских настроек</h1>
			<div class = "itm center">
				<?php if ($def) : ?>
				<p class = "def_text">Установлены заводские настройки</p>
				<?php else : ?>
				<?php echo CHtml::link('Сохранить', array('command/saveDefaultCal'), array('class' => 'btn', 'id' => 'cal_default')); ?>
				<?php endif; ?>
				
			</div>

</div>
<div class = "">
	<?php echo CHtml::link('НАЗАД', array('site/deviceSettings2'), array('class' => 'standart_button_center')); ?>
</div>
