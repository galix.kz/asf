<script>
$(document).ready(function(){
	$('.gotovo_standart').click(function(){
		var val = $(this).parents('.botpopup').find('input[type=hidden]').val();
		var nameVal = ($(this).attr('data-id') == "#val_ph1") ? "cal_ph_1" : "cal_ph_2";
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/ajaxSaveParamVal"); ?>',
			data: {nameVal, val},
			success: function(html){
//				console.log(html);
			}
		});
		
		
	});
	$('#fix_ph1').click(function(){
		var val = $('#val_ph1').text();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/saveCalPh"); ?>',
			data: {nbPoint : 1, valReal : val},
			success: function(html){
				$('#itm_ph1').addClass('wait');
				$('#itm_ph2').removeClass('wait');
			}
		});
		return false;
		
	});
	$('#fix_ph2').click(function(){
		var val = $('#val_ph2').text();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/saveCalPh"); ?>',
			data: {nbPoint : 2, valReal : val},
			success: function(html){
				$('#itm_ph2').addClass('wait');
				$('#itm_ph3').removeClass('wait');
			}
		});
		return false;
		
	});
});
</script>
<div class="calibr-step">
			<div class="itm itmCal" id = "itm_ph1">
				<p>Поместите датчик в раствор:</p>
				<h3>pH <span id = "val_ph1"><?php echo $param1; ?></span></h3>
				<span class="btn showpop">Изменить pH</span>
				<a class="btn" href="#" id = "fix_ph1">Фиксировать</a>
				<p class="opti">Дождитесь стабильного значения pH:  <br/><span class = "val-online" data-val = "ph"><?php echo Data::getCurrentValue('ph'); ?></span></p>
				<div class="botpopup">
					<div class="steps-step">
						<div class="row heads">
							<div class="col1">
								<p class="sz2">Значение pH</p>
							</div>
						</div>
						<div class="row cont">
							<div class="col1-1">
							<?php echo ModernForm::sliderChooseNoModel(3.0, 0.1, 21, $param1); ?>
							</div>
						</div>
						<div class="row btms last">
							<a class="btn btn-l otmena_standart" href="#">Отмена</a>
							<a class="btn btn-r gotovo_standart" data-id = "#val_ph1" href="#">ГОТОВО</a>
						</div>
					</div>
				</div>
			</div>
			<div class="itm itmCal wait" id = "itm_ph2">
				<h3>pH <span id = "val_ph2"><?php echo $param2; ?></span></h3>
				<span class="btn showpop">Изменить pH</span>
				<a class="btn" href="#" id = "fix_ph2">Фиксировать</a>
				<p class="opti">Дождитесь стабильного значения pH: <br/><span class = "val-online" data-val = "ph"><?php echo Data::getCurrentValue('ph'); ?></span></p>
				<div class="botpopup">
					<div class="steps-step">
						<div class="row heads">
							<div class="col1">
								<p class="sz2">Значение pH</p>
							</div>
						</div>
						<div class="row cont">
							<div class="col1-1">
							<?php echo ModernForm::sliderChooseNoModel(6.0, 0.1, 21, $param2); ?>
							</div>
						</div>
						<div class="row btms last">
							<a class="btn btn-l otmena_standart" href="#">Отмена</a>
							<a class="btn btn-r gotovo_standart" data-id = "#val_ph2" href="#">ГОТОВО</a>
						</div>
					</div>
				</div>
			</div>
			<div class="itm itmCal wait" id = "itm_ph3">
			<br>
				<h3>Датчик откалиброван</h3>
			</div>
			<div class="btm">
				<?php echo CHtml::link('Назад к калибровке', array('command/indexNew'), array('style' => 'line-height: 50px;')); ?>
			</div>
		</div>