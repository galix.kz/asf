<script>
$(document).ready(function(){
	$('.gotovo_standart').click(function(){
		var val = $(this).parents('.botpopup').find('input[type=hidden]').val();
		var nameVal = ($(this).attr('data-id') == "#val_tds1") ? "cal_tds_1" : "cal_tds_2";
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/ajaxSaveParamVal"); ?>',
			data: {nameVal, val},
			success: function(html){
//				console.log(html);
			}
		});
		
		
	});
	$('#fix_tds1').click(function(){
		var val = $('#val_tds1').text();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/saveCalTds"); ?>',
			data: {nbPoint : 1, valReal : val},
			success: function(html){
				$('#itm_tds1').addClass('wait');
				$('#itm_tds2').removeClass('wait');
			}
		});
		return false;
		
	});
	$('#fix_tds2').on('click', function(){
		var val = $('#val_tds2').text();
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/saveCalTds"); ?>',
			data: {nbPoint : 2, valReal : val},
			success: function(html){
				$('#itm_tds2').addClass('wait');
				$('#itm_tds3').removeClass('wait');
			}
		});
		return false;
		
	});
});
</script>
<div class="calibr-step">
			<div class="itm itmCal" id = "itm_tds1">
				<p>Поместите датчик в раствор:</p>
				<h3>TDS <span id = "val_tds1"><?php echo $param1; ?></span></h3>
				<span class="btn showpop">Изменить TDS</span>
				<a class="btn" href="#" id = "fix_tds1">Фиксировать</a>
				<p class="opti">Дождитесь стабильного значения TDS: <br/><span class = "val-online" data-val = "tds" > <?php echo Data::getCurrentValue('tds'); ?></span></p>
				<div class="botpopup">
					<div class="steps-step">
						<div class="row heads">
							<div class="col1">
								<p class="sz2">Значение TDS</p>
							</div>
						</div>
						<div class="row cont">
							<div class="col1-1">
							<?php echo ModernForm::sliderChooseNoModel(0, 50, 31, $param1); ?>
							</div>
						</div>
						<div class="row btms last">
							<a class="btn btn-l otmena_standart" href="#">Отмена</a>
							<a class="btn btn-r gotovo_standart" data-id = "#val_tds1" href="#">ГОТОВО</a>
						</div>
					</div>
				</div>
			</div>
			<div class="itm itmCal wait" id = "itm_tds2">
				<h3>TDS <span id = "val_tds2"><?php echo $param2; ?></span></h3>
				<span class="btn showpop">Изменить TDS</span>
				<a class="btn" href="#" id = "fix_tds2">Фиксировать</a>
				<p class="opti">Дождитесь стабильного значения TDS:<br/><span class = "val-online" data-val = "tds"><?php echo Data::getCurrentValue('tds'); ?></span></p>
				<div class="botpopup">
					<div class="steps-step">
						<div class="row heads">
							<div class="col1">
								<p class="sz2">Значение TDS</p>
							</div>
						</div>
						<div class="row cont">
							<div class="col1-1">
							<?php echo ModernForm::sliderChooseNoModel(1000, 50, 41, $param2); ?>
							</div>
						</div>
						<div class="row btms last">
							<a class="btn btn-l otmena_standart" href="#">Отмена</a>
							<a class="btn btn-r gotovo_standart" data-id = "#val_tds2" href="#">ГОТОВО</a>
						</div>
					</div>
				</div>
			</div>
			<div class="itm itmCal wait" id = "itm_tds3">
				<h3>Датчик откалиброван</h3>
			</div>
			<div class="btm">
				<?php echo CHtml::link('Назад к калибровке', array('command/indexNew'), array('style' => 'line-height: 50px;')); ?>
			</div>
		</div>