<?php $settings = Settings::model()->findByPk(1);?>
<script>
$(document).ready(function(){
	function buttonback(button, text){
		$('.turnPump').removeClass('btn_disabled');
		button.text(text);
	}
	function buttoncalibrback(button, text){
		button.removeClass('btn_disabled');
		button.text(text);
	}
	$('.turnPump').click(function(){
		var th = $(this);
		if(th.hasClass('btn_disabled'))
			return false;
		$('.turnPump').addClass('btn_disabled');
//		th.addClass('btn_disabled');
		var nbPump = $(this).attr('data-nbpump');
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("command/turnPump"); ?>',
			data: {nb : nbPump},
			success: function(html){
				th.text('Качаем');
				setTimeout(buttonback, <?php echo $settings->pump_work_second * 1000; ?>, th, 'Прокачать');
			}
		});
		return false;
		
		
	});
	$('#gotovo_pumps').click(function(){
		var val = $(this).parents('.botpopup').find('input[type=hidden]').val();
		if(val > 0)
		{
			$(this).parents('.botpopup').removeClass('active');
			var url = '<?php echo $this->createUrl("command/setTimePumps"); ?>';
			url += ('&time=' + val);
			window.location.href = url;
		}		return false;
	});
	$('#change_time_pumps').click(function(){
		$('.botpopup').addClass('active');
		return false;
	});
	<?php if(isset($_GET['fromC'])) : ?>
		var elem = "#btn_<?php echo $_GET['fromC']; ?>";
		var th = $(elem);
		th.addClass('btn_disabled');
		th.text("Успешно!");
		setTimeout(buttoncalibrback, <?php echo $settings->pump_work_second * 1000; ?>, th, 'Калибровать');
		
	<?php endif; ?>
	
});
</script>
		<div class="calibr-itm">
			<div class="heads">ВРЕМЯ РАБОТЫ НАСОСОВ</div>
			<div class="itms">
				<div class="itm" style = "border-bottom: 1px solid #C7C7CC;margin-bottom: 30px;padding: 20px 25px;">
					<div class="name"><?php echo $settings->pump_work_second; ?> сек</div>
					<a class="btn" id = "change_time_pumps" href="#">Изменить</a>
						<div class="botpopup pumps">
						<div class="top">
							ВРЕМЯ РАБОТЫ НАСОСОВ
						</div>
						<div class="steps-step">
							<div class="heads">
								Укажите время в секундах
							</div>
							<div class="row cont">
								<div class="col1-1">
								<?php echo ModernForm::sliderChooseNoModel(1, 1, 100, $settings->pump_work_second); ?>
								</div>
							</div>
							<div class="row btms">
								<a class="btn btn-l otmena_standart" style = "float:left" href="#">Отмена</a>
								<a class="btn btn-r" id = "gotovo_pumps" href="#">ГОТОВО</a>
							</div>
						</div>
						</div>
				</div>

				<div class="itm" >
					<div class="name">pH UP</div>
					<?php echo CHtml::link("Калибровать", array("command/correctPumps", 'command' => 'phUp'), array('class' => 'btn', 'style' => 'width: 130px;margin-left: 10px;', 'id' => 'btn_phUp')); ?>
					<a class="btn turnPump" data-nbpump = "0" href="#" >Прокачать</a>
				</div>
				<div class="itm">
					<div class="name">pH DOWN</div>
					<?php echo CHtml::link("Калибровать", array("command/correctPumps", 'command' => 'phDown'), array('class' => 'btn', 'style' => 'width: 130px;margin-left: 10px;', 'id' => 'btn_phDown')); ?>
					<a class="btn turnPump" data-nbpump = "1" href="#">Прокачать</a>
				</div>
				<div class="itm">
					<div class="name">Soil A</div>
					<?php echo CHtml::link("Калибровать", array("command/correctPumps", 'command' => 'tdsA'), array('class' => 'btn', 'style' => 'width: 130px;margin-left: 10px;', 'id' => 'btn_tdsA')); ?>
					<a class="btn turnPump" data-nbpump = "2" href="#">Прокачать</a>
				</div>
				<div class="itm">
					<div class="name">Soil B</div>
					<?php echo CHtml::link("Калибровать", array("command/correctPumps", 'command' => 'tdsB'), array('class' => 'btn', 'style' => 'width: 130px;margin-left: 10px;', 'id' => 'btn_tdsB')); ?>
					<a class="btn turnPump" data-nbpump = "3" href="#">Прокачать</a>
				</div>
				<div class="itm">
					<div class="name">Soil C</div>
					<?php echo CHtml::link("Калибровать", array("command/correctPumps", 'command' => 'tdsC'), array('class' => 'btn', 'style' => 'width: 130px;margin-left: 10px;', 'id' => 'btn_tdsC')); ?>
					<a class="btn turnPump" data-nbpump = "4" href="#">Прокачать</a>
				</div>
			</div>
			<div class="btm">
				<?php echo CHtml::link('Назад', array('command/indexNew'), array('class' => 'btn')); ?>
			</div>
		</div>