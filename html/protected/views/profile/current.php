
<?php if( $settings->active_profile_id &&  $settings->active_profile_id > 0) : ?>
<script>
$(document).ready(function(){
	$('#excel_now_id').click(function(){
		var url = '<?php echo $this->createUrl("profile/excelNow"); ?>';
		window.location.href = url;
		return false;
	});
});
</script>
		<div class="controls">
			<div class="itm wait">
				<div class="name">Текущий профиль</div>
				<a href="#" class="btn">Изменить</a>
				<div class="value"><?php echo $model->name; ?></div>
				
			</div>
			<div class="itm wait">
				<div class="name">Статус рассады</div>
				<a href="#" class="btn">Изменить</a>
				<div class="value">День от закладки - <?php echo (strtotime('today') - $settings->active_start_day) / (24 * 60 * 60);?></div>
				
			</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'circle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
			<div class="row">
			<input type = "hidden" name = "stop" value = "1" />
			<?php echo CHtml::submitButton('ОСТАНОВИТЬ', array('class' => 'btn start red')); ?>
			</div>
<?php $this->endWidget(); ?>
			<div class="btns">
<?php if($this->layout == "columnRemote") : ?>
			<a href = "#" id = "excel_now_id" style = "border-bottom: 2px solid #898989;" >ВЫГРУЗИТЬ В EXCEL</a>
	<?php endif; ?>
<?php 
			if($cycle) :
?>			
				<?php echo CHtml::link('ТЕКУЩИЕ НАСТРОЙКИ', array('profile/currentCycle')); ?>
<?php else : ?>				
				<?php echo CHtml::link('СОЗДАТЬ ПРОФИЛЬ', array('profile/index')); ?>
	<?php endif; ?>
				<?php echo CHtml::link('ИСТОРИЯ ВЫРАЩИВАНИЯ', array('profile/history')); ?>
			</div>
		</div>

<?php else : ?>
<script>
$(document).ready(function(){
	$('#select_profile_button').click(function(){
		var val_n = $(this).parents('.itm').find('input[type=hidden]').val();
		var val = $(this).parents('.itm').find('input[type=hidden]').attr('data-text');
		$(this).parents('.itm').find('.value').html(val);
		if(val_n > 0)
		{
			$('#status_rass').show();
			$(this).parents('.itm').find('.mainb').html('Изменить');
			$(this).parents('.itm').next().removeClass('wait');
		}
		else
		{
			$(this).parents('.itm').find('.mainb').html('Выбрать');
			$(this).parents('.itm').next().addClass('wait');
			$('#button_start').addClass('unactive');
		}
	});
	$('#select_profile_button_vir').click(function(){
		var val_n = 1; // id зацикленного выращивания
		$(this).parents('.itm').find('input[type=hidden]').val(val_n);
		var val = $(this).text();
		$(this).parents('.itm').find('.value').html(val);
		if(val_n > 0)
		{
			$(this).parents('.itm').find('.mainb').html('Изменить');
			$(this).parents('.itm').next().next().removeClass('wait');
			$('#button_start').removeClass('unactive');
			$('#status_rass').hide();
		}
	});
	$('#select_day_count_button').click(function(){
		var val_n = $(this).parents('.itm').find('input[type=hidden]').val();
		var val = $(this).parents('.itm').find('input[type=hidden]').attr('data-text');
		$(this).parents('.itm').find('.value').html(val);
		if(val_n >= 0)
		{
			$(this).parents('.itm').find('.mainb').html('Изменить');
			$('#button_start').removeClass('unactive');
		}
		else
		{
			$(this).parents('.itm').find('.mainb').html('Выбрать');
			$('#button_start').addClass('unactive');
		}
	});
	$('#button_start').click(function(){
		if($(this).hasClass('unactive'))
			return false;
	});
	
	$('#edit_profile_button').click(function(){
		var val_n = $(this).parents('.itm').find('input[type=hidden]').val();
		if(val_n > 0)
		{
			var url = '<?php echo $this->createUrl("profile/view"); ?>';
			url += ('&id=' + val_n);
			window.location.href = url;
		}
	});
});
</script>

<div class="controls">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'circle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
			<div class="itm">
				<div class="name">Выбран профиль</div>
				<a href="#" class="btn mainb">Выбрать</a>
				<div class="value value_small">Не выбрано</div>
				<div class="botpopup prof">
<!--					
					<div class="top">
						Зацикленное выращивание
					</div>
-->					
					
					<div class="steps-step">
						<div class="heads">
							Выбрать профиль выращивания:
						</div>
						<div class="row cont">
							<div class="col1">
								<div class="select-values" id="ph1" data-slick='{"initialSlide": 0}'>
									<div class="slide"><input type = "hidden" name = "profile_id" value = "" />&nbsp;</div>
									<div class="slide" data-text = 'Не выбрано'>Не выбрано</div>
									<?php foreach($profile_list as $one)
										if($one->id != 1)
											echo "<div class='slide' data-val = '$one->id' data-text = '$one->name'>$one->name</div>";
									?>
									<div class="slide">&nbsp;</div>
								</div>
							</div>
						</div>
						<div class="prof-btns">
							<a class="btn btn-l" href="#">ОТМЕНА</a>
							<a class="btn btn-r" href="#" id = "select_profile_button" >ВЫБРАТЬ</a>
							<a class="btn btn-c" href="#" id = "select_profile_button_vir"><?php echo Profile::model()->findByPk(1)->name;?></a>
							<?php echo CHtml::link("ПРОФИЛИ ВЫРАЩИВАНИЯ", array('profile/index'), array('class' => 'btn btn-c')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="itm wait" id = "status_rass">
				<div class="name">Статус рассады</div>
				<a href="#" class="btn mainb">Выбрать</a>
				<div class="value">Не выбрано</div>
				<div class="botpopup stat">
					<div class="steps-step">
						<div class="heads">
							Выберите статус рассады:
						</div>
						<div class="row cont">
							<div class="col1">
								<div class="select-values" id="ph1" data-slick='{"initialSlide": 0}'>
									<div class="slide"><input name = "day_count_from" type = "hidden">&nbsp;</div>
									<div class="slide" data-val = "-1" data-text = 'Не выбрано'>Не выбрано</div>
									<div class="slide" data-val = "0" data-text = "День 0. Закладка семени">День 0. Закладка семени</div>
									<div class="slide" data-val = "1" data-text = "День 1 от закладки">День 1 от закладки</div>
									<?php for($i = 2; $i < 50; $i++) 
										echo '<div class="slide" data-val = "'.$i.'" data-text = "День '.$i.'">День '.$i.'</div>';
									?>
									<div class="slide">&nbsp;</div>
								</div>
							</div>
						</div>
						<div class="prof-btns">
							<a class="btn btn-l" href="#">ОТМЕНА</a>
							<a class="btn btn-r" href="#" id = "select_day_count_button">ГОТОВО</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<?php echo CHtml::submitButton('СТАРТ', array('class' => 'btn start unactive', 'id' => 'button_start')); ?>
<?php $this->endWidget(); ?>
			<div class="btns">
				<?php echo CHtml::link('ПРОФИЛИ ВЫРАЩИВАНИЯ', array('profile/index')); ?>
				<?php echo CHtml::link('ИСТОРИЯ ВЫРАЩИВАНИЯ', array('profile/history')); ?>
			</div>
		</div>
	
<?php endif; ?>