			<div class="botpopup keybo">
				<div class="lang">
<?php if(!(isset($hideRus) && $hideRus)) : ?>
					<a href="#" class="lng pst ru" id = "href_rus">RU</a>
<?php endif; ?>					
<?php if(!(isset($hideEng) && $hideEng)) : ?>
					<a href="#" class="lng pst en" id = "href_eng">ENG</a>
<?php endif; ?>					
					<a href="#" class="lng pst num active" id = "href_num">123</a>
					<a href="#" class="lng upper" id = "href_upper">a-A</a>
					
				</div>
				<div class="keys select-values" id="word_rus" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						foreach (range(chr(0xE0),chr(0xFF)) as $v) 
							echo '<div class="slide">'.(iconv('CP1251','UTF-8',strtolower($v))).'</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="keys select-values" id="word_eng" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						for($c = 'a'; $c < 'z'; $c++)
							echo '<div class="slide">'.$c.'</div>';
						echo '<div class="slide">z</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				
				<div class="keys select-values active" id="word_num" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						for($c = '0'; $c <= '9'; $c++)
							echo '<div class="slide">'.$c.'</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="btns">
					<a href="#" class="btn btn-l" id = "button_steret">Стереть</a>
					<a href="#" class="btn btn-r" id = "button_vstavit">Вставить</a>
				</div>
			</div>
