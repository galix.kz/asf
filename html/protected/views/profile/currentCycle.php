<div class = "viewCycle">

			<div class="steps-step">
<?php
?>
	<div class = "top">
		Фаза роста #<?php echo $profile->getNumberCycle($model); ?>
		<p>(c учетом плавного изменения)</p>
	</div>
	
	<div class = "cont">
		<div class = "row2">
			Продолжительность<div class = "value"><?php echo $model->day_count;?> дней</div>
		</div>
		<div class = "row2">
			Настройки pH<div class = "value"><?php echo $model->ph_min." - ".$model->ph_max;?> pH</div>
		</div>
		<div class = "row2">
			<div class = "value"><?php echo $model->ph_ml;?> мл</div>
		</div>
		<div class = "row2">
			Настройки TDS<div class = "value"><?php echo $model->tds_min." - ".$model->tds_max;?> ppm</div>
		</div>
		<div class = "row2">
			<div class = "value"><?php echo "soilA - ".$model->soil_a.", soilB - ".$model->soil_b.", soilC - ".$model->soil_c;?></div>
		</div>
		<div class = "row2">
			Температура воды<div class = "value"><?php echo $model->tank_t_min." - ".$model->tank_t_max;?> &deg;C</div>
		</div>
		<div class = "row2">
			Режим дня<div class = "value"><?php echo $model->light_from." - ".$model->light_to;?>
					<?php // if($model->light_gradual) : ?>
						<?php // echo " ( ".$model->GetCorrectLightFrom2()." - ".$model->GetCorrectLightTo2()." ) ";?>
					<?php // endif; ?>
			</div>
		</div>
		<div class = "row2">
			Влажность<div class = "value"><?php echo $model->hum_min." - ".$model->hum_max;?> %</div>
		</div>
		<div class = "row2">
			Температура воздуха<div class = "value"><?php echo $model->microclimate_t_day." - ".$model->microclimate_t_night;?> &deg;C</div>
		</div>
		<div class = "row2">
			Содержание CO2<div class = "value"><?php echo $model->co2_min." - ".$model->co2_max;?> ppm</div>
		</div>
		<div class = "row2">
			Расписание CO2<div class = "value"><?php if($model->co2_time1_from) echo $model->co2_time1_from." - ".$model->co2_time1_to;?>
			</div>
		</div>
		<?php if($model->co2_time2_from) : ?>
		<div class = "row2">
			<div class = "value"><?php echo $model->co2_time2_from." - ".$model->co2_time2_to;?></div>
		</div>
		<?php endif; ?>
		<?php if($model->co2_time3_from) : ?>
		<div class = "row2">
			<div class = "value"><?php echo $model->co2_time3_from." - ".$model->co2_time3_to;?></div>
		</div>
		<?php endif; ?>
		<?php if($model->tds_gradual || $model->light_gradual) :?>
		<div class = "row2">
			Плавное изменение<div class = "value"><?php if ($model->tds_gradual) echo "TDS  "; if ($model->light_gradual) echo "Light  ";?></div>
		</div>
		<?php endif;?>
	</div>
	<div class = "prebtns">
					<?php echo CHtml::link('НАЗАД', ($profile->id == 1) ? array('profile/index') : array('profile/current'), array('class' => 'btn btn-c')); ?>
	</div>

	</div>

</div>
