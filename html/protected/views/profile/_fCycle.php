<script>
$(document).ready(function(){
	function getForm(){
		$.ajax({
			method: 'get',
			url: '<?php echo $this->createUrl("profile/addCycle", array('profile_id' => $profile_id, 'cycle_id' => ($model->id ? $model->id : NULL)));?>',
			success: function(html){
				$('#pr').html(html);
				$('#loading').hide();
			}
		});	
	}
	
	getForm();
	
	$('#pr').on('click', '#button_next', function(){
		$('#loading').show();
		var fdata = $('#profile-form-cycle').serialize();
		$('#pr').html('');
		$.ajax({
			method: 'post',
			url: '<?php echo $this->createUrl("profile/addCycle", array('profile_id' => $profile_id, 'cycle_id' => ($model->id ? $model->id : NULL)));?>',
			data: fdata,
			success: function(html){
				if(html.length < 200) // ответ в виде id model()
				{
					var url = '<?php echo $this->createUrl("profile/viewCycle"); ?>';
					url += '&id=' + html;
					window.location.href = url;
				}
				else
				{
					$('#pr').html(html);
					$('#loading').hide();
				}
			}
		});	
		return false;		
	});
	

});
</script>
<div class = "loading" id = "loading">
<p style="text-align:center;margin-top: 100px">ЗАГРУЗКА</p>
</div>
<div id = "pr">
</div>