<script>
$(document).ready(function(){
	$('.list_href').slick({
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		vertical: true,
		verticalSwiping: true,
		arrows: true, 
		prevArrow: '<a type="button" class="slick-prev slick-com">ВВЕРХ</a>',
		nextArrow: '<a type="button" class="slick-next slick-com">ВНИЗ</a>',
	});				
});
</script>

<div class="list_href">
<?php foreach( $model as $one ) : ?>
<?php if ($one->id == 1) continue; // Зацикленное выращивание ?>
<div class = "slide">
<?php echo CHtml::link('<div class="name">'.$one->name.'</div>', array('profile/view', 'id' => $one->id), array('class' => 'itm'));?>
</div>
<?php endforeach; ?>
</div>
<div class = "prev_bottom">
<?php echo CHtml::link(Profile::model()->findByPk(1)->name, array('profile/view', 'id' => 1));?>
</div>
<div class = "bottom">
<?php echo CHtml::link("СОЗДАТЬ НОВЫЙ ПРОФИЛЬ", array('profile/create'));?>
</div>