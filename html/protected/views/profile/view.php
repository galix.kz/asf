<?php
/* @var $this ProfileController */
/* @var $model Profile */

?>
<script>
$(document).ready(function(){
	$('#del_profile').click(function(){
		$('.botpopup2').addClass('active');
		return false;
	})
	$('#btn_otmena').click(function(){
		$('.botpopup2').removeClass('active');
		return false;
	})
});
</script>

		<div class="profile">
			<div class="steps-line">
				<div class="slider">
					<?php $i = 0; foreach($model->circles as $one) :?>
						<?php $i++; ?>
						<div class="slide"><?php echo CHtml::link($i, array('profile/viewCycle', 'id' => $one->id), array('class' => 'pre'));?></div>
					<?php endforeach; ?>
					<div class="slide"><?php echo CHtml::link("+", array('profile/addCycle', 'profile_id' => $model->id), array('class' => 'pre', 'style' => 'background:#92E96B;'));?></div>
				</div>
			</div>
			<div class="steps-step">
				<div class = "name"><?php echo $model->name; ?><br>
				<?php echo CHtml::link("Изменить название", array('profile/update', 'id' => $model->id), array('class' => 'btn change')); ?>
				</div>
				<div class = "descr">
				<div class = "row provile_view_row">Создание фазы роста  <?php echo CHtml::link("+", array('profile/addCycle', 'profile_id' => $model->id), array('class' => 'pre'));?></div>
				<div class = "row provile_view_row">Редактирование фазы  <div class = "pre" style = "background:#48B811;">#</div></div>
				</div>
				<div class = "prebtns">
					<a class = "btn btn-c" href = "#" id = "del_profile">УДАЛИТЬ ПРОФИЛЬ</a>
				</div>
				<div class="btns">
					<?php echo CHtml::link('НАЗАД К УПРАВЛЕНИЮ', array('profile/index'), array('class' => 'btn btn-c')); ?>
				</div>
				<div class = "botpopup2">
					<div class = "top">УДАЛЕНИЕ ПРОФИЛЯ</div>
					<div class = "middle">
						Профиль <?php echo $model->name;?><br/><br/>
						Вы уверены?
					</div>
					<br/>
					<div class = "pop_btns">
						<?php echo CHtml::link('УДАЛИТЬ', array('profile/delete1', 'id' => $model->id),array('class' => 'btn_del')); ?>
						<a href = '#' class ="btn_otmena" id = "btn_otmena">ОТМЕНА</a>
					</div>
					
				</div>
			</div>
		</div>
		
