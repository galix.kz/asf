<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

		<div class="profile">
			<div class="plabel">
				Введите пароль<br>
				<span>Подключение к <?php echo $name; ?></span>
			</div>

	<form>
	<input type = "hidden" name = "name" value = "<?php echo $name;?>" /> 
	<div class="row">
		<input type = "text" value = "" name = "pass" id = "to_paste" class = "inputbox" />
	</div>

	<div class="btns">
		<?php echo CHtml::link('Отмена',array('profile/index'), array('class' => 'btn btn-l')); ?>
		<?php echo CHtml::submitButton('Далее', array('class' => 'btn btn-r')); ?>
	</div>
	</form>
			<div class="botpopup keybo">
				<div class="lang">
					<a href="#" class="lng ru" id = "href_rus">RU</a>
					<a href="#" class="lng en active" id = "href_eng">ENG</a>
					<a href="#" class="lng num" id = "href_num">123</a>
				</div>
				<div class="keys select-values" id="word_eng" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						for($c = 'A'; $c <= 'Z'; $c++)
							echo '<div class="slide">'.$c.'</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="keys select-values active" id="word_rus" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						foreach (range(chr(0xC0),chr(0xDF)) as $v) 
							echo '<div class="slide">'.iconv('CP1251','UTF-8',$v).'</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="keys select-values" id="word_num" data-slick='{"initialSlide": 1}'>
					<div class="slide">&nbsp;</div>
					<?php 
						for($c = '0'; $c < '9'; $c++)
							echo '<div class="slide">'.$c.'</div>';
					?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="btns">
					<a href="#" class="btn btn-l" id = "button_steret">Стереть</a>
					<a href="#" class="btn btn-r" id = "button_vstavit">Вставить</a>
				</div>
			</div>

</div><!-- form -->