<?php
/* @var $this ProfileController */
/* @var $model Profile */

$this->breadcrumbs=array(
	'Профили'=>array('index'),
	'Создать',
);

$this->menu=array(
        array('label'=>'Управление профилями', 'url'=>array('admin')),
);
?>

<h1>Создать профиль</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>