<script>
$(document).ready(function(){
		
	$('#view_log').click(function(){
		var val_n = $(this).parents('.history').find('input[type=hidden]').val();
		if(val_n > 0)
		{
			var url = '<?php echo $this->createUrl("profile/viewHistory"); ?>';
			url += ('&id=' + val_n);
			window.location.href = url;
		}
		return false;
	});
	$('#btn_del').click(function(){
		$(this).parents('.history').find('.botpopup').toggleClass('active');
		return false;
	});
	$('.btn-d').click(function(){
		$(this).parents('.history').find('.botpopup').toggleClass('active');
		return false;
	});
	$('#del_fragment').click(function(){
		var val_n = $(this).parents('.history').find('input[type=hidden]').val();
		if(val_n > 0)
		{
			var url = '<?php echo $this->createUrl("profile/delHistory"); ?>';
			url += ('&id=' + val_n);
			window.location.href = url;
		}
		return false;
	});
	$('#excel_id').click(function(){
		var val_n = $(this).parents('.history').find('input[type=hidden]').val();
		if(val_n > 0)
		{
			var url = '<?php echo $this->createUrl("profile/excel"); ?>';
			url += ('&id=' + val_n);
			window.location.href = url;
		}
		return false;
	});
});
</script>
<div class="history">
			<div class="top">История содержит графики изменений параметров выращивания</div>
			<div class="list">
				<p>Выберите процесс выращивания:</p>
				<div class="select-values" id="ph1" data-slick='{"initialSlide": 0}'>
					<div class="slide"><input type = "hidden" name = "log_id" value = "<?php if(isset($history_list) && $history_list) echo $history_list[0]->id;?>" />&nbsp;</div>
					<?php foreach ($history_list as $one) : ?>
					<div class="slide" data-val = '<?php echo $one->id;?>'>
						<p class="name"><?php echo $one->name; ?></p>
						<p class="date"><?php echo date("m-d-Y H:i", $one->start_time)?> - <?php echo date("m-d-Y H:i", $one->stop_time);?></p>
					</div>
					<?php endforeach; ?>
					<div class="slide">&nbsp;</div>
				</div>
				<div class="botpopup del">
					<div class="top">
						Удаление фрагмента
					</div>
					<br>
					Вы уверены?
						<div class="del-btns">
							<a class="btn btn-t" id = "del_fragment" href="#">УДАЛИТЬ</a>
							<a class="btn btn-d" href="#">ОТМЕНА</a>
						</div>
				</div>					
			</div>
			<div class="btns">
				<?php echo CHtml::link('НАЗАД', array('profile/current'), array('class' => 'btn btn-l')); ?>
				<a href="profile_3.hmtl" id = "view_log" class="btn btn-r">СМОТРЕТЬ</a>
				<div class = "clear"></div>
				<a href="profile_1.html" class="btn btn-c" style = "color:red" id = "btn_del" >УДАЛИТЬ ФРАГМЕНТ</a>
				<a href="profile_1.html" class="btn btn-c" id = "excel_id" <?php if($this->layout == "columnLocalhost") echo "style='display:none;'";?> >ВЫГРУЗИТЬ EXCEL</a>
			</div>
						
</div>