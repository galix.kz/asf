<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/style_ajax.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('#button_prev').click(function(){
		var cur = $('.slider_main_step_cycle').slick('slickCurrentSlide');
		if(cur > 0)
		{
			$('.slider_main_step_cycle').slick('slickPrev');		
			return false;
		}
	})
	$('#button_next').click(function(){
		var cur = $('.slider_main_step_cycle').slick('slickCurrentSlide');
		if(cur < 9)
		{
			$('.slider_main_step_cycle').slick('slickNext');		
			return false;
		}
	})
	$('.errorSlick').click(function(){
		$('.slider_main_step_cycle').slick('slickGoTo', $(this).attr('data-slick'));
		return false;
	});

<?php if ($model->hasErrors() ) : ?>	
		$('.slider_main_step_cycle').slick('slickGoTo', 10);
<?php endif; ?>	
});
</script>
<?php $form=$this->beginWidget('ModernForm', array(
	'id'=>'profile-form-cycle',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<div class="profile">
			<div class="steps-line">
				<div class="slider">
					<div class="slide"><span data-span-val = "0" class="active">1</span></div>
					<div class="slide"><span data-span-val = "1">2</span></div>
					<div class="slide"><span data-span-val = "2">3</span></div>
					<div class="slide"><span data-span-val = "3">4</span></div>
					<div class="slide"><span data-span-val = "4">5</span></div>
					<div class="slide"><span data-span-val = "5">6</span></div>
					<div class="slide"><span data-span-val = "6">7</span></div>
					<div class="slide"><span data-span-val = "7">8</span></div>
					<div class="slide"><span data-span-val = "8">9</span></div>
					<div class="slide"><span data-span-val = "9">10</span></div>
					<?php if($model->hasErrors()) : ?>
						<div class="slide"><span data-span-val = "10">11</span></div>
					<?php endif; ?>
				</div>
			</div>
			<div class = "slider_main_step_cycle" id = "slider_main_step_cycle">
				<div class="steps-step">
					<p>Выберите продолжительность фазы роста №1,</p>
					<div class="col1-1">
						<p class="sz2">дней</p>
					</div>
					<div class="col1-1">
						<?php echo $form->sliderChoose($model, 'day_count', 1, 1, 100); ?>
						
					</div>
					
				</div>
				
				<div class="steps-step">
					<h3>Установки pH</h3>
					<div class="row heads">
						<div class="col3">
							<p class="sz1">pH</p>
							<p class="sz3">min</p>
						</div>
						<div class="col3">
							<p class="sz1">pH</p>
							<p class="sz3">max</p>
						</div>
						<div class="col3">
							<p class="sz1">ml</p>
						</div>
					</div>
					<div class="row cont">
						<div class="col3">
							<?php if(!$model->ph_min) $model->ph_min = 5.5; echo $form->sliderChoose($model, 'ph_min', 3, 0.10, 60); ?>
						</div>
						<div class="col3">
							<?php if(!$model->ph_max) $model->ph_max = 6.5; echo $form->sliderChoose($model, 'ph_max', 3, 0.10, 60); ?>
						</div>
						<div class="col3">
<?php 
	$ph_ml = array();
	$i = 0;
	for($k = 1; $k < 20; $k++)
		$ph_ml[$i++] = $k;
	for($k = 20; $k < 40; $k += 2)
		$ph_ml[$i++] = $k;
	for($k = 40; $k < 100; $k += 5)
		$ph_ml[$i++] = $k;
	for($k = 100; $k < 250; $k += 10)
		$ph_ml[$i++] = $k;
	for($k = 250; $k < 1000; $k += 50)
		$ph_ml[$i++] = $k;
?>							
							<?php echo $form->sliderChoose2($model, 'ph_ml', $ph_ml); ?>
						</div>
					</div>
				</div>				
<div class="steps-step">
			<h3>Компоненты удобрений</h3>
			<div class="row heads">
				<div class="col3">
					<p class="sz1">Soil A</p>
				</div>
				<div class="col3">
					<p class="sz1">Soil B</p>
					<p class="sz3">ml/10 L</p>
				</div>
				<div class="col3">
					<p class="sz1">Soil C</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col3">
							<?php echo $form->sliderChoose($model, 'soil_a', 0, 1, 100); ?>
				</div>
				<div class="col3">
							<?php echo $form->sliderChoose($model, 'soil_b', 0, 1, 100); ?>
				</div>
				<div class="col3">
							<?php echo $form->sliderChoose($model, 'soil_c', 0, 1, 100); ?>
				</div>
			</div>
		</div>				
	<div class="steps-step">
			<h3>Установки TDS</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Уровень TDS</p>
				</div>
				<div class="col2">
					<p class="sz3">min</p>
				</div>
				<div class="col2">
					<p class="sz3">max</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'tds_min', 0, 100, 40); ?>
				</div>
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'tds_max', 0, 100, 40); ?>
				</div>
			</div>
			
			<div class="row check">
				<p style = "font-size:25px;">Постепенное изменение</p>
				<label class="switch">
					<?php echo $form->checkBox($model, 'tds_gradual');?>
					<div class="toggle"></div>
				</label>
			</div>
			
		</div>
		<div class="steps-step">
			<h3>Установки Бака с раствором</h3>
			<div class="row heads">
				<div>
					<p class="sz2">Температура</p>
				</div>
			</div>
			<div class="row heads">
				<div class="col2">
					<p class="sz3">min</p>
				</div>
				<div class="col2">
					<p class="sz3">max</p>
				</div>
			</div>
			<div class="row cont big-btm">
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'tank_t_min', 5, 1, 35); ?>
				</div>
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'tank_t_max', 5, 1, 35); ?>
				</div>
			</div>
			<!--
			<div class="row info">
				<p>Периодичность замены воды</p>
				<p><span>Каждые 7 дней</span></p>
			</div>
			-->
		</div>		
		<div class="steps-step">
			<h3>Установки освещения</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Режим дня</p>
				</div>
				<div class="col2">
					<p class="sz3">Восход</p>
				</div>
				<div class="col2">
					<p class="sz3">Закат</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col2">
							<?php echo $form->sliderChooseTime($model, 'light_from', 0, 144); ?>
				
				</div>
				<div class="col2">
							<?php echo $form->sliderChooseTime($model, 'light_to', 0, 144); ?>
				</div>
			</div>
			<div class="row check">
				<p>Постепенное изменение</p>
				<label class="switch">
					<?php echo $form->checkBox($model, 'light_gradual');?>
					<div class="toggle"></div>
				</label>
			</div>
		</div>	
		<div class="steps-step">
			<h3>Установки Микроклимата</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Влажность</p>
				</div>
				<div class="col2">
					<p class="sz3">min</p>
				</div>
				<div class="col2">
					<p class="sz3">max</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'hum_min', 20, 5, 16); ?>
				</div>
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'hum_max', 20, 5, 16); ?>
				</div>
			</div>
		</div>		
		<div class="steps-step">
			<h3>Установки Микроклимата</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Температура</p>
				</div>
				<div class="col2">
					<p class="sz3">день</p>
				</div>
				<div class="col2">
					<p class="sz3">ночь</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col2">
							<?php if(!$model->microclimate_t_day) $model->microclimate_t_day = 20; echo $form->sliderChoose($model, 'microclimate_t_day', 5, 1, 46); ?>
				</div>
				<div class="col2">
							<?php if(!$model->microclimate_t_night) $model->microclimate_t_night = 18; echo $form->sliderChoose($model, 'microclimate_t_night', 5, 1, 46); ?>
				</div>
			</div>
			<div class="row check">
				Гистерезис ±1 °C
			</div>
		</div>		
		
		<div class="steps-step">
			<h3>Установки CO2</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Уровень CO2</p>
				</div>
				<div class="col2">
					<p class="sz3">min</p>
				</div>
				<div class="col2">
					<p class="sz3">max</p>
				</div>
			</div>
			<div class="row cont">
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'co2_min', 100, 100, 30); ?>
				</div>
				<div class="col2">
							<?php echo $form->sliderChoose($model, 'co2_max', 100, 100, 40); ?>
				</div>
			</div>
			<div class="row check">
				points per million	
			</div>
		</div>	

		<div class="steps-step">
			<h3>Установки CO2</h3>
			<div class="row heads">
				<div class="col1">
					<p class="sz2">Расписание подачи</p>
				</div>
			</div>
			<div class="row cont">
				<div class="shedule">
					<div class="itm <?php if (!$model->co2_time1_from || !$model->co2_time1_to) echo 'empty'; ?>" id = "co2_itm_1" >
						<div class="values" data-pop-id = '#popup_cycle_co2_1'>
							<div class="num">1</div>
							<?php if($model->co2_time1_from && $model->co2_time1_to) : ?>
							<div class="time"><?php echo $model->co2_time1_from." - ".$model->co2_time1_to; ?></div>
							<?php else : ?>
							<div class="time">____ - ____</div>
							<?php endif; ?>
						</div>
						
					</div>
					<div class="itm <?php if (!$model->co2_time2_from || !$model->co2_time2_to) echo 'empty'; ?>" id = "co2_itm_2">
						<div class="values" data-pop-id = '#popup_cycle_co2_2'>
							<div class="num">2</div>
							<?php if($model->co2_time2_from && $model->co2_time2_to) : ?>
							<div class="time"><?php echo $model->co2_time2_from." - ".$model->co2_time2_to; ?></div>
							<?php else : ?>
							<div class="time">____ - ____</div>
							<?php endif; ?>
						</div>
						

					</div>
					<div class="itm <?php if (!$model->co2_time3_from || !$model->co2_time3_to) echo 'empty'; ?>" id = "co2_itm_3">
						<div class="values" data-pop-id = '#popup_cycle_co2_3'>
							<div class="num">3</div>
							<?php if($model->co2_time3_from && $model->co2_time3_to) : ?>
							<div class="time"><?php echo $model->co2_time3_from." - ".$model->co2_time3_to; ?></div>
							<?php else : ?>
							<div class="time">____ - ____</div>
							<?php endif; ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<?php if($model->hasErrors()) : ?>
		<div class="steps-step">
			<h3>Ошибки</h3>
			<?php echo $form->errorSummary($model); ?>
			
		</div>
		<?php endif; ?>
		
		
			</div>
						<div class="botpopup cyclePop" id = "popup_cycle_co2_1">
							<div class="row heads">
								<div class="col1">
									<p class="sz2">1. Время подачи CO2</p>
								</div>
							</div>
							<div class="row cont">
								<div class="col2"> 
									
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time1_from', 0, 144); ?>
								</div>
								<div class="col2">
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time1_to', 0, 144); ?>
								</div>
							</div>
							<div class="row btms last">
								<a class="btn btn-l co2_delete" href="#">УДАЛИТЬ</a>
								<a class="btn btn-r co2_gotovo" href="#" data-co2 = '#co2_itm_1' >ГОТОВО</a>
							</div>
						</div>	
						<div class="botpopup cyclePop" id = "popup_cycle_co2_2">
							<div class="row heads">
								<div class="col1">
									<p class="sz2">2. Время подачи CO2</p>
								</div>
							</div>
							<div class="row cont">
								<div class="col2"> 
									
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time2_from', 0, 144); ?>
								</div>
								<div class="col2">
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time2_to', 0 ,144); ?>
								</div>
							</div>
							<div class="row btms last">
								<a class="btn btn-l co2_delete" href="#">УДАЛИТЬ</a>
								<a class="btn btn-r co2_gotovo" href="#" data-co2 = '#co2_itm_2' >ГОТОВО</a>
							</div>
						</div>						
						<div class="botpopup cyclePop" id = "popup_cycle_co2_3">
							<div class="row heads">
								<div class="col1">
									<p class="sz2">3. Время подачи CO2</p>
								</div>
							</div>
							<div class="row cont">
								<div class="col2"> 
									
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time3_from', 0, 144); ?>
								</div>
								<div class="col2">
								<?php echo $form->sliderChooseTimeCO2($model, 'co2_time3_to', 0, 144); ?>
								</div>
							</div>
							<div class="row btms last">
								<a class="btn btn-l co2_delete" href="#">УДАЛИТЬ</a>
								<a class="btn btn-r co2_gotovo" href="#" data-co2 = '#co2_itm_3' >ГОТОВО</a>
							</div>
						</div>
</div>
					<div class="btns" id = "buttons_cycle">
						<?php 
								echo CHtml::link('НАЗАД', ($model->id) ? array('profile/viewCycle', 'id' => $model->id) : array('profile/view', 'id' => $profile_id), array('class' => 'btn btn-l', 'id' => 'button_prev')); 
						?>
						<?php echo CHtml::submitButton('ДАЛЕЕ', array('class' => 'btn btn-r', 'id' => 'button_next')); ?>
						
					</div>
<?php $this->endWidget(); ?>
