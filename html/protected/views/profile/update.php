<?php
/* @var $this ProfileController */
/* @var $model Profile */

$this->breadcrumbs=array(
	'Профили'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
    
	array('label'=>'Создать профиль', 'url'=>array('create')),
        array('label'=>'Управление профилями', 'url'=>array('admin')),
	array('label'=>'Посмотреть профиль', 'url'=>array('view', 'id'=>$model->id)),
        array('label'=>'Создать цикл', 'url'=>array('circle/create')),
        array('label'=>'Управление циклами', 'url'=>array('circle/admin')),
    
);
?>

<h1>Редактировать профиль [<?php echo $model->name; ?>]</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>