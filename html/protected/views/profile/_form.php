<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

		<div class="profile">
			<div class="plabel">
				Введите имя профиля<br>
				<span>(не более 18 символов)</span>
			</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->textField($model,'name',array('class' => 'inputbox', 'id' => 'to_paste')); ?>
	</div>

	<div class="btns">
		<?php echo CHtml::link('Отмена',array('profile/index'), array('class' => 'btn btn-l')); ?>
		<?php echo CHtml::submitButton('Далее', array('class' => 'btn btn-r')); ?>
	</div>

<?php $this->endWidget(); ?>
			<?php $this->renderPartial('_pop_keybo'); ?>

</div><!-- form -->