<?php
/* @var $this CircleController */
/* @var $model Circle */

$this->breadcrumbs=array(
	'Циклы'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Управление циклами', 'url'=>array('admin')),
);
?>

<h1>Создание цикла</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>