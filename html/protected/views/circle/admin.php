<?php
/* @var $this CircleController */
/* @var $model Circle */

$this->breadcrumbs=array(
	'Циклы'=>array('index'),
	'Управление',
);

$this->menu=array(
	//array('label'=>'Список циклов', 'url'=>array('index')),
	array('label'=>'Создать цикл', 'url'=>array('create')),
        array('label'=>'Профили', 'url'=>array('profile/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#circle-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление циклами</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'circle-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'profile_id',
		'name',
		'day',
		'hour_light',
		'hour_dark',
		/*
		'temp',
		'wtemp',
		'ph1',
		'ph2',
		'ph_delay',
		'tds',
		'tdsMin',
		'tdsMax',
		'tdsA',
		'tdsB',
		'tdsC',
		'humMin',
		'reg_delay',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
