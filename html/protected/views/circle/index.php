<?php
/* @var $this CircleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Circles',
);

$this->menu=array(
    array('label'=>'Создать цикл', 'url'=>array('create')),
    array('label'=>'Управление циклами', 'url'=>array('admin')),
    array('label'=>'Управление профилями', 'url'=>array('profile/admin')),

);
?>

<h1>Циклы</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
