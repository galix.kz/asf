<?php
/* @var $this CircleController */
/* @var $model Circle */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'circle-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля помеченные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'profile_id'); ?>
		<?php //echo $form->textField($model,'profile_id'); ?>
                <?php
                $profs = Profile::model()->findAll(array('order' => 'id'));
                $data = CHtml::listData($profs, 'id', 'name');
                //echo $form->dropDownList($model, 'profile_id', $data, array('empty'=>'ВЫБЕРИТЕ ПРОФИЛЬ'));
                echo CHtml::dropDownList('Circle[profile_id]', $profs, $data, 
                        array(
                            'empty' => 'ВЫБЕРИТЕ ПРОФИЛЬ',
                            'options'=>array($model->profile_id => array('selected'=>true))
                            )
                );
                ?>
		<?php echo $form->error($model,'profile_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'day'); ?>
		<?php echo $form->textField($model,'day'); ?>
		<?php echo $form->error($model,'day'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hour_light'); ?>
		<?php echo $form->textField($model,'hour_light'); ?>
		<?php echo $form->error($model,'hour_light'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hour_dark'); ?>
		<?php echo $form->textField($model,'hour_dark'); ?>
		<?php echo $form->error($model,'hour_dark'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp'); ?>
		<?php echo $form->textField($model,'temp',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'temp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wtemp'); ?>
		<?php echo $form->textField($model,'wtemp',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'wtemp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph1'); ?>
		<?php echo $form->textField($model,'ph1',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'ph1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph2'); ?>
		<?php echo $form->textField($model,'ph2',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'ph2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ph_delay'); ?>
		<?php echo $form->textField($model,'ph_delay',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'ph_delay'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tds'); ?>
		<?php echo $form->textField($model,'tds',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tds'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tdsMin'); ?>
		<?php echo $form->textField($model,'tdsMin',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tdsMin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tdsMax'); ?>
		<?php echo $form->textField($model,'tdsMax',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tdsMax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tdsA'); ?>
		<?php echo $form->textField($model,'tdsA',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tdsA'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tdsB'); ?>
		<?php echo $form->textField($model,'tdsB',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tdsB'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tdsC'); ?>
		<?php echo $form->textField($model,'tdsC',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tdsC'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'humMin'); ?>
		<?php echo $form->textField($model,'humMin',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'humMin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reg_delay'); ?>
		<?php echo $form->textField($model,'reg_delay',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'reg_delay'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->