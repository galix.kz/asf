<?php
/* @var $this CircleController */
/* @var $model Circle */

$this->breadcrumbs=array(
	'Циклы'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Создать цикл', 'url'=>array('create')),
	array('label'=>'Посмотреть цикл', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление циклами', 'url'=>array('admin')),
);
?>

<h1>Редактирование цикла [<?php echo $model->name; ?>]</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>