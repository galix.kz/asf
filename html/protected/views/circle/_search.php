<?php
/* @var $this CircleController */
/* @var $model Circle */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_id'); ?>
		<?php echo $form->textField($model,'profile_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'day'); ?>
		<?php echo $form->textField($model,'day'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hour_light'); ?>
		<?php echo $form->textField($model,'hour_light'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hour_dark'); ?>
		<?php echo $form->textField($model,'hour_dark'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp'); ?>
		<?php echo $form->textField($model,'temp',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wtemp'); ?>
		<?php echo $form->textField($model,'wtemp',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph1'); ?>
		<?php echo $form->textField($model,'ph1',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph2'); ?>
		<?php echo $form->textField($model,'ph2',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ph_delay'); ?>
		<?php echo $form->textField($model,'ph_delay',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tdsMin'); ?>
		<?php echo $form->textField($model,'tdsMin',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tdsMax'); ?>
		<?php echo $form->textField($model,'tdsMax',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tdsA'); ?>
		<?php echo $form->textField($model,'tdsA',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tdsB'); ?>
		<?php echo $form->textField($model,'tdsB',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tdsC'); ?>
		<?php echo $form->textField($model,'tdsC',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'humMin'); ?>
		<?php echo $form->textField($model,'humMin',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reg_delay'); ?>
		<?php echo $form->textField($model,'reg_delay',array('size'=>6,'maxlength'=>6)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->