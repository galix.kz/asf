<?php
/* @var $this CircleController */
/* @var $model Circle */

$this->breadcrumbs=array(
	'Циклы'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Создать цикл', 'url'=>array('create')),
	array('label'=>'Редактировать цикл', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить цикл', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление циклами', 'url'=>array('admin')),
);
?>

<h1>Просмотр цикла [<?php echo $model->name; ?>]</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'name',
		'day',
		'hour_light',
		'hour_dark',
		'temp',
		'wtemp',
		'ph1',
		'ph2',
		'ph_delay',
		'tds',
		'tdsMin',
		'tdsMax',
		'tdsA',
		'tdsB',
		'tdsC',
		'humMin',
		'reg_delay',
	),
)); ?>
