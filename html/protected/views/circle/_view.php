<?php
/* @var $this CircleController */
/* @var $data Circle */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_id')); ?>:</b>
	<?php echo CHtml::encode($data->profile_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day')); ?>:</b>
	<?php echo CHtml::encode($data->day); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hour_light')); ?>:</b>
	<?php echo CHtml::encode($data->hour_light); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hour_dark')); ?>:</b>
	<?php echo CHtml::encode($data->hour_dark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp')); ?>:</b>
	<?php echo CHtml::encode($data->temp); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('wtemp')); ?>:</b>
	<?php echo CHtml::encode($data->wtemp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph1')); ?>:</b>
	<?php echo CHtml::encode($data->ph1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph2')); ?>:</b>
	<?php echo CHtml::encode($data->ph2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ph_delay')); ?>:</b>
	<?php echo CHtml::encode($data->ph_delay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tds')); ?>:</b>
	<?php echo CHtml::encode($data->tds); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tdsMin')); ?>:</b>
	<?php echo CHtml::encode($data->tdsMin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tdsMax')); ?>:</b>
	<?php echo CHtml::encode($data->tdsMax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tdsA')); ?>:</b>
	<?php echo CHtml::encode($data->tdsA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tdsB')); ?>:</b>
	<?php echo CHtml::encode($data->tdsB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tdsC')); ?>:</b>
	<?php echo CHtml::encode($data->tdsC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('humMin')); ?>:</b>
	<?php echo CHtml::encode($data->humMin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_delay')); ?>:</b>
	<?php echo CHtml::encode($data->reg_delay); ?>
	<br />

	*/ ?>

</div>