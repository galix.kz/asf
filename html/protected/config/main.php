<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('chartjs', dirname(__FILE__).'/../extensions/yii-chartjs');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Росток Гидропоника, автоматизация выращивания растений. Просто добавь воды',
    	'sourceLanguage' => 'en',
	'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log', 'chartjs'),

    // autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
            	'application.extensions.giix.components.*', // giix components
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		*/
            'gii' => array(
		'class' => 'system.gii.GiiModule',
		'password'=>'qwerty',
		'ipFilters'=>array('127.0.0.1','212.15.38.168','217.71.139.2','217.71.139.236','192.168.1.125', '109.174.127.107','::1', '172.16.29.3', '5.128.82.4', '*'),
		//'generatorPaths' => array(
		//	'application.extensions.giix.generators', // giix generators
			//'application.extensions.giix-core', // giix generators
		//),
            ),
	),

	// application components
	'components'=>array(
//                'chartjs' => array('class' => 'chartjs.components.ChartJs'),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/


		// database settings are configured in database.php
		//'db'=>require(dirname(__FILE__).'/database.php'),
                'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=alfa2',
			'emulatePrepare' => true,
			'username' => 'root',
//			'password' => 'yfigfhjkm',
			'password' => '10RfybyYbufc',
			'charset' => 'utf8',
		//        'enableProfiling'=>true,
		//        'enableParamLogging' => true,
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'me@stansage.com',
                'limits'=>array(
                    'tds'=>array(0,3000),
                    'hum'=>array(20,70),
                    'temp'=>array(0,40),
                    'wtemp'=>array(0,35),
                    'ph'=>array(0,10),
                    'light'=>array(0,50000),
		    'co2'=>array(0,10000),
		    'wlevel'=>array(0,5)
                ),
                'defaults'=>array(
                    'configfile' => 'alfa.php',
					'configSMTP' => 'smtp.php',
                    'ramdpath' => '/home/pi/ramdisk/',
                  /*  'ph1' => 5.5,
                    'ph2' =>6.5,
                    'ph_delay' =>5,
                    'tds' =>1260,
                    'tdsa' =>1,
                    'tdsb' => 1,
                    'tdsc' =>1,
                    'reg_delay' =>5*/
                ),
	),
);
