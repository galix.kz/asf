<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	'import'=>array(
        'application.components.*',
        'application.models.*',
    ),	// application components
	'components'=>array(
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=alfa2',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '10RfybyYbufc',
			'charset' => 'utf8',
		),
		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
		'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron.log',
                    'levels'=>'error, warning',
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'logFile'=>'cron_trace.log',
                    'levels'=>'trace',
                ),
            ),
        ),
	),
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'me@stansage.com',
                'limits'=>array(
                    'tds'=>array(0,3000),
                    'hum'=>array(20,70),
                    'temp'=>array(0,40),
                    'wtemp'=>array(0,35),
                    'ph'=>array(0,10),
                    'light'=>array(0,50000),
		    'co2'=>array(0,10000),
		    'wlevel'=>array(0,5)
                ),
                'defaults'=>array(
                    'configfile' => 'alfa.php',
					'configSMTP' => 'smtp.php',
                    'ramdpath' => '/home/pi/ramdisk/',
                  /*  'ph1' => 5.5,
                    'ph2' =>6.5,
                    'ph_delay' =>5,
                    'tds' =>1260,
                    'tdsa' =>1,
                    'tdsb' => 1,
                    'tdsc' =>1,
                    'reg_delay' =>5*/
                ),
	),
	
);