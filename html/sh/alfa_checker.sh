#!/bin/bash
proc=$1
period=$2
logfile="/var/log/alfa_checker.log"
nomer=$(pidof -x $proc)
if [[ -z $nomer ]]
then
	##echo "$(date) Start процесса $proc" >> $logfile
	sleep 1
	$proc &
	sleep $period
	nomer=$(pidof -x $proc)
	##echo "$(date) Процесс $proc id=$nomer запущен и работает" >> $logfile
else
#    echo $nomer
     ##echo "$(date) Процесс $proc id=$nomer запущен и работает" >> $logfile
	sleep $period
fi

while :
do
if [ "$(pidof -x $proc)" = "$nomer" ]; then
	##echo "$(date) Процесс $proc id=$nomer запущен и работает!" >> $logfile
	sleep $period
else 
	##echo "$(date) Start процесса $proc" >> $logfile
	sleep 1
	$proc &
	sleep $period
	tmpid=
	tmpid=$(pidof -x $proc)
	if [ -n "$tmpid" ]
	then
	    nomer=$(pidof -x $proc)
	    ##echo "$(date) Процесс $proc id=$nomer перезапущен и работает!?" >> $logfile
	fi
	continue
fi
done
