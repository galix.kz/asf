#! /bin/sh
### BEGIN INIT INFO
# Provides:          alfacheck
# Required-Start: 
# Required-Stop:
# Should-Start:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: stark alfa_checker.sh
### END INIT INFO

# Include /usr/bin in path to find on_ac_power if /usr/ is on the root
# partition.
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
logfile="/var/log/alfa_check.log"
DAEMON_NAME="alfacheck.sh"
. /lib/lsb/init-functions

case "$1" in
 start)
	log_daemon_msg "START $DAEMON_NAME"
	/home/pi/Documents/alfa/alfa_checker.sh /home/pi/Documents/alfa/alfa.sh 20 &
	##echo "$(date) Start alfacheck.sh service ok!" >> $logfile
	log_end_msg 0
	;;
 stop)
    log_daemon_msg "STOP $DAEMON_NAME"
    alfacheckerid=$(pidof -x alfa_checker.sh)
    #sleep 1
    if [ -n "$alfacheckerid" ]
    then
	sudo kill $alfacheckerid
	##echo "Kill alfa_checker.sh id=$alfacheckerid" >> $logfile
    fi
    alfaid=$(pidof -x alfa.sh)
    #sleep 1
    if [ -n "$alfaid" ]
    then
	sudo kill $alfaid
	##echo "Kill alfa.sh id=$alfaid" >> $logfile
    fi
    ##echo "$(date) Stop alfacheck.sh service ok!" >> $logfile
    log_end_msg 0
	;;
 *)
    echo "Usage: /etc/init.d/$DAEMON_NAME {start|stop}"
    exit 1  
	;;
esac

exit 0
