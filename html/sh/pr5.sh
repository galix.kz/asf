#/bin/bash
#IN="Tds=0.00; Ph=3.12; Humidity=34.00; Temperature=26.00; WaterTemp=23.50;"
#IN="RegulatedPh=5.29"
#mypass="yfigfhjkm"
mypass="10RfybyYbufc"
sqlsave="mysql -u root -p$mypass -h localhost alfa2 -Bse"
sql="mysql -v -u root -p$mypass -h localhost alfa2"
rampath="/home/pi/ramdisk/"
sql_log="/home/pi/ramdisk/query.sql"
all=$rampath"all"
#sys_log="/var/log/alfasys.log"
#regpath=$rampath:"/reg"
declare -A ARRAY
parse_str () {
    str="$1"
    IFS='; ' read -ra ARR <<< "$str"
    for j in "${ARR[@]}"; do
	while IFS== read arg value
        do
	    ARRAY["$arg"]="$value"
#	    echo "Parse:value="$value" arg="$arg
        done <<< $(echo "$j")
    done
}

function float_cond()
{
    local cond=0
    if [[ $# -gt 0 ]]; then
        cond=$(echo "$*" | bc -q 2>/dev/null)
        if [[ -z "$cond" ]]; then cond=0; fi
        if [[ "$cond" != 0  &&  "$cond" != 1 ]]; then cond=0; fi
    fi
    local stat=$((cond == 0))
    return $stat
}

i=0
stty -F /dev/ttyUSB0 -echo
while read line
do
    ts=$( date +%Y-%m-%d:%H:%M:%S )
    tsday=$( date +%Y-%m-%d )
    ##echo $ts$line >> /var/log/alfa2.log
    ##echo $line >> /var/log/alfa_buf.log
    echo $ts$line >> $rampath"/"$tsday.log
    echo $ts$line >> "/home/pi/sql/"$tsday.log

    parse_str "$line"
    if [ $i != "0" ]
    then
	if [ "$line" != "" ]
	then
            ts=$( date +%s )
            tmin=$( date +%M )
            td=$( date +%d )
            tm=$( date +%m )
            ty=$( date +%Y )
            th=$( date +%H )
            
            ts_min=$(date -d"$ty/$tm/$td $th:$tmin" "+%s")
            ts_h=$(date -d"$ty/$tm/$td $th:00" "+%s")
            ts_d=$(date -d"$ty/$tm/$td 00:00" "+%s")
		if [ "${ARRAY[RegulatedPH]}" != "" ]
		then
			if [ "${ARRAY[PhDownOn]}" != "" ]
			then
			echo ${ARRAY[RegulatedPH]} > $rampath"/RegulatedPH"
			echo ${ARRAY[PhDownOn]} > $rampath"/PhDownOn"
			#echo "INSERT INTO regulated (date, regulatedph, PhDownOn) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[PhDownOn]} );" | $sql;
			echo "INSERT INTO regulated (date, regulatedph, PhDownOn) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[PhDownOn]} );" >> $sql_log;
			ARRAY[PhDownOn]=""
			fi
			if [ "${ARRAY[PhUpOn]}" != "" ]
			then
			echo ${ARRAY[RegulatedPH]} > $rampath"/RegulatedPH"
			echo ${ARRAY[PhUpOn]} > $rampath"/PhUpOn"
			#echo "INSERT INTO regulated (date, regulatedph, PhUpOn) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[PhUpOn]} );" | $sql;
			echo "INSERT INTO regulated (date, regulatedph, PhUpOn) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[PhUpOn]} );" >> $sql_log;
			ARRAY[PhUpOn]=""
			fi
			if [ "${ARRAY[RegulatedTds]}" !="" ]
			then
			regDate=$( date +%Y-%m-%d:%H:%M:%S )
			echo ${ARRAY[RegulatedPH]} > $rampath"/RegulatedPH"
			echo ${ARRAY[RegulatedTds]} > $rampath"/RegulatedTds"
			echo $regDate > $rampath"/RegulatedPHDate"
			echo $regDate > $rampath"/RegulatedTdsDate"
			#echo "INSERT INTO regulated (date, regulatedph, regulatedtds) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[RegulatedTds]} );" | $sql;
			echo "INSERT INTO regulated (date, regulatedph, regulatedtds) VALUES ($ts, ${ARRAY[RegulatedPH]}, ${ARRAY[RegulatedTds]} );" >> $sql_log
			ARRAY[RegulatedTds]=""
			fi
		    ARRAY[RegulatedPH]=""
		elif [ "${ARRAY[RegulatedTds]}" != "" ]
		then
			echo ${ARRAY[RegulatedTds]} > $rampath"/RegulatedTds"
			regDate=$( date +%Y-%m-%d:%H:%M:%S )
			echo $regDate > $rampath"/RegulatedTdsDate"
			#echo "INSERT INTO regulated (date, regulatedtds) VALUES ($ts, ${ARRAY[RegulatedTds]});" | $sql;
			echo "INSERT INTO regulated (date, regulatedtds) VALUES ($ts, ${ARRAY[RegulatedTds]});" >> $sql_log;
			ARRAY[RegulatedTds]=""
		elif [ "${ARRAY[TdsAOn]}" != "" ]
		then
			echo ${ARRAY[TdsAOn]} > $rampath"/TdsAOn"
			#echo "INSERT INTO regulated (date, TdsAOn) VALUES ($ts, ${ARRAY[TdsAOn]});" | $sql;
			echo "INSERT INTO regulated (date, TdsAOn) VALUES ($ts, ${ARRAY[TdsAOn]});" >> $sql_log;
			ARRAY[TdsAOn]=""
		elif [ "${ARRAY[TdsBOn]}" != "" ]
		then
			echo ${ARRAY[TdsBOn]} > $rampath"/TdsBOn"
			#echo "INSERT INTO regulated (date, TdsBOn) VALUES ($ts, ${ARRAY[TdsBOn]});" | $sql;
			echo "INSERT INTO regulated (date, TdsBOn) VALUES ($ts, ${ARRAY[TdsBOn]});" >> $sql_log;
			ARRAY[TdsBOn]=""
		elif [ "${ARRAY[TdsCOn]}" != "" ]
		then
			echo ${ARRAY[TdsCOn]} > $rampath"/TdsCOn"
			#echo "INSERT INTO regulated (date, TdsCOn) VALUES ($ts, ${ARRAY[TdsCOn]});" | $sql;
			echo "INSERT INTO regulated (date, TdsCOn) VALUES ($ts, ${ARRAY[TdsCOn]});" >> $sql_log;
			ARRAY[TdsCOn]=""
		elif [ "${ARRAY[ph1]}" != "" ]
		then
			echo ${ARRAY[ph1]} > $rampath"/ph1"
			echo ${ARRAY[ph2]} > $rampath"/ph2"
			echo ${ARRAY[ph_delay]} > $rampath"/ph_delay"
			echo ${ARRAY[tds]} > $rampath"/tds"
			#echo ${ARRAY[TDS]} > $rampath"/tds"
			echo ${ARRAY[tdsA]} > $rampath"/tdsA"
			echo ${ARRAY[tdsB]} > $rampath"/tdsB"
			echo ${ARRAY[tdsC]} > $rampath"/tdsC"
			echo ${ARRAY[Humidity]} > $rampath"/hum"

			#echo "INSERT INTO regulated (date, ph1, ph2, ph_delay, tds, tdsA, tdsB, tdsC, hum) VALUES ($ts, ${ARRAY[ph1]},${ARRAY[ph2]},${ARRAY[ph_delay]},${ARRAY[tds]},${ARRAY[tdsA]},${ARRAY[tdsB]},${ARRAY[tdsC]},${ARRAY[Humidity]});" | $sql;
			echo "INSERT INTO regulated (date, ph1, ph2, ph_delay, tds, tdsA, tdsB, tdsC, hum) VALUES ($ts, ${ARRAY[ph1]},${ARRAY[ph2]},${ARRAY[ph_delay]},${ARRAY[tds]},${ARRAY[tdsA]},${ARRAY[tdsB]},${ARRAY[tdsC]},${ARRAY[Humidity]});" >> $sql_log;
			ARRAY[ph1]=""
		elif [ "${ARRAY[reg_delay]}" != "" ]
		then
			echo ${ARRAY[reg_delay]} > $rampath"/reg_delay"
			#echo "INSERT INTO regulated (date, reg_delay) VALUES ($ts, ${ARRAY[reg_delay]});" | $sql;
			echo "INSERT INTO regulated (date, reg_delay) VALUES ($ts, ${ARRAY[reg_delay]});" >> $sql_log;
			ARRAY[reg_delay]=""
		else
		    ts_norm=$( date "+%Y-%m-%d %H:%M:%S" )
		    #echo "INSERT INTO data (date,date_norm,tds,ph,hum,temp,wtemp) VALUES ($ts, '$ts_norm', ${ARRAY[TDS]}, ${ARRAY[Ph]}, ${ARRAY[Humidity]}, ${ARRAY[Temperature]}, ${ARRAY[WaterTemp]});" | $sql;
		    echo ${ARRAY[TDS]} > $rampath"/TDS"
		    echo ${ARRAY[Ph]} > $rampath"/ph"
		    echo ${ARRAY[Humidity]} > $rampath"/hum"
		    echo ${ARRAY[Temperature]} > $rampath"/temp"
		    echo ${ARRAY[WaterTemp]} > $rampath"/wtemp"

		    echo "INSERT INTO data (date,date_norm,tds,ph,hum,temp,wtemp) VALUES ($ts, '$ts_norm', ${ARRAY[TDS]}, ${ARRAY[Ph]}, ${ARRAY[Humidity]}, ${ARRAY[Temperature]}, ${ARRAY[WaterTemp]});" >> $sql_log;

		fi
	fi
    fi
    let "i=1"
done < "${1:-/dev/ttyUSB0}"
