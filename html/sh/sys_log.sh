#!/bin/bash

sys_log="/var/log/alfasys.log"

ts=$( date +%Y-%m-%d:%H:%M:%S )

echo $ts >> $sys_log
df -h >> $sys_log
free -h >> $sys_log
cat /proc/meminfo >> $sys_log
vmstat >> $sys_log
ps -Ao pid,user,fname,tmout,pcpu,size,vsize,cmd --sort=-size >> $sys_log
