<?php
echo "Start\r\n";

if(!file_exists("/etc/og_flag_new"))
{
	exec("git checkout unsupported");
}
else
{
require_once ("/var/www/bd_functions.php"); 
	$cur_version = '';
	if ( $argc < 3 ) {
	    echo "usage: ".$argv[ 0 ]." current_version latest_version\n";
	} else {
		$cur_version = trim($argv[ 1 ]);
	    echo "Upgrade from ".$argv[ 1 ]." to ".$argv[ 2 ]."\n";
	}

	$reboot = false;
	switch($cur_version)
	{
		case "1.0.1":
		case "1.0.2":
		case "1.0.3":
		case "1.0.4":
		case "1.0.5":
		case "1.0.6":
			$sql = "CREATE TABLE IF NOT EXISTS `emailParams` (
				`id` int(11) NOT NULL,
				  `name` varchar(64) NOT NULL,
				  `param` varchar(32) NOT NULL,
				  `param_min` varchar(32) NOT NULL,
				  `param_max` varchar(32) NOT NULL,
				  `time` int(11) NOT NULL DEFAULT '0',
				  `isOn` tinyint(4) NOT NULL DEFAULT '0'
				) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;";
			execSql($sql);
			$sql = "INSERT INTO `emailParams` (`id`, `name`, `param`, `param_min`, `param_max`, `time`, `isOn`) VALUES
(1, 'Кислотность ( ph )', 'ph', 'ph_min', 'ph_max', 0, 1),
(2, 'Удобрение ( tds )', 'tds', 'tds_min', 'tds_max', 0, 1),
(3, 't° раствора', 'wtemp', 'tank_t_min', 'tank_t_max', 0, 1),
(4, 't° воздуха', 'temp', 'microclimate_t_day', 'microclimate_t_night', 0, 1),
(5, 'Влажность', 'hum', 'hum_min', 'hum_max', 0, 1),
(6, 'Свет', 'light', 'light_min', 'light_max', 0, 1),
(7, 'CO2', 'co2', 'co2_min', 'co2_max', 0, 1),
(8, 'Вода', 'wlevel', 'wlevel_min', 'wlevel_max', 0, 1);";
			execSql($sql);
			$sql = "ALTER TABLE `emailParams`
 ADD PRIMARY KEY (`id`);";
			execSql($sql);
			$sql = "ALTER TABLE `emailParams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;";
			execSql($sql);
			exec("sudo cp /var/www/files-upgrade/pi /var/spool/cron/crontabs/pi");
			exec("sudo cp /var/www/files-upgrade/root /var/spool/cron/crontabs/root");
			exec("sudo cp /var/www/files-upgrade/comPortArduino.service /etc/systemd/system/comPortArduino.service");
			exec("sudo cp /var/www/files-upgrade/syncArduino.service /etc/systemd/system/syncArduino.service");
			exec("sudo systemctl enable comPortArduino.service");
			exec("sudo systemctl start comPortArduino.service");
			exec("sudo systemctl enable syncArduino.service");
			exec("sudo systemctl start syncArduino.service");
/*			exec("sudo systemctl restart web-helper");
			echo "t3";
			
			require_once( "/var/www/html/protected/vendor/sysapi/rpc.php" );
			$rpc = new Rpc();	
			$cur_auto = $rpc->systemrpc_isNtpdEnabled();
			$rpc->systemrpc_disableNtpd();
			echo "t4";
			if($cur_auto)
				$rpc->systemrpc_enableNtpd();
*/			
			$reboot = true;
			
//			break;
		case "1.0.7":
		case "1.0.8":
		case "1.0.9":
		case "1.0.10":
		case "1.0.11":
		case "1.0.12":
			exec("sudo cp /var/www/files-upgrade/tcpClient.service /etc/systemd/system/tcpClient.service");
			exec("sudo systemctl enable tcpClient.service");
			exec("sudo systemctl start tcpClient.service");
			$reboot = true;
			
			$sql = "CREATE TABLE IF NOT EXISTS `params` (
				`id` int(11) NOT NULL,
				  `name` varchar(255) NOT NULL,
				  `val` varchar(255) NOT NULL
				) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
			";
			execSql($sql);
			
			$sql = "INSERT INTO `params` (`id`, `name`, `val`) VALUES
				(1, 'cal_ph_1', '5'),
				(2, 'cal_ph_2', '6.9'),
				(3, 'cal_tds_1', '600'),
				(4, 'cal_tds_2', '2400');
			";
			execSql($sql);
			
			$sql = "ALTER TABLE `params`
				ADD PRIMARY KEY (`id`);
			";
			execSql($sql);
			
			$sql = "ALTER TABLE `params`
				MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
			";
			execSql($sql);
		case "1.0.13":
			$sql = "ALTER TABLE `regulatedPH` ADD `date_norm` VARCHAR(64) NOT NULL AFTER `date`;";
			execSql($sql);
			$sql = "ALTER TABLE `regulatedTDS` ADD `date_norm` VARCHAR(64) NOT NULL AFTER `date`;";
			execSql($sql);
			
		
	}
	if($reboot)	
		exec("sudo reboot");
	
}
exec("sync");
?>
