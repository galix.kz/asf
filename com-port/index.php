<?php
	die();
	$rampath = "/home/pi/ramdisk/";
	$path_time_last_recieve = "/home/pi/ramdisk/last_recieve";
	$fileLog = "/var/log/comPortLog.txt";

	function ResizeLogs($fileLog){
//		global $fileLog;
		$offset = 10000;
		
		$rs = fopen($fileLog,'rb');
		if ( $rs === false ) return;
			 
		fseek($rs, $offset, SEEK_END);
		$buffer = fread($rs, $offset);
		fclose($rs);		
		
		file_put_contents($fileLog, $buffer);
	}

	require_once ("/var/www/bd_functions.php"); 
	$debug = true;
	
	
	
	$time0 = time();
	exec('stty -F /dev/ttyS0 115200 -echo');
	$h = fopen('/dev/ttyS0', "rb");
	if(!$h) die("Arduino not found");
	$cnt = 0; $cntLimit = 14;
	$arrayCorrectParam = array('AirTemp' => 'temp', 'Humidity' => 'hum', 'WaterTemp' => 'wtemp', 'tds1' => 'tds_min', 'hum' => 'hum_min');
	$nbStr = 0;
	while(true)
//	while($time0 + 15 > time())
	{
		ob_start();
		$str = '';
		if($debug) echo date("Y-m-d H:i:s")."  Начинаем fread "."\r\n";
		while( ($c = fread($h, 1)) != chr(0xA) ) { $str .= $c; }
		file_put_contents($path_time_last_recieve, time().";".date('Y-m-d H:i:s'));
		if($debug) echo date("Y-m-d H:i:s")."  ".$str."\r\n";
		if(strlen($str) > 3)
		{
			$result = array();
			$res1 = explode(";",$str);
			foreach($res1 as $v1)
			{
				if($v1)
				{
					$v1 = trim($v1);
					$res2 = explode("=", $v1);
					$name =$res2[0];
					if(isset($arrayCorrectParam[$name]))
						$name = $arrayCorrectParam[$name];
					if($name != '' && isset($res2[1]))
					{
						$val = trim($res2[1]);
						if($val == "null" || $val == "nan")
							$val = -1;
						if($val == "-111")
						{
							$val = "Разогрев";
						}
						if($name != '')
						$result[$name] = $val;
					}
				}
			
			}
			foreach($result as $k => $v) // сохраняем в файл
			{
				@file_put_contents($rampath. mb_strtolower ($k), $v);
			}
			if(isset($result['Ph'])) // текущие значения с датчиков
			{
				if($cnt >= $cntLimit)
				{
					$cnt = 0;
					if(getActiveProfile() > 0) 
					{
						if($debug) echo date("Y-m-d H:i:s")."  Пытаемся записать в бд"."\r\n";
						$result['date'] = time();
						$result['date_norm'] = date("Y-m-d H:i:s");
						if(isset($result['curtime']))
							unset($result['curtime']);
						$strKeys = implode(",", array_keys($result));
						$strResult = "'".implode("','", $result)."'";
						
						$sql = "INSERT INTO data($strKeys) VALUES($strResult)";
						execSql($sql);
					}
				}
				else
					$cnt++;
			}
			else if (isset($result['RegulatedPH']) || isset($result['RegulatedTds'])) // значения после регулировки
			{
				if(getActiveProfile() > 0) 
				{
					if($debug) echo date("Y-m-d H:i:s")."  Пытаемся записать в бд(RegulatedPH)"."\r\n";
					$result['date'] = time();
//					$result['date_norm'] = date("Y-m-d H:i:s");
					$strKeys = implode(",", array_keys($result));
					$strResult = "'".implode("','", $result)."'";
					$tableName = isset($result['RegulatedPH']) ? "regulatedPH" : "regulatedTDS";
					$sql = "INSERT INTO $tableName ($strKeys) VALUES($strResult)";
					if($debug) echo $sql."\r\n";
					execSql($sql);
				}
			}
		}
		$strOut = ob_get_contents();
		ob_end_clean();
		file_put_contents($fileLog, $strOut, FILE_APPEND);
		$nbStr++;
//		if(filesize($fileLog) > 10000000) // 10 мб
		if($nbStr > 20000) // 10 мб
		{
			ResizeLogs($fileLog);
			$nbStr = 0;
		}
	}
	
	fclose($h);
	die("Complete\r\n");	
